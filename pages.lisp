;;; -*- mode: Lisp; show-trailing-whitespace: t; indent-tabs-mode: t; -*-

;;; Copyright (c) 2012-2025. José María Alonso Josa. All rights reserved

(in-package :lisperes)

(setf (html-mode) :html5)
(setf parenscript::*js-string-delimiter* #\")

(defun fortune-css ()
  (lass:compile-and-write
   `("#fortune" :width "80%"
        :margin "auto"
        :padding "20px"
        :background-color "#f44336"
        :color "white"
        :margin-bottom "15px"
        :border "4px"
        :border-style "solid"
        :border-color "black"
        :border-radius "20px"
        :background "red")))

(defun rest-css ()
  (lass:compile-and-write
    `("#rest" :text-align "center")))

(defun count-lines (file)
  "Count lines in a file"
  (let ((counter 0)
    (in (open file :if-does-not-exist nil)))
    (when in
      (loop for line = (read-line in nil)
     while line do (setf counter (1+ counter)))
      (close in)
      counter)))

(defparameter *core-images*
  '(("http://xkcd.com/224/" "https://imgs.xkcd.com/comics/lisp.jpg" "Lisp" "http://xkcd.com/224/")
    ("http://0xbabaf000l.blogspot.com.es/" "https://2.bp.blogspot.com/_P6813spMtmM/Sk9HQBYU_bI/AAAAAAAABDU/R-rsdDA9YQE/s1600/0010_en_vi-vs-emacs.png" "vi vs emacs" "0xbabaf000l")
    ("http://xkcd.com/297/" "https://imgs.xkcd.com/comics/lisp_cycles.png" "Lisp cycles" "http://xkcd.com/297/")
    ("https://www.reddit.com/r/lisp/comments/aqcqx/how_a_common_lisp_programmer_views_users_of_other/" "https://external-preview.redd.it/38BV_S1bnar7v2ih0t9Jn0dGPQGHpogw9coRn97b7-g.jpg?auto=webp&s=2f9f89f1b56487acc7cff9244df9c5cbe6ab5935" "How a Common Lisp programmer views users of other languages" "© http://vintage-digital.com/hefner/misc/lisp-programmers.jpg")
    ("http://xkcd.com/378/" "https://imgs.xkcd.com/comics/real_programmers.png" "Real programmers" "http://xkcd.com/378/")
    ("https://toggl.com/blog/save-princess-8-programming-languages" "https://toggl.com/blog/wp-content/uploads/2018/04/toggl-how-to-save-the-princess-in-8-programming-languages-0c32e93f47f3f6401913846c4c184e3e.jpg" "How to save the Princess" "2016 Mark Virkus")
    ("https://toggl.com/blog/programming-languages-explained-with-music-comic" "https://toggl.com/blog/wp-content/uploads/2019/07/programming-explained-with-music-toggl.jpg" "Programming explained as music" "2019 Mark Virkus")
    ("https://toggl.com/blog/programming-languages-games" "https://toggl.com/blog/wp-content/uploads/2020/08/toggl-programming-languages-as-games_1-721x1024.jpg" "Programming languages as games" "Mark Virkus")
    ("https://i.imgur.com/1gF1j.jpg" "https://i.imgur.com/1gF1j.jpg" "Programmers as seen as..." "")
    ("https://forums.scotsnewsletter.com/index.php?/topic/94361-man-loses-will-to-live-during-gentoo-install/" "img/gentoo.png" "Gentoo increasing blood pressure since 1999" "")
    ("https://sfconservancy.org/GiveUpGitHub/" "img/give-up-github.svg" "Give up Gitgub" "Software Freedom Conservancy")
    ("http://landoflisp.com/" "img/timeline.png" "Computer languages timelineFreeshell.de logo" "Conrad Barski")))

(defmacro image (link src alt class)
  `(htm (:a :href ,link
            (:img :src ,src
                  :alt ,alt
                  :class ,class))))

(defmacro unordered-list (items)
  `(htm (:ul
         (loop for (link . title) in ,items
               do (htm (:li (:a :href link (str title))))))))

(defun index-page (title)
  "Index page"
  (with-html-output-to-string
    (*standard-output* nil :prologue t :indent t)
    (:html :class "html"
           (:head
            (:title title)
            (:meta :http-equiv "content-type" :content "text/html; charset=UTF-8")
            (:meta :charset "utf-8")
            (:meta :name "viewport" :content "width=device-width, initial-scale=1")
            (:link :rel "author" :href "humans.txt")
            (:link :rel "shortcut icon" :href "/img/lambda-y.png")
            (:link :rel "stylesheet" :type "text/css" :href "css/style.css"))
            ;; (:style (str (fortune-css))
           ;;         (str (rest-css))))
           (:body :class "body"
                  (:blockquote "")
                  (:div :id "header"
                        (:div :id "fortune"
                              "#"
                              (str (count-lines *access-log*))
                              (:nbsp)
                              (str (say-fortune *fortunes*))))
                  (:div :id "core"
                        (loop for img in *core-images*
                              do (progn (image (first img)
                                               (second img)
                                               (third img)
                                               "responsive")
                                        (htm (:p :class "copyright" (str (conc "©" (fourth img)))))))
                        (:p "Contact")
                        (:hr)
                        (unordered-list
                         '(("https://es.linkedin.com/in/chemaalonsojosa" . "LinkedIn")
                           ("https://codeberg.org/nimiux" . "Codeberg")))
                        (:ul
                         (:li (:a :rel "me" :href "https://mastodon.sdf.org/@nimiux" "Mastodon")))
                        (:p "Other pages")
                        (:hr)
                        (unordered-list
                         '(("http://www.educa2.madrid.org/web/jose.alonsojosa" . "EducaMadrid")
                           ("http://es.wikipedia.org/wiki/Usuario:Alonsojosa" . "Wikipedia")))
                        (:p "Webcams")
                        (:hr)
                        (unordered-list
			  '(("https://www.webcamsdeasturias.com/asturias/picos-de-europa/cabrales/camarmena/picu-urriellu-naranjo-de-bulnes/43/" . "Camarmeña")
                           ("http://www.colladojermoso.com/webcam-meteo/" . "Collado Jermoso")
                           ("http://www.curavacas.es/cam" . "Curavacas")
                           ("https://cantur.com//camaras/home/cantucom/public.html/camaras/fuentede.jpg" . "Fuente Dé")
                           ("http://www.restaurantebarlabodeguilla.com/camara/gredos.jpg" . "Gredos")
                           ("http://www.refugiolagunagrandegredos.es/?page_id=1114" . "Laguna Grande de Gredos")
                           ("https://www.webcamsdeasturias.com/asturias/oriente/cangas-de-onis/cangas-de-onis/lagos-de-covadonga-lago-enol/159/" . "Lago Enol")
                           ("http://www.refugiodelmeicin.es/webcam/image.jpg" . "Meicín 1")
                           ("http://www.refugiodelmeicin.es/webcam/image2.jpg" . "Meicín 2")
                           ("https://aviaciondeportiva.senasa.es/camarasIP/CamaraOcanaHangar/camHangar000M.jpg" . "Ocaña")
                           ("https://www.webcamsdeasturias.com/asturias/refugio-de-urriellu/cabrales/bulnes/refugio-del-urriellu/135/" . "Picu Urriellu")
                           ("https://babia.net/webcam" . "San Emiliano")
                           ("http://webcam.valdeon.org/devisionnetwok.jpg" . "Valdeón")
                           ("https://www.webcamsdeasturias.com/asturias/oriente/cangas-de-onis/cangas-de-onis/refugio-de-vega-de-ario-marques-de-villaviciosa/177/" . "Vega de Ario")
                           ("http://grupogredos.com/index.php/webcam-victory/" . "Victory")))
                        (:p "Common Lisp resources and books")
                        (:hr)
                        (unordered-list
                         '(("http://mitpress.mit.edu/sicp" . "Structure and Interpretation of Computer Programs")
                           ("http://www.paulgraham.com/onlisp.html" . "On Lisp by Paul Graham")
                           ("http://www.defmacro.org/ramblings/lisp.html" . "The Nature of Lisp")
                           ("https://common-lisp.net/" . "Common-Lisp.net")
                           ("http://www.cliki.net/index" . "The Common Lisp Wiki")
                           ("http://www.lispworks.com/documentation/HyperSpec/Front/index.htm" . "Common Lisp HyperSpec")
                           ("http://articulate-lisp.com/" . "Articulate Common Lisp")
                           ("http://lisp-lang.org/" . "Common Lisp")
			   ("https://lispcookbook.github.io/cl-cookbook/" . "Common Lisp Cookbook")
                           ("https://www.cs.cmu.edu/Groups/AI/util/html/cltl/cltl2.html" . "Common Lisp the Language, 2nd edition by Guy L. Steele")
                           ("http://www.gigamonkeys.com/book/" . "Practical Common Lisp by Peter Seibel")
                           ("https://letoverlambda.com/" . "Let Over Lambda by Doug Hoyte")
                           ("http://landoflisp.com/" . "Land of Lisp by Conrad Barski")))
                        (:p "Obsoleted links")
                        (:hr)
                        (unordered-list
                         '(("https://wiki.gentoo.org/wiki/User:Nimiux" . "Gentoo")
                           ("http://cia.vc/stats/author/nimiux" . "Cia.vc"))))
                  (:div :id "footer"
                        (:div :class "footer-element"
                              (image "http://lisperati.com/"
                                     "img/lisplogo_warning_128.png"
                                     "Made with lisp logo"
                                     ""))
                        (:div :class "footer-element"
			                        (image "https://codeberg.org/"
                                     "img/codeberg.org-logo.png"
                                     "Codeberg logo"
				                      "")
                              (image "humans.txt"
                                     "img/humans.txt-logo.png"
                                     "Humans.txt logo"
				                      "")
                              (image "https://freeshell.de"
                                     "img/freeshell.de-logo.png"
                                     "Freeshell.de logo"
				                      ""))
                        (:div :class "footer-element"
                              (:a :href "#"
				  :onclick (parenscript:ps (alert "Welcome!"))
				  "© 2000-2025 Chema Alonso Josa")))))))

(defun robots-page ()
  "robots.txt page"
  (format nil "User-agent: *~%Disallow:"))

(defun repl ()
  (with-html-output-to-string
    (*standard-output* nil :prologue t :indent t)
    (:html
      (:head
        (:meta :char-set "utf-8"))
      (:body
        (:h2 "Jank REPL")))))

