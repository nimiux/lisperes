;;; -*- mode: Lisp; show-trailing-whitespace: t; indent-tabs-mode: t; -*-

;;; Copyright (c) 2012-2025. José María Alonso Josa. All rights reserved

(in-package :cl-user)

(defpackage #:lisperes
  (:use :cl :hunchentoot :cl-who :slynk)
  (:export #:start-slynk
           #:stop-slynk
           #:start-httpd
           #:stop-httpd))

