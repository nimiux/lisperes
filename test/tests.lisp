;;; -*- mode: Lisp; show-trailing-whitespace: t; indent-tabs-mode: t; -*-

;;; Copyright (c) 2012-2025. José María Alonso Josa. All rights reserved

(in-package :lisperes/test)

(defparameter *test-suite*
  `((ok (not (find 4 '(1 2 3))))
    (is 4 4)
    (isnt 1 #\1)))

(defun run-tests (tests)
  (not (some #'null (mapcar #'eval tests))))

(plan (length *test-suite*))
(defparameter *exit-code* (if (run-tests *test-suite*)
			   0
			   1))
(finalize)
(sb-ext:exit :code *exit-code*)

