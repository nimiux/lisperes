;;; -*- mode: Lisp; show-trailing-whitespace: t; indent-tabs-mode: t; -*-

;;; Copyright (c) 2012-2025. José María Alonso Josa. All rights reserved

(in-package :cl-user)

(defpackage #:lisperes/test
  (:use :cl :prove))
