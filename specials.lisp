;;; -*- mode: Lisp; show-trailing-whitespace: t; indent-tabs-mode: t; -*-

;;; Copyright (c) 2012-2025. José María Alonso Josa. All rights reserved

(in-package :lisperes)

;; HTTP server configuration
(defparameter *httpd-address* "127.0.0.1")
(defparameter *httpd-port* 4242)
(defparameter *app-root* (merge-pathnames #P"common-lisp/lisperes/" (user-homedir-pathname)))
(defparameter *document-root* (merge-pathnames #P"common-lisp/lisperes/htdocs/" (user-homedir-pathname)))
(defparameter *log-dir* (namestring (make-pathname :directory (namestring (user-homedir-pathname)) :name "log/lisperes")))
(defparameter *access-log* (make-pathname :directory *log-dir* :name "access" :type "log"))
(defparameter *message-log* (make-pathname :directory *log-dir* :name "messages" :type "log"))
(defparameter *error-template-directory* (merge-pathnames #P"errors/" *document-root*))

 ;; The port SBCL will be listening for shutdown. Not used ATM
;(defparameter *shutdown-port* 6200)

;; The port used for remote interaction with the server through slime or slynk
(defparameter *listening-port* 4005)

;; Fortunes
(defparameter *fortunes-filename* (make-pathname :directory (namestring (merge-pathnames #P"db/" *app-root*)) :name "fortunes" :type "lisp"))
(defvar *fortunes*)
