;;; -*- mode: Lisp; show-trailing-whitespace: t; indent-tabs-mode: t; -*-

;;; Copyright (c) 2012-2025. José María Alonso Josa. All rights reserved

(in-package :lisperes)

; The Hunchentoot logic goes in here
; this can be just a simple package loading
; calling some methods or it might be something else

(define-easy-handler (lisperes :uri "/") ()
  (index-page "Lisperes"))

(define-easy-handler (fortune :uri "/sayfortune") ()
  (say-fortune *fortunes*))

(define-easy-handler (robots :uri "/robots.txt") ()
  (robots-page))

(define-easy-handler (repl :uri "/repl") ()
  (repl))

;; Sly
(let ((slynk-instance))
  (defun start-slynk ()
    "Starts slynk server"
    (setf slynk-instance (slynk:create-server :port *listening-port* :dont-close t)))
  (defun stop-slynk ()
    "Stops slynk server"
    (slynk:stop-server slynk-instance)))

;; Swank
(let ((swank-instance))
  (defun start-swank ()
    "Starts swank server"
    (setf swank-instance (swank:create-server :port *listening-port* :dont-close t)))
  (defun stop-swank ()
    "Stops swank server"
    (swank:stop-server swank-instance)))

(setf hunchentoot:*show-lisp-errors-p* t)

;; HTTPD instance
(let ((httpd-instance (make-instance 'hunchentoot:easy-acceptor
				     :address *httpd-address*
                                     :port *httpd-port*
                                     :document-root *document-root*
                                     :access-log-destination *access-log*
                                     :message-log-destination *message-log*
                                     :error-template-directory *error-template-directory*)))
  (setf *fortunes* (read-file *fortunes-filename*))
  (defun start-httpd ()
    (hunchentoot:start httpd-instance))
  (defun stop-httpd ()
    (hunchentoot:stop httpd-instance)))
