((:CATEGORY "ACTIONS" :TEXT "If we keep doing what we are doing, we are going to keep getting what we are getting." :AUTHOR "Stephen R. Covey" :ITEM "" :DATE "")
 (:CATEGORY "ACTIONS" :TEXT "Those that can, do. Those that can't, complain." :AUTHOR "Linus Torvalds" :ITEM "" :DATE "")
 (:CATEGORY "ACTIONS" :TEXT "People don’t buy what you do, they buy why you do it." :AUTHOR "Simon Sinek" :ITEM "" :DATE "")
 (:CATEGORY "AI" :TEXT "Never ask an AI system what to do. Ask it to tell you the consecuences of the different things you might do." :AUTHOR "John McCarthy" :ITEM "The Robot and the Baby" :DATE "20041016" :URL "http://www-formal.stanford.edu/jmc/robotandbaby/robotandbaby.html")
 (:CATEGORY "BINARY" :TEXT "There are 10 types of people, those who understand binary and those who not." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "BINARY" :TEXT "There are 10 types of people in the world: those who know ternary, those who don't, and those who thought this was going to be a binary joke." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "BIRTHDAY" :TEXT "Birthdays are like busses, never get the number you want." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "BLINDDOG" :TEXT "On the Rue des Ecoles
lived an old man
with a blind dog
Every evening I would see him
guiding the dog along
the sidewalk, keeping
a firm grip on the leash
so that the dog wouldn't
run into a passerby
Sometimes the dog would stop
and look up at the sky
Once the old man
noticed me watching the dog
and he said, «Oh, yes,
this one knows
when the moon is out,
he can feel it on his face»." :AUTHOR "Barry Gifford" :ITEM "" :DATE "")
 (:CATEGORY "BOSS" :TEXT "Un señor que va en coche y se percata de que está perdido, maniobra y pregunta a alguien en la calle:
- ¡Disculpe!,
  ¿Podría usted ayudarme? He quedado a las dos con un amigo y llevo media hora de retraso y no sé dónde me encuentro.
- Claro que sí - le contesta - se encuentra usted en un coche, a unos 7 Km.  del centro ciudad, entre 40 y 42 grados de latitud norte y 58 y 60 de longitud oeste.
- Es usted ingeniero, ¿Verdad? -dice el del coche
- Sí señor, lo soy ¿Cómo lo ha adivinado?
- Muy sencillo, porque todo lo que me ha dicho es «técnicamente correcto», pero «prácticamente inútil»: continúo perdido, llegaré tarde y no sé qué hacer con su información.
- Es usted jefe, ¿Verdad? - pregunta el de la calle.
- En efecto - responde orgulloso el del coche - ¿Cómo lo ha sabido?
- Porque no sabe dónde está ni hacia dónde se dirige, ha hecho una promesa que no puede cumplir y espera que otro le resuelva el problema. De hecho, está usted exactamente en la misma situación que estaba antes de encontrarnos, pero ahora, por alguna extraña razón parece que la culpa es mía." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "BUGS" :TEXT "Ever wondered about the origins of the term «bugs» as applied to computer technology? U.S. Navy Capt. Grace Murray Hopper has firsthand explanation.
The 74-year-old captain, who is still on active duty, was a pioneer in computer technology during World War II. At the C.W. Post Center of Long Island University, Hopper told a group of Long Island public school administrators that the first computer «bug» was a real bug(a moth). At Harvard one August night in 1945, Hopper and her associates were working on the «granddaddy» of modern computers, the Mark I. «Things were going badly; there was something wrong in one of the circuits of the long glass-enclosed computer,» she said. «Finally, someone located the trouble spot and, using ordinary tweezers, removed the problem, a two-inch moth. From then on, when anything went wrong with a computer, we said it had bugs in it.» Hopper said that when the veracity of her story was questioned recently, «I referred them to my 1945 log book, now in the collection of the Naval Surface Weapons Center, and they found the remains of that moth taped to the page in question.»
[actually, the term «bug» had even earlier usage in regard to problems with radio hardware. Ed.]." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CAUSES" :TEXT "For many events, roughly 80% of the effects come from 20% of the causes." :AUTHOR "Vilfredo Pareto" :ITEM "" :DATE "")
 (:CATEGORY "CHESS" :TEXT "The Fastest Defeat In Chess
The big name for us in the world of chess is Gibaud, a French chess master.
In Paris during 1924 he was beaten after only four moves by a Monsieur Lazard. Happily for posterity, the moves are recorded and so chess enthusiasts may reconstruct this magnificent collapse in the comfort of their own homes.
Lazard was black and Gibaud white:
    1: P-Q4, Kt-KB3
    2: Kt-Q2, P-K4
    3: PxP, Kt-Kt5
    4: P-K6, Kt-K6
White then resigns on realizing that a fifth move would involve either a Q-KR5 check or the loss of his queen." :AUTHOR "Stephen Pile" :ITEM "The Book of Heroic Failures" :DATE "")
 (:CATEGORY "POSSIBLE" :TEXT "I. Cuando un científico distinguido pero anciano afirma que algo es posible, es casi seguro que tiene razón. Cuando afirma que algo es imposible, es muy probable que esté equivocado.
II. La única manera de descubrir los límites de lo posible es aventurarse un poco más allá¡ de ellos en lo imposible.
III. Any sufficiently advanced technology is indistinguishable from magic." :AUTHOR "Arthur C. Clarke" :ITEM "The three laws" :DATE "")
 (:CATEGORY "CLOSURES" :TEXT "The venerable master Qc Na was walking with his student, Anton. Hoping to prompt the master into a discussion, Anton said «Master, I have heard that objects are a very good thing - is this true?» Qc Na looked pityingly at his student and replied, «Foolish pupil - objects are merely a poor man's closures.»
Chastised, Anton took his leave from his master and returned to his cell, intent on studying closures. He carefully read the entire «Lambda: The Ultimate…» series of papers and its cousins, and implemented a small Scheme interpreter with a closure-based object system. He learned much, and looked forward to informing his master of his progress.
On his next walk with Qc Na, Anton attempted to impress his master by saying «Master, I have diligently studied the matter, and now understand that objects are truly a poor man's closures.» Qc Na responded by hitting Anton with his stick, saying «When will you learn? Closures are a poor man's object.» At that moment, Anton became enlightened." :AUTHOR "Anton van Straaten" :ITEM "" :DATE "" :URL "http://people.csail.mit.edu/gregs/ll1-discuss-archive-html/msg03277.html")
 (:CATEGORY "COMPUTERS" :TEXT "Computer science is not about computers any more than astronomy is about telescopes." :AUTHOR "Edsger W. Dijkstra" :ITEM "" :DATE "")
 (:CATEGORY "COMPUTERS" :TEXT "Computers are like air conditioners: they become useless when you open Windows." :AUTHOR "Linus Torvalds" :ITEM "" :DATE "")
 (:CATEGORY "COMPUTERS" :TEXT "Computers are like Old Testament gods; lots of rules and no mercy." :AUTHOR "Joseph Campbell" :ITEM "" :DATE "")
 (:CATEGORY "COMPUTERS" :TEXT "The effort of using machines to mimic the human mind has always struck me as rather silly. I would rather use them to mimic something better." :AUTHOR "Edsger W. Dijkstra")
 (:CATEGORY "COMPUTERS" :TEXT "Computers are there to optimize Murphy's law." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "COMPUTERS" :TEXT "De las cosas que se puede hacer con un ordenador, las inútiles son las mas divertidas." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "COMPUTERS" :TEXT "Never trust a computer you can't throw out a window." :AUTHOR "Steve Wozniak" :ITEM "" :DATE "")
 (:CATEGORY "COMPUTERS" :TEXT "A computer scientist is someone who, when told «go to hell», considers the «go to» harmful rather than the destination." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "COMPUTERS" :TEXT "If you want to write fast software, use a slow computer." :AUTHOR "Dominic Tarr" :ITEM "" :DATE "")
 (:CATEGORY "CONFERENCE" :TEXT "People who go to conferences are the ones who shouldn't." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "COURAGE" :TEXT "The opposite of courage is not cowardice, it's conformity." :AUTHOR "John Perry Barlow" :ITEM "" :DATE "")
 (:CATEGORY "DANGER" :TEXT "One is never so dangerous as when he’s utterly convinced he is right." :AUTHOR "John Perry Barlow" :ITEM "" :DATE "")
 (:CATEGORY "DESIGN" :TEXT "Design is not just what it looks like and feels like. Design is how it works." :AUTHOR "Steve Jobs" :ITEM "" :DATE "")
 (:CATEGORY "DESIGN" :TEXT "Design is the strategy, algorithm is the tactics." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DESIGN" :TEXT "No, Watson, this was not done by accident, but by design." :AUTHOR "Sherlock Holmes" :ITEM "" :DATE "")
 (:CATEGORY "DEVELOPMENT" :TEXT "Basically, perfect development is impossible. Development can be fast, good, and cheap. Pick two." :AUTHOR "Larry Wall" :ITEM "" :DATE "")
 (:CATEGORY "EARTH" :TEXT "/earth: file system full." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "EDUCATION" :TEXT "Education is what remains when what has been learnt has been forgotten." :AUTHOR "B.F. Skinner" :ITEM "" :DATE "")
 (:CATEGORY "FASHION" :TEXT "Fashion is a form of ugliness so intolerable that we have to alter it every six months." :AUTHOR "Oscar Wilde" :ITEM "" :DATE "")
 (:CATEGORY "FEAR" :TEXT "The man who fears suffering is already suffering from what he fears." :AUTHOR "Michel de Montaigne" :ITEM "" :DATE "")
 (:CATEGORY "FREE" :TEXT "If you can't do it better, don't complain at all. (specially when its for free)" :ITEM "" :DATE "")
 (:CATEGORY "FRANKLIN" :TEXT "I made it a rule to forbear all direct contradictions to the sentiments of others, and all positive assertion of my own. I even forbade myself the use of every word or expression in the language that imported a fixed opinion, such as «certainly», «undoubtedly», etc. I adopted instead of them «I conceive», «I apprehend», or «I imagine» a thing to be so or so; or «so it appears to me at present».
When another asserted something that I thought an error, I denied myself the pleasure of contradicting him abruptly, and of showing him immediately some absurdity in his proposition. In answering I began by observing that in certain cases or circumstances his opinion would be right, but in the present case there appeared or semed to me some difference, etc.
I soon found the advantage of this change in my manner; the conversations I engaged in went on more pleasantly. The modest way in which I proposed my opinions procured them a readier reception and less contradiction.
I had less mortification when I was found to be in the wrong, and I more easily prevailed with others to give up their mistakes and join with me when I happened to be in the right." :AUTHOR "Benjamin Franklin" :ITEM "Autobiography" :DATE "")
 (:CATEGORY "GENIUS" :TEXT "A man of genius makes no mistakes. His errors are volitional and are the portals of discovery." :AUTHOR "James Joyce" :ITEM "Ulysses" :DATE "")
 (:CATEGORY "HAMLET" :TEXT "To be or not to be, that is the question;
Whether 'tis nobler in the mind to suffer
The slings and arrows of outrageous fortune,
Or to take arms against a sea of troubles,
And by opposing, end them. To die, to sleep;
No more; and by a sleep to say we end
The heartache and the thousand natural shocks
That flesh is heir to 'tis a consummation
Devoutly to be wish'd. To die, to sleep;
To sleep, perchance to dream. Ay, there's the rub,
For in that sleep of death what dreams may come,
When we have shuffled off this mortal coil,
Must give us pause. There's the respect
That makes calamity of so long life,
For who would bear the whips and scorns of time,
Th'oppressor's wrong, the proud man's contumely,
The pangs of despised love, the law's delay,
The insolence of office, and the spurns
That patient merit of th'unworthy takes,
When he himself might his quietus make
With a bare bodkin? who would fardels bear,
To grunt and sweat under a weary life,
But that the dread of something after death,
The undiscovered country from whose bourn
No traveller returns, puzzles the will,
And makes us rather bear those ills we have
Than fly to others that we know not of?
Thus conscience does make cowards of us all,
And thus the native hue of resolution
Is sicklied o'er with the pale cast of thought,
And enterprises of great pitch[1] and moment
With this regard their currents turn awry,
Aknd lose the name of action." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HANGOVER" :TEXT "There are two problems with a major hangover. You feel like you are going to die and you're afraid that you won't." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HEALTH" :TEXT "Be careful of reading health books, you might die of a misprint." :AUTHOR "Mark Twain" :ITEM "" :DATE)
 (:CATEGORY "HUMAN" :TEXT "Man is condemned to be free. Condemned, because he did not create himself, yet, [he] is free; because, once thrown into the world, he is responsible for everything he does." :AUTHOR "Jean Paul Sartre" :ITEM "Existentialism is a Humanism" :DATE "")
 (:CATEGORY "HUMOR" :TEXT "Humor is the only divine quality to be found in humanity." :AUTHOR "Schopenhauer" :ITEM "" :DATE)
 (:CATEGORY "ICE" :TEXT "There are many of us in this old world of ours who hold that things break about even for all of us. I have observed, for example, that we all get about the same amount of ice. The rich get it in the summer and the poor get it in the winter." :AUTHOR "Bat Masterson" :ITEM "" :DATE "")
 (:CATEGORY "IDEAS" :TEXT "There are some ideas so wrong that only a very intelligent person could believe in them." :AUTHOR "George Orwell" :ITEM "" :DATE "")
 (:CATEGORY "IGNORANCE" :TEXT "It takes considerable knowledge just to realize the extent of your own ignorance." :AUTHOR "Thomas Sowell" :ITEM "" :DATE "")
 (:CATEGORY "INFORMATION" :TEXT "Information is cheap, meaning is expensive." :AUTHOR "George Dyson" :ITEM "" :DATE "")
 (:CATEGORY "INSANITY" :TEXT "Sometimes the appropriate response to reality is to go insane." :AUTHOR "Philip K. Dick" :ITEM "" :DATE "")
 (:CATEGORY "INSANITY" :TEXT "Some people never go crazy, What truly horrible lives they must live." :AUTHOR "Charles Bukowski" :ITEM "" :DATE "")
 (:CATEGORY "INSANITY" :TEXT "The first principle is that you must not fool yourself, and you are the easiest person to fool." :AUTHOR "Richard Feynman" :ITEM "" :DATE "")
 (:CATEGORY "INSPIRATION" :TEXT "La inspiración existe pero te debe encontrar cuando estás trabajando." :AUTHOR "Pablo Picasso" :ITEM "" :DATE "")
 (:CATEGORY "INTELLIGENCE" :TEXT "A veces pienso que la prueba mas fehaciente de que existe vida inteligente en el universo es que nadie ha intentado contactar con nosotros." :AUTHOR "Bill Watterson" :ITEM "" :DATE "")
 (:CATEGORY "INTELLIGENCE" :TEXT "Why is it that all of the instruments seeking intelligent life in the universe are pointed away from Earth?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "INVESTIGATION" :TEXT "La verdadera investigación consiste en buscar a oscuras el interruptor de la luz. Cuando la luz se enciende, todo el mundo lo ve muy claro." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "INVESTIGATION" :TEXT "Se ha descubierto recientemente que la investigacion causa cancer en la ratas." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "INVOLVE" :TEXT "Tell me and I forget, teach me and I may remmember, involve me and I learn." :AUTHOR "Confucio, Lao Tzu, Xun Kuang, Benjamin Franklin" :ITEM "" :DATE "")
 (:CATEGORY "LEARN" :TEXT "Me lo contaron y lo olvidé, lo vi y lo entendí, lo hice y lo aprendí." :AUTHOR "Confucio" :ITEM "" :DATE "")
 (:CATEGORY "JURY" :TEXT "Just remember: when you go to court, you are trusting your fate to twelve people that weren't smart enough to get out of jury duty!." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "KIND" :TEXT "Be kind, for everyone is fighting a hard battle." :AUTHOR "Ian MacLaren" :ITEM "" :DATE "")
 (:CATEGORY "KNOWLEDGE" :TEXT "Knowledge, sir, should be free to all!." :AUTHOR "Harry Mudd" :ITEM "I, Mudd. stardate 4513.3" :DATE "")
 (:CATEGORY "KNOWLEDGE" :TEXT "I think by far the most important bill in our whole code is that for the diffusion of knowlege among the people. No other sure foundation can be devised, for the preservation of freedom and happiness. If anybody thinks that kings, nobles, or priests are good conservators of the public happiness send them here." :AUTHOR "Thomas Jefferson" :ITEM "Letter To George Wythe Paris" :DATE "August 13, 1786")
 (:CATEGORY "KNOWLEDGE" :TEXT "Those who know don't talk. Those who talk don't know." :AUTHOR "Lao-Tse" :ITEM "" :DATE "")
 (:CATEGORY "LATIN" ∴TEXT "Latin is a language,
Dead as Dead Can Be,
First it Killed the Romans,
Now It's Killing Me." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "Sonreír cuando las cosas van mal, quiere decir que ya tienes a quien echarle la culpa." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LEADER" :TEXT "A good leader is someone whose troops will follow him, if only out of curiosity." :AUTHOR "Gen. Colin Powell")
 (:CATEGORY "LEARN" :TEXT "Nunca he encontrado una persona tan ignorante que no pueda aprender algo de ella." :AUTHOR "Galileo Galilei" :ITEM "" :DATE "")
 (:CATEGORY "LEARN" :TEXT "Anyone who stops learning is old, whether at twenty or eighty. Anyone who keeps learning stays young." :AUTHOR "Henry Ford" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Life, loathe it or ignore it, you can't like it." :AUTHOR "Marvin" :ITEM "Hitchhiker's Guide to the Galaxy" :DATE "")
 (:CATEGORY "LIFE" :TEXT "So much has been lost: so much pain,
                   so much blood and for what? I wonder.
                   The past tempts us, the present confuses us,
                   and the future frightens us.
                   And our lives slip away moment by moment
                   in that vast terrible in-between.
                   But there is still time to seize that one,
                   last, fragile moment.
                   To choose something better. To make a difference,
                   as you say. And I intend to do just that." :AUTHOR "Turham. Emperor of the Centauri Republic" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Life -- Love It or Leave It." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Producir y nutrir, producir y no poseer, obrar y no retener, acrecentar y no regir, son el misterio de la vida." :AUTHOR "Lao Tse" :DATE "Siglo VI ac")
 (:CATEGORY "LIFE" :TEXT "If you wind up with a boring, miserable life because you listened to your mom, your dad, your teacher, your priest, or some guy on TV telling you how to do your shit, then YOU DESERVE IT." :AUTHOR "Frank Zappa" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "A life spent making mistakes is not only more honorable, but more useful than a life spent doing nothing." :AUTHOR "George Bernard Shaw" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Life is a tragedy for those who feel, and a comedy for those who think." :AUTHOR "Jean de La Bruyère" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Good-bye. I am leaving because I am bored" :AUTHOR "George Saunders" :ITEM "Dying words" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Our life is frittered away by detail. Simplify, simplify." :AUTHOR "Henry David Thoreau" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "A LISP programmer knows the value of everything, but the cost of nothing." :AUTHOR "Alan Perlis" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "When my first wife left me, she took everything. The kids, the house, the car. She didn't even leave me the cdr!." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "Bumper sticker: «My other CAR is a CDR»." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "Tiger got to hunt,
                    Bird got to fly;
                    Lisper got to sit and wonder,(Y (Y Y))?
                    Tiger got to sleep,
                    Bird got to land;
                    Lisper got to tell himself he understand." :AUTHOR "Kurt Vonnegut, modified by Darius Bacon" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "LISP is the most intelligent way to misuse a computer." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "The power of LISP is its own worst enemy." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "LISP has assisted a number of our most gifted fellow humans in thinking previously impossible thoughts." :AUTHOR "Edsger W. Dijkstra" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "There was a joke back in the 80's, when Reagan's SDI (Strategic Defense Initiative) programme was in full swing, that someone stole the Lisp source code to the missile interceptor program and to prove it, he showed the last page of code:
    ))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "The enlightenment came instantaneously. One moment I understood nothing, and the next moment everything clicked into place. I've achieved nirvana. Dozens of times I heard Eric Raymond's statement quoted by different people: ¡Lisp is worth learning for the profound enlightenment experience you will have when you finally get it; that experience will make you a better programmer for the rest of your days, even if you never actually use Lisp itself a lot.¡ I never understood this statement. I never believed it could be true. And finally, after all the pain, it made sense! There was more truth to it than I ever could have imagined. I've achieved an almost divine state of mind, an instantaneous enlightenment experience that turned my view of computer science on its head in less than a single second." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "(eq today (car (cdr life)))" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "(eq today (first (rest your-life)))" :AUTHOR "JayOsako" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "(eq today (car (cdr (life you))))" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "Q: How many Lisp programmers does it take to change a lightbulb?. A: A whole CAR full" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "These are some of the funniest examples of a genre of jokes told at the MIT AI Lab about various noted hackers. The original koans were composed by Danny Hillis, who would later found Connection Machines, Inc. In reading these, it is at least useful to know that Minsky, Sussman, and Drescher are AI researchers of note, that Tom Knight was one of the Lisp machine's principal designers, and that David Moon wrote much of Lisp Machine Lisp.
    Tom Knight and the Lisp Machine
    A novice was trying to fix a broken Lisp machine by turning the power off and on.
    Knight, seeing what the student was doing, spoke sternly:
    You cannot fix a machine by just power-cycling it with no understanding of what is going wrong.
    Knight turned the machine off and on.
    The machine worked.
    Moon instructs a student
    One day a student came to Moon and said:
    «I understand how to make a better garbage collector. We must keep a reference count of the pointers to each cons.»
    Moon patiently told the student the following story:
    One day a student came to Moon and said: «I understand how to make a better garbage collector…»
    [Ed. note: Pure reference-count garbage collectors have problems with circular structures that point to themselves.]
    Sussman attains enlightenment
    In the days when Sussman was a novice, Minsky once came to him as he sat hacking at the PDP-6.
    «What are you doing?», asked Minsky.
    «I am training a randomly wired neural net to play Tic-Tac-Toe» Sussman replied.
    «Why is the net wired randomly?», asked Minsky.
    «I do not want it to have any preconceptions of how to play», Sussman said.
    Minsky then shut his eyes.
    «Why do you close your eyes?», Sussman asked his teacher.
    «So that the room will be empty.»
    At that moment, Sussman was enlightened.
    Drescher and the toaster
    A disciple of another sect once came to Drescher as he was eating his morning meal.
    «I would like to give you this personality test», said the outsider, «because I want you to be happy.»
    Drescher took the paper that was offered him and put it into the toaster, saying: «I wish the toaster to be happy, too.»
    Visitors to this country are often surprised to find that Americans like to begin a conversation by asking «what do you do?» I've never liked this question. I've rarely had a neat answer to it. But I think I have finally solved the problem. Now, when someone asks me what I do, I look them straight in the eye and say «I'm designing a new dialect of Lisp.» I recommend this answer to anyone who doesn't like being asked what they do. The conversation will turn immediately to other topics")
 (:CATEGORY "LISP" :TEXT "While for most programmers knowing a language means having memorised a bunch of syntax, for a lisp programmer it means how the language relates to Lisp." :AUTHOR "Douglas Hoyte" :ITEM "Let Over Lambda" :DATE "2008")
 (:CATEGORY "LISP" :TEXT "Lisp has all the visual appeal of oatmeal with fingernail clippings mixed in." :AUTHOR "Larry Wall" :ITEM "" :DATE "")
 (:CATEGORY "LOGIC" :TEXT "La lógica es un método sistemático para llegar con confianza a la conclusión errónea." :AUTHOR "Manly" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Love at first sight is one of the greatest labor-saving devices the world has ever seen." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Cuando el poder del amor sobrepase el amor al poder, el mundo conocerá la paz." :AUTHOR "James Marshall (Jimi Hendrix)" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "1 El amor eterno dura aproximadamente 3 meses.
              2 No te metas en el mundo de las drogas. Ya somos muchos y hay muy poca.
              3 Todo tiempo pasado fue anterior.
              4 Tener la conciencia limpia es síntoma de mala memoria.
              5 El que nace pobre y feo tiene grandes posibilidades de que al crecer se le desarrollen ambas condiciones.
              6 Los honestos son inadaptados sociales.
              7 El que quiera celeste que mezcle azul y blanco.
              8 Pez que lucha contra la corriente muere electrocutado.
              9 La esclavitud no se abolió, se cambió a 8 horas diarias.
             10 Si la montaña viene hacia ti, Corre Es un derrumbe.
             11 Lo importante no es ganar, sino hacer perder al otro.
             12 No soy un completo inútil. Por lo menos sirvo de mal ejemplo.
             13 La droga te buelbe vruto.
             14 Si no eres parte de la solución, eres parte del problema.
             15 Errar es humano, pero echarle la culpa a otro es mas humano todavía.
             16 El que nace pa tamal nunca ta bien.
             17 Lo importante no es saber, sino tener el teléfono del que sabe.
             19 Yo no sufro de locura, la disfruto a cada minuto.
             20 Es bueno dejar el trago, lo malo es no acordarse donde.
             21 El dinero no hace la felicidad la compra hecha.
             22 Una mujer me arrastró a la bebida Y nunca tuve la cortesía de darle las gracias.
             23 Si tu novia perjudica tu estudio, deja el estudio y perjudica a tu novia.
             24 La inteligencia me persigue pero yo soy mas rápido.
             25 Huye de las tentaciones despacio, para que puedan alcanzarte.
             26 La verdad absoluta no existe y esto es absolutamente cierto.
             27 Hay un mundo mejor pero es carísimo.
             28 Ningún tonto se queja de serlo… No les debe ir tan mal.
             29 Estudiar es desconfiar de la inteligencia del compañero de al lado.
             30 La mujer que no tiene suerte con los hombres no sabe la suerte que tiene.
             31 No hay mujer fea solo belleza rara.
             32 La pereza es la madre de todos los vicios y como madre hay que respetarla.
             33 Si un pajarito te dice algo debes estar loco pues los pájaros no hablan.
             34 En cada madre hay una suegra en potencia.
             35 Lo importante es el dinero, la salud va y viene.
             36 Trabajar nunca mato a nadie… pero para que arriesgarse.
             37 No te tomes la vida en serio, al fin y al cabo no saldrás vivo de ella.
             38 Felices los que nada esperan, porque nunca serán defraudados.
             39 El alcohol mata lentamente. No importa no tengo prisa.
             40 La confusión esta clarísima.
             41 Mátate estudiando y serás un cadáver culto.
             42 Lo triste no es ir al cementerio, sino quedarse.
             43 Hay dos palabras que te abrirán muchas puertas Tire y Empuje.
             44 ¿Para que tomar y manejar si puedes fumar y volar.
             45 Dios mío dame paciencia, ¡¡Pero dámela YA!!." :AUTHOR "Les Luthiers" :ITEM "" :DATE "")
 (:CATEGORY "LUCK" :TEXT "La suerte no es más que el meditado cuidado de todos los detalles" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LUCK" :TEXT "There is no such thing as luck; there is only adequate or inadequate preparation to cope with a statistical universe." :AUTHOR "Robert A. Heinlein" :ITEM "Have Space Suit - Will Travel" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MATHEMATICS" :TEXT "If people do not believe that mathematics is simple, it is only because they do not realize how complicated life is." :AUTHOR "John von Neumann" :ITEM "" :DATE "")
 (:CATEGORY "MIND" :TEXT "Great minds run in great circles" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MIND" :TEXT "Keep an open mind not so open that your brain falls out" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MOUNTAIN" :TEXT "El alpinista es quién conduce su cuerpo allí dónde un día sus ojos lo soñaron." :AUTHOR "Gaston Rébuffat" :ITEM "" :DATE "")
 (:CATEGORY "MUSIC" :TEXT "Information is not knowledge
                  Knowledge is not wisdom
                  Wisdom is not truth
                  Truth is not beauty
                  Beauty is not love
                  Love is not music
                  Music is the best." :AUTHOR "Frank Zappa" :ITEM "" :DATE "")
 (:CATEGORY "NATURE" :TEXT "The scientist does not study Nature because it is useful; he studies it because he delights in it, and he delights in it because it is beautiful. If Nature were not beautiful, it would not be worth knowing, and if Nature were not worth knowing, life would not be worth living." :AUTHOR "Henri Poincaré" :ITEM "" :DATE "")
 (:CATEGORY "NICE" :TEXT "I
                 am
                 not
                 very
                 happy
                 acting
                 pleased
                 whenever
                 prominent
                 scientists
                 overmagnify
                 intellectual
                 enlightenment" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "NIETZSCHE" :TEXT "God is Dead.
                      -- Nietzsche
                      Nietzsche is Dead.
                      -- God
                      Nietzsche is God.
                      -- Dead" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "OCCAM" :TEXT "entia non sunt multiplicanda praeter necessitatem" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "OFFEND" :TEXT "I like offending people because I think people who get offended should be offended." :AUTHOR "Linus Torvalds" :ITEM "" :DATE "")
 (:CATEGORY "OFFICE" :TEXT "<Tazman> damn my office is cold.
                   <Tazman> need a hot secretary to warm it up." :AUTHOR "Tazman" :ITEM "#Linux" :DATE "")
 (:CATEGORY "OPTIMIST" :TEXT "optimist, n: A bagpiper with a beeper")
 (:CATEGORY "PAINTERS" :TEXT "Some painters transform the sun into a yellow spot; others transform a yellow spot into the sun." :AUTHOR "Pablo Picasso" :ITEM "" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "People's Action Rules:
       (1) Some people who can, shouldn't.
       (2) Some people who should, won't.
       (3) Some people who shouldn't, will.
       (4) Some people who can't, will try, regardless.
       (5) Some people who shouldn't, but try, will then blame others" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "The world is full of people who have never, since childhood, met an open doorway with an open mind." :AUTHOR "E.B. White" :ITEM "" :DATE "")
 (:CATEGORY "PERFECTION" :TEXT "Perfection is reached, not when there is no longer anything to add, but when there is no longer anything to take away." :AUTHOR "Antoine de Saint-Exupery" :ITEM "" :DATE "")
 (:CATEGORY "PESSIMIST" :TEXT "A pessimist is a man who has been compelled to live with an optimist." :AUTHOR "Elbert Hubbard" :ITEM "" :DATE "")
 (:CATEGORY "POLITICIANS" :TEXT "Politicians should read science fiction, not westerns and detective stories." :AUTHOR "Arthur C. Clarke" :ITEM "" :DATE "")
 (:CATEGORY "POLITICS" :TEXT "Haz política, porque, de todas formas la política se va a hacer, y, si tú no la haces, otros la harán contra ti." :AUTHOR "Antonio Machado" :ITEM "" :DATE "")
 (:CATEGORY "POSTELSLAW" :TEXT "In 1981, [Jon Postel] formulated what's known as Postel's Law:
    «Be conservative in what you do; be liberal in what you accept from others.»
    Originally intended to foster «interoperability», the ability of multiple computer systems to understand one another, Postel's Law is now recognized as having wider applications. To build a robust global network with no central authority, engineers were encouraged to write code that could «speak» as clearly as possible yet «listen» to the widest possible range of other speakers, including those who do not conform perfectly to the rules of the road.
    The human equivalent of this robustness is a combination of eloquence and tolerance - the spirit of good conversation. Trolls embody the opposite principle. They are liberal in what they do and conservative in what they construe as acceptable behavior from others. You, the troll says, are not worthy of my understanding; I, therefore, will do everything I can to confound you" :AUTHOR "Sullyman" :ITEM "To Mock a Mockingbird")
 (:CATEGORY "PROBABILITY" :TEXT "A statistician told a friend that he never took airplanes: «I have computed the probability that there will be a bomb on the plane,» he explained, «and although this probability is low, it is still too high for my comfort».
Two weeks later, the friend met the statistician on a plane. «How come you changed your theory?» he asked. «Oh, I didn't change my theory; it's just that I subsequently computed the probability that there would simultaneously be two bombs on a plane. This probability is low enough for my comfort. So now I simply carry my own bomb»")
 (:CATEGORY "PROBLEMS" :TEXT "Peers's Law: The solution to a problem changes the nature of the problem" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROBLEMS" :TEXT "The way we see the problem is the problem." :AUTHOR "Stephen R. Covey" :ITEM "" :DATE "")
 (:CATEGORY "PROBLEMS" :TEXT "Laugh at your problems; everybody else does." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROBLEMS" :TEXT "En el mundo de la informática los problemas no desaparecen, solo se solapan." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROBLEMS" :TEXT "The chief cause of problems is solutions." :AUTHOR "Eric Sevareid" :ITEM "" :DATE "")
 (:CATEGORY "PRODUCTIVITY" :TEXT "Productivity is being able to do things that you were never able to do before." :AUTHOR "Franz Kafka" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAM" :TEXT "Though a program be but three lines long, someday it will have to be maintained." :AUTHOR "Tao of programming" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAM" :TEXT "Fold knowledge into data so program logic can be stupid and robustram." :AUTHOR "Erick Steven Raymond" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAM" :TEXT "A well-written program is its own Heaven a poorly-written program is its own Hell." :AUTHOR "Tao of programming" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAM" :TEXT "A proof is a program; the formula it proves is a type for the program." :AUTHOR "Haskell Curry" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAM" :TEXT "Programs must be written for people to read, and only incidentally for machines to execute." :AUTHOR "Abelson and Sussman" :ITEM "Structure and Interpretation of Computer Programs" :DATE "")
 (:CATEGORY "PROGRAM" :TEXT "Progress is possible only if we train ourselves to think about programs without thinking of them as pieces of executable code." :AUTHOR "Edsger W. Dijkstra" :ITEM "" DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "La programación es una carrera entre ingenieros de software luchando para construir programas cada vez mas grandes, mejores y a prueba de idiotas, y el universo intentando producir cada vez mas grandes y mejores idiotas. Por ahora, gana el universo." :AUTHOR "Rich Cook" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "It's not enough to be a great programmer; you have to find a great problem." :AUTHOR "Charles Simonyi" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "Los verdaderos programadores no hablan de su código. Fue difícil escribirlo, y aún mucho mas entenderlo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "Los verdaderos programadores no dibujan organigramas. Los hombres de las cavernas dibujaban organigramas, y mira como les ha ido." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "Los verdaderos programadores no trabajan de 9 a 6. Si puede verse alguno a las 9, es porque ha estado toda la noche trabajando" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "All programmers are optimists." :AUTHOR "Frederick P. Brooks, Jr" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "Maldecir es el único lenguaje que dominan todos los programadores" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "Real programmers don't comment their code. It was hard to write, it should be hard to understand" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "We will encourage you to develop the three great virtues of a programmer: laziness, impatience, and hubris." :AUTHOR "Larry Wall" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMERS" :TEXT "Bad programmers worry about the code. Good programmers worry about data structures and their relationships." :AUTHOR "Linus Torvalds" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live." :AUTHOR "Kyle Ritcher" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "The C Programming language: A language which combines the flexibility of assembly language with the power of assembly language" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it" :AUTHOR "Brian W. Kernighan" :ITEM "The Elements of Programming Style, 2nd edition, chapter 2." :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "The random quantum fluctuations of my brain are historical accidents that happen to have decided that the concepts of dynamic scoping and lexical scoping are orthogonal and should remain that way." :AUTHOR "Larry Wall" :ITEM "<199709021854.LAA12794@wall.org>" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "No programming language is perfect.
    There is not even a single best language.
    There are only languages well suited or perhaps poorly suited for particular purposes." :AUTHOR "Herbert Mayer" :ITEM "" :DATE "")
 (:CATEGORY "OOPROGRAMMING" :TEXT "Object-oriented programming is an exceptionally bad idea which could only have originated in California." :AUTHOR "Edsger W. Dijkstra" :ITEM "" :DATE "")
 (:CATEGORY "OOPROGRAMMING" :TEXT "object-oriented design is the roman numerals of computing." :AUTHOR "Rob Pike" :ITEM "" :DATE "")
 (:CATEGORY "OOPROGRAMMING" :TEXT "The phrase «object-oriented» means a lot of things. Half are obvious, and the other half are mistakes." :AUTHOR "Paul Graham" :ITEM "" :DATE "")
 (:CATEGORY "OOPROGRAMMING" :TEXT "Implementation inheritance causes the same intertwining and brittleness that have been observed when goto statements are overused. As a result, OO systems often suffer from complexity and lack of reuse." :AUTHOR "John Ousterhout Scripting" :ITEM "IEEE Computer" :DATE "March 1998")
 (:CATEGORY "OOPROGRAMMING" :TEXT "90% of the shit that is popular right now wants to rub its object-oriented nutsack all over my code." :AUTHOR "kfx" :ITEM "" :DATE "")
 (:CATEGORY "OOPROGRAMMING" :TEXT "Sometimes, the elegant implementation is just a function. Not a method. Not a class. Not a framework. Just a function." :AUTHOR "John Carmack" :ITEM "" :DATE "")
 (:CATEGORY "OOPROGRAMMING" :TEXT "The problem with object-oriented languages is they’ve got all this implicit environment that they carry around with them. You wanted a banana but what you got was a gorilla holding the banana and the entire jungle." :AUTHOR "Joe Armstrong" :ITEM "" :DATE "")
 (:CATEGORY "OOPROGRAMMING" :TEXT "I used to be enamored of object-oriented programming. I’m now finding myself leaning toward believing that it is a plot designed to destroy joy." :AUTHOR "Eric Allman D" :ITEM "" :DATE "")
 (:CATEGORY "PROJECTS" :TEXT "Caminar sobre las aguas y desarrollar programas a partir de especificaciones es fácil, si ambas están congeladas." :AUTHOR "Howard V. Berar" :ITEM "" :DATE "")
 (:CATEGORY "PROJECTS" :TEXT "El objetivo de la informática es construir algo que dure al menos hasta que hayamos terminado de construirlo" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROJECTS" :TEXT "Tardó 300 años en construirse, y cuando iba por el 10%, ya se sabía que iba
                        a ser un desastre. Pero para entonces la inversión había sido tan grande que
                        se sintieron obligados a seguir. Desde que se terminó, ha costado una
                        fortuna mantenerla y todavía está en peligro de caerse. No hay ningún plan
                        para sustituirla porque nunca ha sido necesaria. Supongo que toda
                        instalación de software tiene programas análogos a lo anterior." :AUTHOR "K. E. Iverson" :ITEM "Sobre la torre inclinada de Pisa")
 (:CATEGORY "PUNCTUAL" :TEXT "The trouble with being punctual is that nobody's there to appreciate it." :AUTHOR "Franklin P. Jones" :ITEM "" :DATE "")
 (:CATEGORY "QUESTION" :TEXT "I try not to think with my gut. If I’m serious about understanding the world, thinking with anything besides my brain, as tempting as that might be, is likely to get me into trouble." :AUTHOR "Carl Sagan" :ITEM "When asked a question to which he didn’t know the answer and after he firmly said so and the questioner persisted: ‘But what is your gut feeling?’]" :DATE "")
 (:CATEGORY "NUMBERS" :TEXT "Anyone who attempts to generate random numbers by deterministic means is, of course, living in a state of sin." :AUTHOR "John von Neumann" :ITEM "" :DATE "")
 (:CATEGORY "TEXT" :TEXT "Aoccdrnig to rscheearch at Cmabrigde Uinervtisy, it deosn't mttaer in waht oredr the ltteers in a wrod are, the olny iprmoetnt tihng is taht the frist and lsat ltteer be at the rghit pclae. The rset can be a toatl mses and you can sitll raed it wouthit a porbelm. Tihs is bcuseae the huamn mnid deos not raed ervey lteter by istlef, but the wrod as a wlohe" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "REALITY" :TEXT "eality is what refuses to go away when I stop believing in it." :AUTHOR "Philip K. Dick" :ITEM "" :DATE "")
 (:CATEGORY "REASONABLE" :TEXT "The reasonable man adapts himself to the world; the unreasonable one persists in trying to adapt the world to himself. Therefore all progress
    depends on the unreasonable man." :AUTHOR "George Bernard Shaw" :ITEM "" :DATE "")
 (:CATEGORY "RECURSION" :TEXT "Recursion, noun: See recursion" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "RECURSION" :TEXT "To understand recursion you have to understand recursion" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "RSA" :TEXT "Take two large primes, q and p.
    Find the product n, and the totient phi.
    If e and phi have GCD one
    And d 's inverse of e , you're done!
    For, sending M raised to the e
    Reduced mod n gives secre- C.
    Now, take that C and raise to the d
    And then mod n --the perfect key!." :AUTHOR "Daniel G" :ITEM "Treat, «Proof by poem: The RSA encryption algorithm,» Mathematics Magazine, 75(Oct. 2002): p. 255" :DATE "")
 (:CATEGORY "RUSH" :TEXT "Nunca permitas que un objeto mecánico sepa que tienes prisa" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SAIDDONE" :TEXT "«It's easier said than done.»
    … and if you don't believe it, try proving that it's easier done than
    said, and you'll see that «it's easier said that `it's easier done than
    said' than it is done», which really proves that «it's easier said than
    done»" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SCIENCE" :TEXT "Basic Definitions of Science:
    If it's green or wiggles, it's biology.
    If it stinks, it's chemistry.
    If it doesn't work, it's physics":AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SIMPLICITY" :TEXT "Simplicity and complexity need each other." :AUTHOR "John Maeda" :ITEM "" :DATE "")
 (:CATEGORY "YOU" :TEXT "Don't hate yourself in the morning, sleep till noon" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SOFTWARE" :TEXT "Great software requires a fanatical devotion to beauty." :AUTHOR "Paul Graham" :ITEM "" :DATE "")
 (:CATEGORY "SOFTWARE" :TEXT "1. Every good work of software starts by scratching a developer's personal itch.
    2. Good programmers know what to write. Great ones know what to rewrite(and reuse).
    3. «Plan to throw one away; you will, anyhow.»(Fred Brooks, The Mythical Man-Month, Chapter 11))
       Or, to put it another way, you often don't really understand the problem until after the first time you implement a solution.
    4. If you have the right attitude, interesting problems will find you.
    5. When you lose interest in a program, your last duty to it is to hand it off
    to a competent successor.
    6. Treating your users as co-developers is your least-hassle route to rapid code improvement and effective debugging.
    7. Release early. Release often. And listen to your customers.
    8. Given a large enough beta-tester and co-developer base, almost every problem
    will be characterized quickly and the fix obvious to someone" :AUTHOR "Eric Steven Raymond" :ITEM "The Cathedral and the Bazaar" :DATE "")
 (:CATEGORY "SOFTWARE" :TEXT "There are two ways of constructing a software design; one way is to make it so simple that there are obviously no deficiencies, and the other way is to make it so complicated that there are no obvious deficiencies. The first method is far more difficult." :AUTHOR "C. A. R. Hoare" :ITEM "" DATE "")
 (:CATEGORY "SONG" :TEXT "Brighter than a thousand suns." :AUTHOR "Judas Priest" :ITEM "Painkiller" :DATE "")
 (:CATEGORY "SONG" :TEXT "Youth's like diamonds in the sun. And diamonds are forever." :AUTHOR "Alphaville" :ITEM "Forever Young" :DATE "")
 (:CATEGORY "SONG" :TEXT "A man who casts no shadow has no soul." :AUTHOR "Iron Maiden" :ITEM "Out of the shadows" :DATE "")
 (:CATEGORY "SONG" :TEXT "Nothing new it's the same old shit. If it works this good why fuck with it" :AUTHOR "KMFDM" :ITEM "WWIII" :DATE "")
 (:CATEGORY "SPEEDOFLIGHT" :TEXT "Professor Hubert Farnsworth: These are the dark matter engines I invented.
                         They allow my starship to travel between galaxies in mere hours.
                         Cubert J. Farnsworth: That's impossible. You can't go faster than the speed of light.
                         Professor Hubert Farnsworth: Of course not. That's why scientists increased the speed of light in 2208.
                         Cubert J. Farnsworth: Also impossible
                         Professor Hubert Farnsworth: And what makes my engines truly remarkable is the afterburner, which delivers 200% fuel efficiency.
                         Cubert J. Farnsworth: That's especially impossible.
                         Professor Hubert Farnsworth: Not at all. It's very simple.
                         Cubert J. Farnsworth: Then explain it.
                         Professor Hubert Farnsworth: Now that's impossible! It came to me in a dream, and I forgot it in another dream." :AUTHOR "Futurama" :ITEM "" :DATE "")
 (:CATEGORY "STANDARDS" :TEXT "The nice thing about standards is that you have so many to choose from." :AUTHOR "Andrew S. Tanenbaum" :ITEM "" :DATE "")
 (:CATEGORY "STUDENT" :TEXT "Advice to students: Leap in and try things. If you succeed, you can have enormous influence. If you fail, you have still learned something, and your
    next attempt is sure to be better for it.
    Advice to graduates: Do something you really enjoy doing. If it isn't fun to get up in the morning and do your job or your school program, you're in the wrong field." :AUTHOR "Brian Kernighan" :ITEM "" :DATE "")
 (:CATEGORY "SUM" :TEXT "1 + 1 = 3 for large values of 1." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SYSTEMS" :TEXT "To the systems programmer, users and applications serve only to provide a test load" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TAO" :TEXT "El ser no hace, es." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TAO" :TEXT "The highest virtue is to act without a sense of self
    The highest kindness is to give without a condition
    The highest justice is to see without a preference
    When Tao is lost one must learn the rules of virtue
    When virtue is lost, the rules of kindness
    When kindness is lost, the rules of justice
    When justice is lost, the rules of conduct")
 (:CATEGORY "TAO" :TEXT "Un hombre de verdad es capaz de llevar lo importante dentro de sí mismo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TAO" :TEXT "- Maestro, no soy capaz de encontrar la paz interior.
    - ¿Cuál es el motivo?- interrogó el maestro.
    - Lo ignoro. Por eso estoy aquí, buscando tu sabiduría y consejo.
    El maestro quedo pensativo unos instantes y dijo:
    -Vas a ir ahora mismo al cementerio. Allí­ te sentarás en medio de las tumbas y pasarás la mañana elevando toda suerte de elogios a los muertos.
    El discípulo obedeció y, una vez que hubo cumplido la tarea, regresó.
    - ¿Has hecho lo que te dije? - preguntó el maestro.
    - Sí lo he hecho- respondió el estudiante.
    - Bien; pues ahora volverás al cementerio y pasarás la tarde vertiendo insultos e injurias a los muertos.
    El discípulo volvió a cumplir la orden del maestro.
    Llegada la noche, regresó de nuevo.
    - Maestro, durante la mañana he ensalzado las virtudes de los muertos con toda clase de elogios, pero por la tarde he ofendido gravemente a esos mismos muertos con grandes insultos. ¿Puedes decirme ahora el objetivo de tus mandatos?
    - ¿Qué te contestaron los muertos? - preguntó a su vez el maestro-
    - ¿No se mostraron satisfechos y se vanagloriaron con tus alabanzas?, ¿Tal vez se volvieron indignados y coléricos con tus insultos?
    - Pero maestro, eso no es posible. ¿Cómo van a reaccionar si están muertos?
    - Pues eso es exactamente lo que has de esperar de ti mismo: la ausencia de reacciones, tanto ante las ofensas como ante las alabanzas. Si alguien te insulta y enciende tu cólera, ¿No ves el poder que tiene sobre ti? Si alguien te alaba e inflama tu vanidad, ¿No ves el poder que tiene sobre ti? Tu paz interior la tienes ahora en manos de los demás o en poder de los acontecimientos que te rodean. Ve y rompe esas cadenas, recupera tu libertad y entonces encontrarás la paz interior" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TAO" :TEXT "Everything changes" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TAO" :TEXT "Once upon a time there was an old farmer who had worked his crops for many years. One day his horse ran away. Upon hearing the news, his neighbors came to visit. Such bad luck, they said sympathetically.
    Maybe, the farmer replied.
    The next morning the horse returned, bringing with it three other wild horses. How wonderful, the neighbors exclaimed.
    Maybe, replied the old man.
    The following day, his son tried to ride one of the untamed horses, was thrown, and broke his leg. The neighbors again came to offer their sympathy on his misfortune.
    Maybe, answered the farmer.
    The day after, military officials came to the village to draft young men into the army. Seeing that the son's leg was broken, they passed him by. The neighbors congratulated the farmer on how well things had turned out.
    Maybe, said the farmer" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TECHNOLOGY" :TEXT "I would advise students to pay more attention to the fundamental ideas rather than the latest technology. The technology will be out-of-date before they graduate. Fundamental ideas never get out of date." :AUTHOR "Dr. David Lorge Parnas" :ITEM "" :DATE "")
 (:CATEGORY "TECHNOLOGY" :TEXT "Technology is dominated by two types of people: those who understand what they do not manage, and those who manage what they don't understand" :AUTHOR "" ITEM "Putt's law" :DATE "")
 (:CATEGORY "TESTING" :TEXT "Program testing can be used to show the presence of bugs, but never to show their absence!." :AUTHOR "Edsger W. Dijkstra" :ITEM "" DATE "")
 (:CATEGORY "THEOREM" :TEXT "Theorem: All positive integers are equal.
    Proof: Sufficient to show that for any two positive integers, A and B, A = B.
    Further, it is sufficient to show that for all N > 0, if A and B
   (positive integers) satisfy (MAX(A, B) = N) then A = B.
    Proceed by induction:
       If N = 1, then A and B, being positive integers, must both be 1.
           So A = B.
       Assume that the theorem is true for some value k. Take A and B with
           MAX(A, B) = k+1. Then MAX((A-1),(B-1)) = k. And hence
             (A-1) = (B-1). Consequently, A = B" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "THEORY" :TEXT "Theory and practice are the same in theory, but different in practice" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "THEORY" :TEXT "Una teoría pasa por cuatro etapas antes de ser aceptada:
I. esto es un sinsentido sin ningún valor;
II. es interesante, pero perversa;
III. esto es cierto, pero no tiene ninguna importancia;
IV. yo siempre lo dije" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "THING" :TEXT "If a thing is worth doing, it is worth doing badly." :AUTHOR "G.K. Chesterton" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "You need more time; and you probably always will" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TODAY" :TEXT "Today is what happened to yesterday" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TRAVEL" :TEXT "A good traveler has no fixed plans and is not intent on arriving." :AUTHOR "Lao Tzu")
 (:CATEGORY "TURING" :TEXT "We may hope that machines will eventually compete with men in all purely
    intellectual fields. But which are the best ones to start with? Many people
    think that a very abstract activity, like the playing of chess, would be
    best. It can also be maintained that it is best to provide the machine with
    the best sense organs that money can buy, and then teach it to understand
    and speak English." :AUTHOR "Alan M. Turing" :ITEM "" DATE "")
 (:CATEGORY "UNIVERSE" :TEXT "Los astrónomos aseguran que el universo es finito, lo cual es reconfortante para aquellos que no podemos recordar donde dejamos las cosas." :AUTHOR "Frank Zappa" ITEM "" :DATE "")
 (:CATEGORY "UNIVERSE" :TEXT "Solo hay dos cosas infinitas: el universo y la estupidez humana. No se cual va primero." :AUTHOR "Albert Einstein" :ITEM "" DATE "")
 (:CATEGORY "UNIVERSE" :TEXT "Hay una teoría que afirma que si alguna vez alguien descubre exactamente para que es el Universo y por que existe, este desaparecería instantáneamente y seria reemplazado por algo mucho mas raro e inexplicable. Hay otra teoría que afirma que esto ya ha sucedido." :AUTHOR "Douglas Adams" :ITEM "" :DATE "")
 (:CATEGORY "VIOLENCE" :TEXT "Violencia activa
    Cuando alguien (o una institución) inflige o causa un mal mediante coacción física o moral a una persona o grupo de personas.
    Por ejemplo, golpear a alguien o no pagarle el salario debido a un trabajo realizado, etc.
    Las agresiones a los derechos humanos fundamentales suelen ir acompañadas de violencia activa.
    Violencia pasiva
    La recibe la persona o grupo de personas perjudicadas por la acción violenta de otros.
    Por ejemplo, el agredido por ser de otra etnia, o el trabajador a quien no se le paga su sueldo.
    Les es lícito a los agredidos o receptores de la violencia activa repelerla y rechazarla con medios adecuados y proporcionados al mal recibido y por los cauces adecuados(para lo cual tienen que existir esos cauces y funcionar adecuadamente, pues de otro modo obligarán a los perjudicados a tomarse la justicia por su mano).
    No violencia activa (método utilizado por Gandhi, Luther King, Mandela, Oscar Romero, …))
    Consiste en expresar con acciones, el rechazo de la injusticia desobedeciendo las leyes o la situación injusta, y aceptando la violencia que por su actitud se les hace sin recurrir ellos a la misma. El ejemplo paradigmático es el de Gandhi quien, ante la prohibición de Inglaterra de que los indios recogiesen sal, organizó millonarias marchas de indios al mar para recoger sal, aceptando impasiblemente las palizas que les iban dando los soldados británicos; acciones así consiguieron la independencia de la India." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "Engineering: «How will this work?»
                   Science: «Why will this work?»
                   Management: «When will this work?»
                   Liberal Arts: «Do you want fries with that?»." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "Work is of two kinds: first, altering the position of matter at or near the earth's surface relative to other matter; second, telling other people to do so." :AUTHOR "Bertrand Russell")
 (:CATEGORY "WORK" :TEXT "El trabajo mejor hecho es el que pasa desapercibido." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "Night time is really the best time to work. All the ideas are there to be yours because everyone else is asleep." :AUTHOR "Catherine O'Hara" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "It's much easier to think for 8 hours and work for 2, than the opposite." :AUTHOR "Mirko Ilic)")
 (:CATEGORY "WORK6" :TEXT "It takes a lot of effort to make something seem effortless." :AUTHOR "Alexander Isley")
 (:CATEGORY "WORLD" :TEXT "It'll be a nice world if they ever get it finished" :AUTHOR "")
 (:CATEGORY "WORLD" :TEXT "In a world without fences and walls nobody needs gates and windows." :AUTHOR "")
 (:CATEGORY "WORLD" :TEXT "The world is a tragedy to those who feel, but a comedy to those who think." :AUTHOR "Horace Walpope")
 (:CATEGORY "XML" :TEXT "Some languages can be read by human, but not by machines, while others can be read by machines but not by humans. XML solves this problem by being readable to neither")
 (:CATEGORY "XML" :TEXT "XML is like violence: if it doesn't solve your problem, you aren't using enough of it." :AUTHOR "Chris Maden")
 (:CATEGORY "YOU" :TEXT "No matter where you go, there you are." :AUTHOR "Buckaroo Banzai")
 (:CATEGORY "ZEN" :TEXT "Nan-in, a Japanese master during the Meiji era (1868-1912), received a university professor who came to inquire about Zen.
    Nan-in served tea. He poured his visitor's cup full, and then kept on pouring.
    The professor watched the overflow until he no longer could restrain himself. «It is overfull. No more will go in!»
    «Like this cup,» Nan-in said, «you are full of your own opinions and speculations. How can I show you Zen unless you first empty your cup?»" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ZEN" :TEXT "When Bankei held his seclusion-weeks of meditation, pupils from many parts of Japan came to attend. During one of these gatherings a pupil was caught stealing. The matter was reported to Bankei with the request that the culprit be expelled. Bankei ignored the case.
    Later the pupil was caught in a similar act, and again Bankei disregarded the matter. This angered the other pupils, who drew up a petition asking for the dismissal of the thief, stating that otherwise they would leave in a body.
    When Bankei had read the petition he called everyone before him. You are wise brothers, he told them. You know what is right and what is not right. You may go somewhere else to study if you wish, but this poor brother does not even know right from wrong. Who will teach him if I do not? I am going to keep him here even if all the rest of you leave.
    A torrent of tears cleansed the face of the brother who had stolen. All desire to steal had vanished." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CREATIVITY" :TEXT "The secret to creativity is knowing how to hide your sources." :AUTHOR "Albert Einstein" :ITEM "" :DATE "")
 (:CATEGORY "EXPERIMENT" :TEXT "Take wrong turns. Talk to strangers. Open unmarked doors. And if you see a group of people in a field, go find out what they are doing. Do things without always knowing how they’ll turn out." :AUTHOR "Randall Munroe" :ITEM "" :DATE "")
 (:CATEGORY "STUPIDITY" :TEXT "Stupidity is its own reward." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "POSSIBLE" :TEXT "Nothing is impossible, the word itself says «I'm possible»!." :AUTHOR "Audrey Hepburn" :ITEM "" :DATE "")
 (:CATEGORY "DEBUG" :TEXT "The most effective debugging tool is still careful thought, coupled with judiciously placed print statements." :AUTHOR "Brian Kernighan" :ITEM "Unix for Begginers" :DATE "1979")
 (:CATEGORY "DOCTOR" :TEXT "«I keep seeing spots in front of my eyes» «Did you ever see a doctor?» «No, just spots»." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SILENCE" :TEXT "Silence is the only virtue you have left." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HAPPY" :TEXT "If you are happy you are successful." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "EARTH" :TEXT "And yet it moves." :AUTHOR "Galileo Galilei" :ITEM "" :DATE "")
 (:CATEGORY "ABSINTHE" :TEXT "After the first glass of absinthe you see things as you wish they were. After the second you see them as they are not. Finally you see things as they really are, and that is the most horrible thing in the world. I mean disassociated. Take a top hat. You think you see it as it really is. But you don’t because you associate it with other things and ideas.If you had never heard of one before, and suddenly saw it alone, you’d be frightened, or you’d laugh. That is the effect absinthe has, and that is why it drives men mad. Three nights I sat up all night drinking absinthe, and thinking that I was singularly clear-headed and sane. The waiter came in and began watering the sawdust.The most wonderful flowers, tulips, lilies and roses, sprang up, and made a garden in the cafe. «Don’t you see them?» I said to him. «Mais non, monsieur, il n’y a rien»." :AUTHOR "Oscar Wilde" :ITEM "" :DATE "")
 (:CATEGORY "END" :TEXT "Alles hat ein Ende nur die Wurst hat zwei (Krause & Ruth)." :AUTHOR "Stephan Remmler" :ITEM "" :DATE "1986")
 (:CATEGORY "DEBUG" :TEXT "If debugging is the process of removing bugs, then programming must be the process of putting them in." :AUTHOR "Edsger Dijkstra" :ITEM "" :DATE "")
 (:CATEGORY "MUSIC" :TEXT "If you don't like where we're going. Then you won't like what's coming next." :AUTHOR "Megadeth" :ITEM "Post American World" :DATE "2016")
 (:CATEGORY "MAGIC" :TEXT "Sometimes magic is just someone spending more time on something than anyone else might reasonably expect." :AUTHOR "Teller" :ITEM "" :DATE "")
 (:CATEGORY "ARMY" :TEXT "Join the army, see the world, meet interesting, exciting people, and kill them." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "He who seeks rest finds boredom. He who seeks work finds rest." :AUTHOR "Dylan Thomas" :ITEM "" :DATE "")
 (:CATEGORY "FENCE" :TEXT "Don't ever take a fence down until you know why it was put up." :AUTHOR "Robert Frost" :ITEM "" :DATE "")
 (:CATEGORY "ENCODING" :TEXT "Prof: So the American government went to IBM to come up with a data encryption standard and they came up with… Student: EBCDIC!." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SOFTWARE" :TEXT "The only thing more expensive than writing software is writing bad software." :AUTHOR "Alan Cooper" :ITEM "" :DATE "")
 (:CATEGORY "LEARN" :TEXT "You have to finish things — that's what you learn from, you learn by finishing things." :AUTHOR "Neil Gaiman" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "The greatest single programming language ever designed." :AUTHOR "Alan Kay" :ITEM "On Lisp" :DATE "")
 (:CATEGORY "LISP" :TEXT "Lisp is worth learning for the profound enlightenment experience you will have when you finally get it; that experience will make you a better programmer for the rest of your days, even if you never actually use Lisp itself a lot." :AUTHOR "Eric Raymond" :ITEM "How to become a hacker" :DATE "")
 (:CATEGORY "LISP" :TEXT "Within a couple weeks of learning Lisp I found programming in any other language unbearably constraining." :AUTHOR "Paul Graham" :ITEM "Road to Lisp" :DATE "")
 (:CATEGORY "LISP" :TEXT "Lisp is the most sophisticated programming language I know. It is literally decades ahead of the competition … it is not possible (as far as I know) to actually use Lisp seriously before reaching the point of no return." :AUTHOR "Christian Lynbech" :ITEM "Road to Lisp" :DATE "")
 (:CATEGORY "LISP" :TEXT "Greenspun's Tenth Rule of Programming: any sufficiently complicated C or Fortran program contains an ad hoc informally-specified bug-ridden slow implementation of half of Common Lisp." :AUTHOR "Philip Greenspun" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "We were not out to win over the Lisp programmers; we were after the C++ programmers. We managed to drag a lot of them about halfway to Lisp. Aren't you happy?." :AUTHOR "Guy Steele" :ITEM "LL1 mailing list" :DATE "2003")
 (:CATEGORY "LISP" :TEXT "Lisp has jokingly been called «the most intelligent way to misuse a computer» I think that description is a great compliment because it transmits the full flavor of liberation: it has assisted a number of our most gifted fellow humans in thinking previously impossible thoughts." :AUTHOR "Edsger Dijkstra" :ITEM "CACM, 15:10" :DATE "")
 (:CATEGORY "LISP" :TEXT "Historically, languages designed for other people to use have been bad: Cobol, PL/I, Pascal, Ada, C++. The good languages have been those that were designed for their own creators: C, Perl, Smalltalk, Lisp." :AUTHOR "Paul Graham" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "Lisp… made me aware that software could be close to executable mathematics." :AUTHOR "L. Peter Deutsch" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "Lisp is a programmable programming language." :AUTHOR "John Foderaro" :ITEM "CACM" :DATE "September 1991")
 (:CATEGORY "LISP" :TEXT "Will write code that writes code that writes code that writes code for money." :AUTHOR "Anonymous" :ITEM "comp.lang.lisp" :DATE "")
 (:CATEGORY "LISP" :TEXT "I object to doing things that computers can do." :AUTHOR "Olin Shivers" :ITEM "" :DATE "")
 (:CATEGORY "Lisp" :TEXT "Lisp is a language for doing what you've been told is impossible." :AUTHOR "Ken Pitman" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "Anyone could learn Lisp in one day, except that if they already knew Fortran, it would take three days." :AUTHOR "Marvin Minsky" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "Programming in Lisp is like playing with the primordial forces of the universe. It feels like lightning between your fingertips. No other language even feels close." :AUTHOR "Glenn Ehrlich" :ITEM "Road to Lisp" :DATE "")
 (:CATEGORY "LISP" :TEXT "Lisp is the red pill." :AUTHOR "John Fraser" :ITEM "comp.lang.lisp" :DATE "")
 (:CATEGORY "LISP" :TEXT "The language God would have used to implement the Universe." :AUTHOR "Svein Ove Aas" :ITEM "comp.lang.lisp" :DATE "")
 (:CATEGORY "LISP" :TEXT "Lisp doesn't look any deader than usual to me." :AUTHOR "David Thornley" :ITEM "Reply to a question older than most programming languages" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "Don't worry about what anybody else is going to do. The best way to predict the future is to invent it." :AUTHOR "Alan Kay" :ITEM "" :DATE "")
 (:CATEGORY "SPACE" :TEXT "I don't know what you can say about a day when you see four beautiful sunsets… This is a little unusual, I think." :AUTHOR "John Glenn" :ITEM "" :DATE "")
 (:CATEGORY "EXPERT" :TEXT "The definition of an expert is someone who knows what not to do." :AUTHOR "Charles Willson" :ITEM "" :DATE "")
 (:CATEGORY "NICENESS" :TEXT "Mom always tod me if you can't say anything nice, then don't say anything at all. And some people wonder why I'm so quiet around them." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "Real programmers don't write in BASIC. Actually, no programmers write in BASIC after reaching puberty." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PRODUCTIVITY" :TEXT "Nothing makes a person more productive than the last minute." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SIMPLICITY" :TEXT "The trouble with so many of us is that we underestimate the power of simplicity." :AUTHOR "Robert Stuberg" :ITEM "" :DATE "")
 (:CATEGORY "MEN" :TEXT "Men have become tools of their tools." :AUTHOR "Henry David Thoreau" :ITEM "" :DATE "")
 (:CATEGORY "LETTERS" :TEXT "I'm sorry I wrote you such a long letter; I didn't have time to write a short one." :AUTHOR "Blaise Pascal" :ITEM "" :DATE "")
 (:CATEGORY "COMPUTERS" :TEXT "At the source of every error which is blamed on the computer you will find at least two human errors, including the error of blaming it on the computer." :AUTHOR "Anonymous" :ITEM "" :DATE "")
 (:CATEGORY "THEORY" :TEXT "In theory, there is no difference between theory and practice. But, in practice, there is." :AUTHOR "Jan L. A. van de Snepscheut" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT " Life is like a ten speed bicycle. Most of us have gears we never use." :AUTHOR "Charles M. Schulz" :ITEM "" :DATE "")
 (:CATEGORY "LAW" :TEXT "Almost everything in life is easier to get into than out of." :AUTHOR "Agnes' Law" :ITEM "" :DATE "")
 (:CATEGORY "SUCCESS" :TEXT "A person’s success in life can usually be measured by the number of uncomfortable conversations he or she is willing to have." :AUTHOR "Tim Ferriss" :ITEM "" :DATE "")
 (:CATEGORY "YES" :TEXT "No more yes. It’s either HELL YEAH! or no." :AUTHOR "Derek Sivers" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "You know that you are not a good programmer when you don't see any problems that need to be solved." :AUTHOR "Barry Jones" :ITEM "" :DATE "")
 (:CATEGORY "RELIGION" :TEXT "All religions are founded on the fear of the many and the cleverness of the few." :AUTHOR "Stendhal" :ITEM "" :DATE "")
 (:CATEGORY "SOFTWARE" :TEXT " Reading computer manuals without the hardware is as frustrating as reading sex manuals without the software." :AUTHOR "Arthur C. Clarke" :ITEM "" :DATE "")
 (:CATEGORY "ACTIONS" :TEXT "You have to recognize that every out-front maneuver you make is going to be uncomfortable. That warm sense of everything going well is usually the body temperature at the center of the herd." :AUTHOR "Irv Grousbeck" :ITEM "" :DATE "")
 (:CATEGORY "MUSIC" :TEXT "1000 years later, on a desolated Planet called Earth, a technomancer tribe finds the remnants of a 1990s Hi-Fi System with an intact CD Inside, labeled «Judas Priest - Painkiller» After fixing the Stereo System with parts of ancient tech & with the help of the «Machine God» the moment comes, when the high Tech-Priest, finally presses the «Play» button on the machine. A moment of Silence, with over 100 eyes fixated with marvel on this ancient piece of technology and what magic it might behold… before the roaring sound of Judas fucking Priest, shakes the world again, after over 1000 years. A moment one can only compare to, the «Dawn of Man» scene, in Kubrick's «2001 Space Odyssey»." :AUTHOR "twistedmeta04" :ITEM "" :DATE "07-2017")
 (:CATEGORY "LIVE" :TEXT "You can only be afraid of what you think you know." :AUTHOR "Jiddu Krishnamurti" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "It's not about ideas. It's about making ideas happen." :AUTHOR "Scott Belsky" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "You only live once, but if you do it right, once is enough." :AUTHOR "Mae West" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Never fear starting. Fear never starting." :AUTHOR "Ryan Lilly" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "; ALL USERS PLEASE NOTE
;
; There has been some confusion concerning MAPCAR.
(DEFUN MAPCAR(&FUNCTIONAL FCN &EVAL &REST LISTS)
 (PROG (V P LP)
       (SETQ P (LOCF V))
        L(SETQ LP LISTS)
       (%START-FUNCTION-CALL FCN T (LENGTH LISTS) NIL)
        L1(OR LP (GO L2))
       (AND (NULL (CAR LP)) (RETURN V))
       (%PUSH (CAAR LP))
       (RPLACA LP (CDAR LP))
       (SETQ LP (CDR LP))
       (GO L1)
        L2(%FINISH-FUNCTION-CALL FCN T (LENGTH
        LISTS) NIL)
       (SETQ LP (%POP))
       (RPLACD P (SETQ P (NCONS LP)))
       (GO L)))
; We hope this clears up the many questions we've had about it." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISP" :TEXT "Tiger got to hunt,
Bird got to fly;
Lisper got to sit and wonder,(Y (Y Y))?

Tiger got to sleep,
Bird got to land;
Lisper got to tell himself he understand." :AUTHOR "Kurt Vonnegut, modified by Darius Bacon" :ITEM "" :DATE "")
 (:CATEGORY "RETIREMENT" :TEXT "Si haces esto, mucha gente te llamará un 'derrotista' o un 'agorero', o sostendrá que estás 'quemado'. Te dirán que tienes la obligación de trabajar por la justicia climática o por la paz mundial o por el fin de las desgracias en todas partes, y que 'luchar' es siempre mejor que 'rendirse'. Ignóralos y participa de una tradición práctica y espiritual muy antigua: retirarse de la refriega. Retírate no con cinismo, sino con una mente indagadora. Retírate para poder sentarte tranquilamente y sentir, intuir, averiguar lo que es bueno para ti y lo que la naturaleza puede necesitar de ti. Retírate porque negarse a contribuir a que la máquina avance (negarse a apretar más el trinquete) es una posición profundamente moral. Retírate porque la acción no siempre es más efectiva que la inacción. Retírate para examinar tu visión del mundo: la cosmología, el paradigma, los supuestos, la dirección del viaje. Todo cambio real empieza con la retirada." :AUTHOR "Kingdnorth" :ITEM "Dark Ecology" :DATE "")
 (:CATEGORY "KINGDOM" :TEXT "Más allá de donde aún se esconde la vida, queda un reino, queda cultivar como un rey su agonía, hacer florecer como un reino la sucia flor de la agonía: yo que todo lo prostituí, aún puedo prostituir mi muerte y hacer de mi cadáver el último poema." :AUTHOR "Leopoldo María Panero" :ITEM "Dedicatoria" :DATE "")
 (:CATEGORY "PRIE" :TEXT "Ángeles de oro vestidos, de púrpura y de jacinto.
            El genio y el amor son fáciles deberes.
            Amasé sólo barro y de él extraje oro
            Llevaba en la mirada el brío del corazón.
            En París, su desierto, viviendo a la intemperie,
            Fuerte como una bestia y libre como un Dios." :AUTHOR "Charles Baudelaire" :ITEM "Orgullo" :DATE "")
 (:CATEGORY "POWER" :TEXT "Cuando el arquero dispara sin buscar un premio, tiene toda su destreza cuándo dispara por una medalla de bronce se pone nervioso cuándo dispara por una presa de oro enceguece ve dos blancos y está fuera de sí. Su destreza no ha cambiado pero el premio lo divide. ¡Le importa! piensa más en ganar en disparar. Y la necesidad de ganar le quita su poder." :AUTHOR "Tranxu" :ITEM "" :DATE "")
 (:CATEGORY "LOSS" :TEXT "A todo lo que he perdido, gracias por dejarme libre." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MOMENT" :TEXT "A veces nunca sabrás el valor de un momento hasta que se convierta en un gran recuerdo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DREAM" :TEXT "A veces sueño con volver a ser el que era cuando soñaba con llegar a ser el que soy." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SUCCESS" :TEXT "Al éxito y al fracaso, esos dos impostores, trátalos siempre con la misma indiferencia." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SERENITY" :TEXT "Alcanza la serenidad cooperando incondicionalmente con lo inevitable." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIVING" :TEXT "Antes de arriesgarte a vivir con alguien aprende a vivir contigo mismo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TIME" :TEXT "Aprende a apreciar lo que tienes antes de que el tiempo te haga apreciar lo que tenías." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "GREED" :TEXT "Avaricia y derroche, dos puntas de un mismo error." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "GOD" :TEXT "Busqué a Dios en la oración y no lo encontré, me busqué a mi mismo en la meditación y no me encontré, busqué a mi hermano y encontré a los tres." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FOREVER" :TEXT "Creo que todos estamos de acuerdo en que nada es para siempre pero lo que se cuida dura un poco más." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CELL" :TEXT "Cuando el monje va a la taberna la taberna se convierte en su celda. Cuando el borracho va a la celda la celda se convierte en su taberna." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MOON" :TEXT "Cuando el sabio señala la luna el idiota no ve sino el dedo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROBLEM" :TEXT "Cuando ella y tú estén peleando deben recordar que son ustedes dos contra el problema no tú contra ella ni ella contra tí." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "RELATIONS" :TEXT "Cuando les preguntaron como hacían para permanecer 65 años juntos dijeron. Nacimos en un tiempo en el que cuando algo se rompía lo arreglábamos, no lo tirábamos a la basura." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MOUTH" :TEXT "Cuida tu boca. Observa minuciosamente lo que entra y lo que sale de ella." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "NIÑOS" :TEXT "Dios habla por la boca de los niños." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Dios teje tapices perfectos con los hilos de nuestras vidas incluidos nuestros pecados. Si no somos capaces de verlo es porque estamos mirando la otra cara del tapiz." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ERROR" :TEXT "Donde hay humildad para aceptar el error hay inteligencia para aprender de él." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FISHING" :TEXT "El «malo» te roba el pescado. El «bueno» te regala el pescado. El sabio te enseña a pescar, si se lo pides." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "El amor no es emoción sino acción." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "El amor nunca es de mal gusto, el castigo sí." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PATH" :TEXT "El camino, ese gran desconocido." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "NEEDS" :TEXT "El cuerpo necesita descanso. La mente necesita paz. El corazón necesita alegrías." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MONEY" :TEXT "El dinero y los cojones, para las ocasiones." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PAIN" :TEXT "El dolor es inevitable pero el sufrimiento es opcional." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ERROR" :TEXT "El error es fuente de crecimiento no de tortura." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "El futuro no está esperando que llegues a él si no esperando a que lo fabriques." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SLAVE" :TEXT "El mejor esclavo es aquel que no sabe que lo es y ama su esclavitud." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FEAR" :TEXT "El miedo es un obstáculo mayor que el obstáculo en sí." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FEAR" :TEXT "Inténtalo una y otra vez hasta que el miedo te tenga miedo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORLD" :TEXT "El mundo no es grande ni pequeño. Somos cada uno de nosotros los que definimos su tamaño." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HEAD" :TEXT "El que no tiene cabeza tiene que tener pies." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DEAF" :TEXT "El sordo siempre cree que los que bailan están locos." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SUICIDE" :TEXT "El suicidio es una solución permanente a un problema temporal." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "El tiempo que pasa uno riendo es tiempo que pasa con los dioses." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "El trabajo es la maldición de la clase bebedora." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "El truco de la vida consiste en morir joven pero lo más tarde posible." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SUCCESS" :TEXT "El éxito es la habilidad de ir de fracaso en fracaso con entusiasmo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "En dos palabras puedo resumir cuanto he aprendido de la vida: Sigue adelante." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SYSTEM" :TEXT "En un sistema hipócrita ser sincero es ser hipócrita." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "POSSIBLE" :TEXT "Existe mucha diferencia entre hacer lo posible y hacerlo posible." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DEMOCRACY" :TEXT "Existen tantas definiciones de democracia como autores." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LISTEN" :TEXT "Hablar es una necesidad. Escuchar es un arte." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "Hay mucha diferencia entre: Vamos a ver si funciona y vamos a hacer que funcione." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DO" :TEXT "Haz algo bueno hoy o cállate." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "INNER" :TEXT "Humor y amor ahuyentan el temor y revelan lo más grande y hermoso que se encuentra en tu interior." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TRUST" :TEXT "La confianza no es saber todo del otro, es no necesitar saberlo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DEPRESSION" :TEXT "La depresión también tiene sus adversarios, la pasión, la paciencia y el perdón . Ilusión, coraje y esperanza son sus vacunas." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La diferencia entre la escuela y la vida es que en la escuela primero aprendes una lección y luego te ponen una prueba y en la vida primero te envían una prueba y luego aprendes la lección." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "EXPERIENCE" :TEXT "La experiencia es el peine que nos da la vida cuando estamos calvos." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MIND" :TEXT "La mente crea abismos que el corazón puede saltar." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "EATING" :TEXT "La mesura en el comer da más vida que placer." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORD" :TEXT "La palabra y la saeta una vez lanzadas no vuelven al arco." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ONTIME" :TEXT "La puntualidad no consiste solo en llegar a tiempo, también en marcharse a tiempo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "RELIGION" :TEXT "La religión es para aquellos que tienen miedo de ir al infierno. La espiritualidad es para quienes ya han estado allí." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LUCK" :TEXT "La suerte no es más que el meditado cuidado de todos los detalles." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La vida es un arco iris que incluye el negro." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La vida se teje con hilos de experiencias. Si no le encontramos sentido o lo que obtenemos no es de nuestro agrado es porque estamos mirando la otra cara del tapiz." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "La única cosa que puedes cambiar en una persona es tu forma de verla." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "IDEAS" :TEXT "Las ideas son como las pulgas, saltan de cráneo a cráneo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LOAD" :TEXT "Las personas no están solas porque no tengan a nadie con quién compartir su carga si no porque es su carga lo único que pueden compartir." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MIND" :TEXT "Las puertas de la mente solo se abren desde dentro." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Lo contrario del amor no es el odio sino la apatía." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WISDOM" :TEXT "Los hombres sabios hablan porque tienen algo que decir. Los hombres tontos porque tienen que decir algo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SORROW" :TEXT "Merece la pena quien te la quite." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ENEMY" :TEXT "Mi enemigo me dijo: Ama a tu enemigo y yo le obedecí y me amé a mi mismo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIES" :TEXT "Nadie tiene más posibilidades de caer en un engaño que aquel a quien la mentira se ajusta a sus deseos." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PAST" :TEXT "Nadie tiene tan limpio el pasado como para poder juzgar el tuyo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MULE" :TEXT "No busques tu mula por el pueblo cuando vas cabalgando sobre ella." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DISCOURAGEMENT" :TEXT "No dejes que nada te desanime porque hasta una patada en el culo te empuja hacia adelante." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CHANGE" :TEXT "No es el cambio lo que produce dolor sino la resistencia a él." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ADAPTATION" :TEXT "No es saludable estar bien adaptado a una sociedad profundamente enferma." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "No esperes al futuro, él no te está esperando a ti." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ROOTS" :TEXT "No permitas que tus raíces estén confinadas en una maceta." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ARROGANCE" :TEXT "No seas arrogante con el humilde ni humilde con el arrogante." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "No te tomes la vida en serio, al fin y al cabo no saldrás vivo de ella." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "REALITY" :TEXT "O somos víctimas de nuestra realidad o somos sus creadores." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Piensa como un adulto. Vive como un joven. Da consejos como un anciano y sueña como un niño." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "BRAIN" :TEXT "Ponga el cerebro en funcionamiento antes de poner la lengua en movimiento." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FIGHT" :TEXT "Prepárate para luchar sin herir y para vencer sin humillar porque el único rival eres tú." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "KID" :TEXT "Procura que el niño que fuiste no se avergüence del adulto que eres." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PERSON" :TEXT "Procura ser el tipo de persona que te gustaría conocer." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HUNGER" :TEXT "Que no te mate lo que mata tu hambre." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PATH" :TEXT "Quien cede el paso ensancha el camino." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MONEY" :TEXT "Sacrificamos nuestro precioso tiempo para hacer dinero. Sacrificamos nuestro dinero duramente ganado para luchar contra el tiempo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MISS" :TEXT "Se equivoca quien no se equivoca." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ATTENTION" :TEXT "Si algo no te agrada quítale el único poder que tiene: Tu atención." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "NEED" :TEXT "Si antes de verlo no lo necesitabas, no lo necesitas." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TRUTH" :TEXT "Si dices la verdad eso será parte de tu pasado. Si mientes eso será parte de tu futuro." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "KILL" :TEXT "Si el insecticida mata insectos. ¿Qué mata la publicidad?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "STUPID" :TEXT "Si miras unos años atrás y no estás impactado por lo estúpido que eras, no has aprendido nada." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "GO" :TEXT "Si no sabes dónde vas regresa para saber de dónde vienes." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Si nos convertimos en sembradores de amor. ¿Te imaginas la cosecha?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HEAL" :TEXT "Si nunca sanas lo que te hirió, sangrarás sobre personas que nunca te cortaron." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "AGE" :TEXT "Si quieres llegar a viejo guarda el aceite en el pellejo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SOLUTION" :TEXT "Si tiene solución, para qué preocuparse; y si no la tiene, para qué preocuparse." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PLANS" :TEXT "Si tú no haces tus planes alguien los hará por ti." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Si ya tienes el amor, cuídalo, ámalo, respétalo, riégalo todos los días. Es lo más importante que tendrás en tu vida." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MISFORTUNE" :TEXT "Solo hay dos desgracias en la vida: No conseguir lo que deseas y conseguir lo que deseas." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MESSAGE" :TEXT "Sí esperas mensaje de esa persona que no te escribe, eso es el mensaje." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FLY" :TEXT "Tal vez puedas adornarte con las plumas de otro pero no puedes volar con ellas." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "OFFENSE" :TEXT "Te exigen sinceridad pero se ofenden si les dices la verdad. Entonces ¿Te ofendo por sinceridad o te miento por educación?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "Toco el futuro, me dedico a enseñar." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SAY" :TEXT "Todo el mundo se lo había dicho menos él." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "BORN" :TEXT "Todos al nacer ya sabemos llorar, necesitamos aprender a reír." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "STATUS" :TEXT "Tu estado de ánimo no puede depender de una noticia, una nube o una persona porque entonces ya no es tuyo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORDS" :TEXT "Tus palabras son tan importantes como la forma en que las dices." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MAN" :TEXT "Un hombre de verdad es capaz de llevar lo importante dentro de sí mismo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "NEUROTIC" :TEXT "Un neurótico se pasa la mitad de su vida poniéndose trampas y la otra mitad cayendo en ellas." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PROBLEM" :TEXT "Un problema es algo que puede resolverse. Un hecho en la vida es algo que debe aceptarse." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CRY" :TEXT "Un sabio contó un chiste ante su público y todos se rieron. Lo volvio a contar y casi nadie se rió. Lo contó una tercera vez y nadie se rió. Si no puedes reírte varias veces de una sola cosa ¿Porque lloras por lo mismo una y otra vez?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DEFECTS" :TEXT "Una buena manera de descubrir tus defectos es observar lo que te irrita de los demás." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "GIVE" :TEXT "Una cosa es dar y otra dejar que te quiten. Una cosa es dar y otra obligar a recibir." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CHESS" :TEXT "Una vez terminada la partida el rey y el peón vuelven a la misma caja." :AUTHOR "Proverbio italiano" :ITEM "" :DATE "")
 (:CATEGORY "WIND" :TEXT "Viento que llegas a todas partes. Desordena, ordena, renueva." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TONGUE" :TEXT "Vigila tu lengua, la palabra que no has dicho es tu esclava, la que has dicho es tu ama." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HAPPY" :TEXT "¡Qué feliz sería yo con lo que no tengo!." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PRISON" :TEXT "¿Cómo escapar de la prisión?. Haz que la prisión esté fuera." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HEART" :TEXT "¿De qué sirve tener ojos si el corazón es ciego?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WRITE" :TEXT "¿Porqué escribes si nadie lo va a leer?. ¿Porqué canta el pájaro aunque no tenga público?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DRUG" :TEXT "¿Quieres tu droga?. Prepárate para la resaca." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DEPRESSION" :TEXT "¿Qué es la depresión? Es la expresión de algo que no puede expresarse." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "¿Qué es la vida sino un constante epílogo?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "¿Qué es vivir?. Es nacer lentamente." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SOUL" :TEXT "Haz que mi alma nacida ciega se cubra de ojos que no teman ver." :AUTHOR "Alejandro Jodorowski" :ITEM "" :DATE "")
 (:CATEGORY "WARRIOR" :TEXT "El camino de un guerrero es la planta de sus pies, ellas van hacia la muerte gozando cada uno de los pasos." :AUTHOR "Alejandro Jodorowski" :ITEM "" :DATE "")
 (:CATEGORY "PATH" :TEXT "Ánimo querido amigo. Al final del primer camino te estás esperando. Al encontrarte comenzarás el segundo camino y al morir feliz comenzarás el tercero." :AUTHOR "Alejandro Jodorowsky" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Si un huevo se rompe desde el exterior se acaba la vida. Sí se rompe desde el interior la vida comienza." :AUTHOR "Alejandro Jodorowsky" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Hace falta toda una vida para aprender a vivir." :AUTHOR "Séneca" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "We love life not because we are used to living but because we are used to loving." :AUTHOR "Nietzsche" :ITEM "" :DATE "")
 (:CATEGORY "FAITH" :TEXT "Nunca pierdas la fe. Los mejores comienzos vienen después de los peores finales." :AUTHOR "Bob Marley" :ITEM "" :DATE "")
 (:CATEGORY "PAST" :TEXT "No me juzgues por mi pasado, ya no vivo allí ni tú tampoco." :AUTHOR "Sherezade" :ITEM "" :DATE "")
 (:CATEGORY "REPAIR" :TEXT "Repara tu trineo en el verano y tu carreta en el invierno." :AUTHOR "Proverbio armenio" :ITEM "" :DATE "")
 (:CATEGORY "WISDOM" :TEXT "Por tres métodos podemos adquirir sabiduría, por reflexión que es el más noble. Por imitación que es el más fácil y por experiencia que es el más amargo." :AUTHOR "Confucio" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Lo más difícil de aprender en esta vida es saber qué puentes hay que cruzar y que puentes hay que quemar." :AUTHOR "Bertrand Russell" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "No se enamoró de ella sino de su sombra. La visitaba al alba cuando su amada era más larga." :AUTHOR "Alejandro Jodorowsky" :ITEM "" :DATE "")
 (:CATEGORY "PERSON" :TEXT "¿Cómo de podrido y fraudulento es cuando las personas pretenden “decírtelo claramente”? - La persona directa, recta y buena debería ser como el olor de una cabra - sabes cuando están en la habitación contigo." :AUTHOR "Marco Aurelio" :ITEM "" :DATE "")
 (:CATEGORY "WORD" :TEXT "Cada palabra tiene consecuencias. Cada silencio también." :AUTHOR "Jean-Paul Sartre" :ITEM "" :DATE "")
 (:CATEGORY "WORRY" :TEXT "Preocuparte es usar tu imaginación para crear algo que no quieres." :AUTHOR "Abraham Hicks" :ITEM "" :DATE "")
 (:CATEGORY "WISE" :TEXT "El sabio siempre gana porque no compite." :AUTHOR "Lao-Tse" :ITEM "" :DATE "")
 (:CATEGORY "WISE" :TEXT "Quien no sabe que no sabe es un necio, apártate de él. Quién sabe que no sabe es sencillo, instruyelo. Quién no sabe que sabe está dormido, despierta lo. Quién sabe que sabe es un sabio, síguelo." :AUTHOR "Proverbio árabe" :ITEM "" :DATE "")
 (:CATEGORY "EXIST" :TEXT "Existir es un hecho, vivir es un arte." :AUTHOR "Sri Sri Ravi Shankar" :ITEM "" :DATE "")
 (:CATEGORY "WISDOM" :TEXT "The man of knowledge must be able not only to love his enemies, but also to hate his friends." :AUTHOR "Nietzsche" :ITEM "" :DATE "")
 (:CATEGORY "PATIENTE" :TEXT "La paciencia es la fortaleza del débil. La impaciencia es la debilidad del fuerte." :AUTHOR "Immanuel Kant" :ITEM "" :DATE "")
 (:CATEGORY "CHANGE" :TEXT "En tiempos de cambio quiénes están abiertos al aprendizaje se adueñarán del futuro mientras que aquellos que creen saberlo todo estarán bien equipados para un mundo que ya no existe." :AUTHOR "Erick Hoffer" :ITEM "" :DATE "")
 (:CATEGORY "PLACE" :TEXT "Antes de entrar en un lugar fíjate por donde se puede salir." :AUTHOR "Proverbio vikingo" :ITEM "" :DATE "")
 (:CATEGORY "SKY" :TEXT "Abuelito, ¿Por qué el cielo es tan azul?. Porque para hacerte feliz me tragué todas las nubes negras." :AUTHOR "Alejandro Jodorowski" :ITEM "" :DATE "")
 (:CATEGORY "LOOK" :TEXT "Su mirada se cruzó con la tuya, ahora tus heridas supuran perlas." :AUTHOR "Alejandro Jodorowski" :ITEM "" :DATE "")
 (:CATEGORY "SKIN" :TEXT "Al contacto con tu piel supe lo que era tener manos." :AUTHOR "Alejandro Jodorowski" :ITEM "" :DATE "")
 (:CATEGORY "DESTINY" :TEXT "La gente que dice que todo está predestinado y que no podemos hacer nada para cambiarlo mira antes de cruzar la calle." :AUTHOR "Stephen Hawking" :ITEM "" :DATE "")
 (:CATEGORY "MATCH" :TEXT "Ganar o perder un partido depende de las ganas que usted tenga de jugar el último tanto." :AUTHOR "Bjorn Borg" :ITEM "" :DATE "")
 (:CATEGORY "DEATH" :TEXT "Cuida de no morir antes de tu muerte." :AUTHOR "Vicente Huidobro" :ITEM "" :DATE "")
 (:CATEGORY "SIDES" :TEXT "A veces uno sabe de qué lado estar viendo quienes están del otro lado." :AUTHOR "Leonard Cohen" :ITEM "" :DATE "")
 (:CATEGORY "JUDGEMENT" :TEXT "No juzgues cada día por la cosecha que has recogido sino por las semillas que has plantado." :AUTHOR "Robert L. Stevenson" :ITEM "" :DATE "")
 (:CATEGORY "BEAUTY" :TEXT "La belleza comienza por la decisión de ser uno mismo." :AUTHOR "Coco Chanel" :ITEM "" :DATE "")
 (:CATEGORY "BEAUTY" :TEXT "La naturaleza te da la cara que tienes a los veinte, a los cincuenta depende de ti" :AUTHOR "Coco Chanel" :ITEM "" :DATE "")
 (:CATEGORY "SHUTUP" :TEXT "Callando es como se aprende a escuchar. Escuchando es como se aprende a hablar. Hablando es como se aprende a callar." :AUTHOR "Diógenes de Sinope" :ITEM "" :DATE "")
 (:CATEGORY "MODESTY" :TEXT "La humildad no es pensar que eres menos sino pasar menos tiempo pensando en ti." :AUTHOR "C. S. Lewis" :ITEM "" :DATE "")
 (:CATEGORY "WALK" :TEXT "Si caminas solo irás más rápido. Si caminas acompañado llegarás más lejos." :AUTHOR "Proverbio chino" :ITEM "" :DATE "")
 (:CATEGORY "SILENCE" :TEXT "Practica el silencio. En él se hallan las mejores respuestas." :AUTHOR "Edgar Payares" :ITEM "" :DATE "")
 (:CATEGORY "WORD" :TEXT "Las palabras elegantes no suelen ser sinceras. Las palabras sinceras no suelen ser elegantes." :AUTHOR "Lao-Tse" :ITEM "" :DATE "")
 (:CATEGORY "JOY" :TEXT "Reconocí a la alegría por el ruido que hizo al marcharse." :AUTHOR "Jacques Prévert" :ITEM "" :DATE "")
 (:CATEGORY "LONELINESS" :TEXT "La soledad no es aquello que sucede cuando estás solo sino aquello que sientes cuando no puedes estar contigo mismo." :AUTHOR "Pago" :ITEM "" :DATE "")
 (:CATEGORY "CHEAT" :TEXT "Si lograste engañar a una persona no quiere decir que fuera tonta, quiere decir que confiaba en ti más de lo que merecías." :AUTHOR "Charles Bukowski" :ITEM "" :DATE "")
 (:CATEGORY "PERSON" :TEXT "Una persona inteligente se recupera pronto de un fracaso. Una persona mediocre jamás se recupera de un éxito." :AUTHOR "Séneca" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Entonces dijo Almitra: Háblanos del amor. Y Al-Mustafá alzó la cabeza y miró a la multitud, y un silencio cayó sobre todos, y con fuerte voz, dijo él: Cuando el amor os llame, seguidle, aunque sus caminos sean escabrosos y escarpados. Y cuando sus alas os envuelvan, entregaos a él, aunque la espada oculta en su plumaje pueda heriros…" :AUTHOR "kahlil" :ITEM "El profeta" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "No existe nada tan terrible y peligroso como la gente normal." :AUTHOR "H.P. Lovecraft" :ITEM "" :DATE "")
 (:CATEGORY "SOURCES" :TEXT "Las grandes fuentes se conocen en las grandes sequías. Los buenos amigos en las épocas desgraciadas." :AUTHOR "Proverbio chino" :ITEM "" :DATE "")
 (:CATEGORY "MASTER" :TEXT "Maestro, daría mi vida por tocar como usted. Ese es el precio que yo he pagado." :AUTHOR "" :ITEM "Anécdota atribuida a Andrés Segovia" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "El amor es la preocupación activa por la vida y el crecimiento de lo que amamos." :AUTHOR "Eric Fromm" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "El amor tiene dos enemigos principales: la indiferencia que lo mata lentamente, y la desilusión que lo elimina de una vez." :ITEM "Walter Riso" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Uno de los trucos de la vida consiste, más que en tener buenas cartas en saber jugar bien las que uno tiene." :AUTHOR "Josh Billings" :ITEM "" :DATE "")
 (:CATEGORY "SELF" :TEXT "Es un acto de irresponsabilidad no dedicarte tiempo a ti mismo." :AUTHOR "Walter Riso" :ITEM "" :DATE "")
 (:CATEGORY "LOAD" :TEXT "Para poder descargar un cargamento de halva lo más importante es tener recipientes donde guardar el halva." :AUTHOR "Proverbio arabe" :ITEM "" :DATE "")
 (:CATEGORY "CUENTOS" :TEXT "Los cuentos ayudan a dormir a los niños y a despertar a los adultos." :AUTHOR "Jorge Bucay" :ITEM "" :DATE "")
 (:CATEGORY "MAN" :TEXT "El hombre entero comprende el dolor de los demás y comparte la alegría de cualquiera." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "UNDERSTANDING" :TEXT "Un entendimiento solo nutrido de lógica es como un la hoja de un cuchillo sin mango que hiere la mano de su dueño." :AUTHOR "Tagore" :ITEM "" :DATE "")
 (:CATEGORY "MONEY" :TEXT "Si prefieres entregar tu dinero a las farmacias, a los psicólogos y a los médicos, no te des gustos." :AUTHOR "Walter Riso" :ITEM "" :DATE "")
 (:CATEGORY "GRANT" :TEXT "Nadie puede hacerte sentir inferior sin tu consentimiento." :AUTHOR "Roosevelt" :ITEM "" :DATE "")
 (:CATEGORY "CHANGE" :TEXT "A la gente le sienta mal que haya un camino personal." :AUTHOR "Brassens" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Amar es una conducta que hay que aprender, enseñar y practicar." :AUTHOR "Mila Cahue" :ITEM "" :DATE "")
 (:CATEGORY "DO" :TEXT "Es cierto que nadie puede hacer siempre todo lo que quiere, pero cualquiera puede no hacer nunca lo que no quiere." :AUTHOR "Jorge Bucay" :ITEM "" :DATE "")
 (:CATEGORY "ANSIETY" :TEXT "La ansiedad no agota las angustias del mañana solo agota la fuerza del hoy." :AUTHOR "Charles Spurgeon" :ITEM "" :DATE "")
 (:CATEGORY "MAN" :TEXT "Qué curioso es el hombre. Nacer no pide, vivir no sabe, morir no quiere." :AUTHOR "Proverbio Chino" :ITEM "" :DATE "")
 (:CATEGORY "PEACE" :TEXT "Quién tiene paz en su conciencia lo tiene todo." :AUTHOR "Don Bosco" :ITEM "" :DATE "")
 (:CATEGORY "MIND" :TEXT "La mente tiene su propio lugar, y en sí misma puede convertir el infierno en cielo y el cielo en infierno." :AUTHOR "John Milton" :ITEM "" :DATE "")
 (:CATEGORY "PERFECTION" :TEXT "La perfección es una pulida colección de errores." :AUTHOR "Mario Satz" :ITEM "" :DATE "")
 (:CATEGORY "PERFECTION" :TEXT "La perfección es una pulida colección de errores." :AUTHOR "Mario Benedetti" :ITEM "" :DATE "")
 (:CATEGORY "BEST" :TEXT "Procurando lo mejor estropeamos lo que está bien." :AUTHOR "William Shakespeare" :ITEM "" :DATE "")
 (:CATEGORY "FREEDOM" :TEXT "La libertad significa responsabilidad por eso la mayoría de los hombres le tiene tanto miedo." :AUTHOR "George Bernard Shaw" :ITEM "" :DATE "")
 (:CATEGORY "WARTH" :TEXT "El que domina su cólera domina su peor enemigo." :AUTHOR "Confucio" :ITEM "" :DATE "")
 (:CATEGORY "APART" :TEXT "Para que nada nos separe que nada nos una." :AUTHOR "Pablo Neruda" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Tu tarea no es buscar el amor sino buscar y encontrar todas las barreras que has construido dentro de ti contra él." :AUTHOR "Rumi" :ITEM "" :DATE "")
 (:CATEGORY "HANDS" :TEXT "Cuando nacemos, nacemos con las manos cerradas porque no traemos nada. Cuando morimos lo hacemos con las manos abiertas porque no nos llevamos nada" :ITEM "" :DATE "")
 (:CATEGORY "KNOW" :TEXT "Conócete, acéptate, supérate." :AUTHOR "Santo Tomás de Aquino" :ITEM "" :DATE "")
 (:CATEGORY "DEATH" :TEXT "Si no has descubierto nada por lo que merezca la pena morir es que no estás preparado para vivir." :AUTHOR "Martín L. King" :ITEM "" :DATE "")
 (:CATEGORY "MAN" :TEXT "El hombre no es derrotado por sus oponentes sino por sí mismo." :AUTHOR "Jan Christian Smuts" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "La risa es la distancia más corta entre dos personas." :AUTHOR "Víctor Borge" :ITEM "" :DATE "")
 (:CATEGORY "NEED" :TEXT "No hay necesidad de apresurarse. No hay necesidad de brillar. No es necesario ser nadie más que uno mismo." :AUTHOR "Virginia Wolf" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "El ego es feliz cuando puede obtener algo. El amor es feliz cuando puede dar algo." :AUTHOR "Osho" :ITEM "" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "Hay gente sin gusto por las letras y maniaca con la ortografía." :AUTHOR "Brontis Jodorowski" :ITEM "" :DATE "")
 (:CATEGORY "DIOS" :TEXT "Dios ha hecho tres cosas muy bellas: las estrellas del cielo, las flores del campo y los ojos de los niños." :AUTHOR "Joseph Mary Plunket" :ITEM "" :DATE "")
 (:CATEGORY "HAPPINESS" :TEXT "La publicidad se basa en una única cosa: la felicidad. […] Pero ¿Qué es la felicidad?. La felicidad es solo un momento antes de que quieras más felicidad." :AUTHOR "Don Draper" :ITEM "Mad Men" :DATE "")
 (:CATEGORY "IMPOSSIBLE" :TEXT "Todo es imposible hasta que lo consigues. Todo da miedo hasta que lo conoces. Todo parece que importa muy poco hasta que lo pierdes." :AUTHOR "Redry" :ITEM "" :DATE "")
 (:CATEGORY "GOAL" :TEXT "Que tu meta hoy sea ganarle a tu mejor excusa." :AUTHOR "Ayer Ita" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "La risa es el sol que ahuyenta el invierno del rostro humano." :AUTHOR "Víctor Hugo" :ITEM "" :DATE "")
 (:CATEGORY "FIGHT" :TEXT "Cuando dos elefantes se pelean es la hierba la que sufre." :AUTHOR "Proverbio africano" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La vida empieza a tener sentido cuando ayudas a otro a ponerse de pie y a andar." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La vida tiene sentido cuando andas, cuando evolucionas, y no dejas tras de ti amargura." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La vida es una atrevida aventura o no es nada." :AUTHOR "Hellen Keller" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La vida es como pintar un cuadro no como hacer una suma." :AUTHOR "Oliver Wendel Holmes" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "A life spent making mistakes is not only more honorable, but more useful than a life spent doing nothing." :AUTHOR "George Bernard Shaw" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Si quieres conocer tu vida pasada contempla tu estado presente. Si quieres conocer tu vida futura contempla tus acciones presentes." :AUTHOR "Padmasambhava" :ITEM "" :DATE "")
 (:CATEGORY "MATURITY" :TEXT "La madurez del hombre es volver a encontrar la seriedad con la que jugaba cuando era niño." :AUTHOR "Friedrich Nietzsche" :ITEM "" :DATE "")
 (:CATEGORY "WAS" :TEXT "Lo que eres ellos lo fueron, lo que son tú lo serás." :AUTHOR "" :ITEM "Inscripción en la cripta de los monjes capuchinos en Santa María de la Concezione" :ITEM "" :DATE "")
 (:CATEGORY "SUICIDE" :TEXT "The thought of suicide is a great consolation: by means of it one gets successfully through many a bad night." :AUTHOR "Nietzsche" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Estaba enamorado del resultado: la imagen de mí sobre el escenario, la gente aplaudiendo, yo tocando, dejandome la piel en la canción…, pero no estaba enamorado del proceso. Y por ello fracasé." :AUTHOR "Mark Manson" :ITEM "" :DATE "")
 (:CATEGORY "PRESENT" :TEXT "Cuida el presente porque en él vivirás el resto de tu vida." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La vida es el arte del encuentro." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "SUFFER" :TEXT "Un hombre que sufre antes de lo necesario sufre más de lo necesario." :AUTHOR "Séneca" :ITEM "" :DATE "")
 (:CATEGORY "HEART" :TEXT "Un gran hombre es aquél que no ha perdido su corazón de niño." :AUTHOR "Mencio" :ITEM "" :DATE "")
 (:CATEGORY "VERB" :TEXT "En el principio el verbo era." :AUTHOR "Juan, el evangelista" :ITEM "Juan 1, 1" :DATE "")
 (:CATEGORY "WORDS" :TEXT "Ve y convierte tus palabras en hechos." :AUTHOR "Ralph Waldo Emerson" :ITEM "" :DATE "")
 (:CATEGORY "PATH" :TEXT "Encontraremos un camino o bien lo construiremos." :AUTHOR "Anibal" :ITEM "" :DATE "")
 (:CATEGORY "FLAME" :TEXT "Una poderosa llama siguió a una diminuta chispa." :AUTHOR "Dante" :ITEM "" :DATE "")
 (:CATEGORY "IDESAS" :TEXT "It's not about ideas. It's about making ideas happen." :AUTHOR "Scott Belsky" :ITEM "" :DATE "")
 (:CATEGORY "BE" :TEXT "Somos aquello que hacemos repetidamente. Por eso el mérito no está en la acción sino en el hábito." :AUTHOR "Aristóteles" :ITEM "" :DATE "")
 (:CATEGORY "HABIT" :TEXT "El hábito es o el mejor de los sirvientes o el peor de los amos." :AUTHOR "Natheniel Emmons" :ITEM "" :DATE "")
 (:CATEGORY "HABIT" :TEXT "Primero construimos nuestros hábitos y luego esos hábitos nos construyen a nosotros." :AUTHOR "John Dryden" :ITEM "" :DATE "")
 (:CATEGORY "HABIT" :TEXT "Las diminutas cadenas de los hábitos son generalmente demasiado pequeñas para sentirlas hasta que llegan a ser demasiado fuertes para romperlas." :AUTHOR "Ben Jonson" :ITEM "" :DATE "")
 (:CATEGORY "START" :TEXT "Never fear starting. Fear never starting." :AUTHOR "Ryan Lilly" :ITEM "" :DATE "")
 (:CATEGORY "FALL" :TEXT "Even if you fall on your face, you're still moving forward." :AUTHOR "Victor Kiam" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Vivimos esta vida como si lleváramos otra en la maleta." :AUTHOR "Ernest Hemingway" :ITEM "" :DATE "")
 (:CATEGORY "DUMMY" :TEXT "Nunca he encontrado una persona tan ignorante que no pueda aprender algo de ella." :AUTHOR "Galileo Galilei" :ITEM "" :DATE "")
 (:CATEGORY "LIGHT" :TEXT "Everything is shown up by being exposed to the light, and whatever is exposed to the light itself becomes light." :AUTHOR "St. Paul" :ITEM "" :DATE "")
 (:CATEGORY "LEARN" :TEXT "Anyone who stops learning is old, whether at twenty or eighty. Anyone who keeps learning stays young." :AUTHOR "Henry Ford" :ITEM "" :DATE "")
 (:CATEGORY "HEART" :TEXT "Tanto los hombres como las mujeres se dejan dirigir con mayor frecuencia por sus corazones que por sus entendimientos." :AUTHOR "Lord Chesterfield" :ITEM "" :DATE "")
 (:CATEGORY "MIND" :TEXT "Es la mente la que hace el bien o el mal, la que hace desgraciado o feliz, rico o pobre." :AUTHOR "Edmund Spencer" :ITEM "" :DATE "")
 (:CATEGORY "CHANGE" :TEXT "Las cosas no cambian, somos nosotros los que cambiamos." :AUTHOR "Henry David Thoreau" :ITEM "" :DATE "")
 (:CATEGORY "ASK" :TEXT "Pide y recibirás, Busca y encontrarás; llama y se te abrirán las puertas." :AUTHOR "Mateo" :ITEM "Mateo 7, 7" :DATE "")
 (:CATEGORY "EXPERIENCE" :TEXT "La experiencia no es lo que le sucede a un hombre sino lo que ese hombre hace con lo que le sucede." :AUTHOR "Aldoux Huxley" :ITEM "" :DATE "")
 (:CATEGORY "TIME" :TEXT "Disponemos de tiempo suficiente si queremos y lo utilizamos correctamente." :AUTHOR "Johann Wolfgang von Goethe" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La vida es la infancia de nuestra inmortalidad." :AUTHOR "Johann Wolfgang von Goethe" :ITEM "" :DATE "")
 (:CATEGORY "UP" :TEXT "Sube alto, llega lejos. Tu objetivo es el cielo, tu meta, las estrellas." :AUTHOR "" :ITEM "Inscripción en el Williams College" :DATE "")
 (:CATEGORY "WISDOM" :TEXT "Poseo una gotita de sabiduría en mi alma, deja que se disuelva en tu océano." :AUTHOR "Rumi" :ITEM "" :DATE "")
 (:CATEGORY "HOW" :TEXT "Aquél que tiene un porqué para vivir se enfrentará a todos los cómos." :AUTHOR "Friedich Nietzche" :ITEM "" :DATE "")
 (:CATEGORY "DIVINITY" :TEXT "El carácter de un hombre es el guardián de su divinidad." :AUTHOR "Heráclito" :ITEM "" :DATE "")
 (:CATEGORY "CHANGE" :TEXT "Nada permanece salvo el cambio." :AUTHOR "Heráclito" :ITEM "" :DATE "")
 (:CATEGORY "CHANGE" :TEXT "Change alone is unchanging." :AUTHOR "Heráclito" :ITEM "" :DATE "")
 (:CATEGORY "KNOW" :TEXT "Solo sé que no sé nada." :AUTHOR "Sócrates" :ITEM "" :DATE "")
 (:CATEGORY "BEAUTY" :TEXT "Dadme belleza interior, que el hombre interior y exterior sean uno solo." :AUTHOR "Sócrates" :ITEM "" :DATE "")
 (:CATEGORY "TEACH" :TEXT "No puedo enseñaros nada, solamente puedo ayudaros a buscar el conocimiento dentro de vosotros mismos, lo cual es mucho mejor que traspasaros mi poca sabiduría." :AUTHOR "Sócrates" :ITEM "" :DATE "")
 (:CATEGORY "DUMB" :TEXT "Cualquier tonto puede establecer una regla y a cualquier tonto puede importarle." :AUTHOR "Henry David Thoreau" :ITEM "" :DATE "")
 (:CATEGORY "KNOWLEDGE" :TEXT "El conocimiento del mundo solo se adquiere en el mundo y no en un armario." :AUTHOR "Lord Chesterfield" :ITEM "" :DATE "")
 (:CATEGORY "HEAVEN" :TEXT "El que busca el cielo en la tierra se ha dormido en clase de geografía." :AUTHOR "Stanislaw Lem" :ITEM "" :DATE "")
 (:CATEGORY "IMAGINATION" :TEXT "La imaginación es más importante que el conocimiento." :AUTHOR "Albert Einstein" :ITEM "" :DATE "")
 (:CATEGORY "LIMITS" :TEXT "La única forma de descubrir los límites de lo posible consiste en ir más allá de ellos, hacia lo imposible." :AUTHOR "Arthur C. Clarke" :ITEM "" :DATE "")
 (:CATEGORY "LIMITS" :TEXT "No estamos limitados por nuestra edad sino que nos vemos limitados por ella." :AUTHOR "Stu Mittelman" :ITEM "" :DATE "")
 (:CATEGORY "HEART" :TEXT "En un corazón lleno siempre hay espacio para todo, mientras que en un corazón vacío no queda espacio para nada." :AUTHOR "Antonio Porcha" :ITEM "" :DATE "")
 (:CATEGORY "PAUSE" :TEXT "La habilidad de hacer una pausa y no actuar por el primer impulso se ha vuelto aprendizaje crucial en la vida diaria." :AUTHOR "D. Goleman" :ITEM "" :DATE "")
 (:CATEGORY "EFFORT" :TEXT "It takes a lot of effort to make something seem effortless." :AUTHOR "Alexander Isley" :ITEM "" :DATE "")
 (:CATEGORY "LONGEVITY" :TEXT "Cuadruple prefecto de la longevidad: Uno, porque nunca comí sin gana; dos, porque nunca estuve de pie si pude estar sentado; tres, porque jamas bebí sino agua; y cuatro, porque me guardé siempre de dormir una noche entera con mujer alguna" :AUTHOR "Abenámar" :ITEM "" :DATE "")
 (:CATEGORY "GARDEN" :TEXT "Debemos cultivar nuestro jardín." :AUTHOR "Voltaire" :ITEM "" :DATE "")
 (:CATEGORY "DEPRESSION" :TEXT "La depresión es una prisión en la que eres tanto el prisionero como el cruel carcelero." :AUTHOR "Dorothy Rowe" :ITEM "" :DATE "")
 (:CATEGORY "HAPPINESS" :TEXT "Toda la dicha que hay en este mundo toda proviene de desear que los demás sean felices y todo el sufrimiento que hay en este mundo todo proviene de desear ser feliz yo." :AUTHOR "Shantideva" :ITEM "" :DATE "")
 (:CATEGORY "PRISONER" :TEXT "La mejor forma de evitar que un prisionero no escape, es asegurarse de que nunca sepa que está en prisión." :AUTHOR "Dostoyeski" :ITEM "" :DATE "")
 (:CATEGORY "CIRCUMSTANCES" :TEXT "El hombre no es hijo de las circunstancias. Las circunstancias son hijas del hombre." :AUTHOR "Benjamin Disraeli" :ITEM "" :DATE "")
 (:CATEGORY "ENEMY" :TEXT "Ni tus peores enemigos pueden hacer tanto daño como tus propios pensamientos." :AUTHOR "Buda" :ITEM "" :DATE "")
 (:CATEGORY "MOUTH" :TEXT "No lo que entra en la boca contamina al hombre; mas lo que sale de la boca, esto contamina al hombre." :AUTHOR "Mateo" :ITEM "Mateo 15" :DATE "")
 (:CATEGORY "THINGS" :TEXT "No vemos las cosas tal cual son, las vemos tal cual somos." :AUTHOR "Anais Nin" :ITEM "" :DATE "")
 (:CATEGORY "WORDS" :TEXT "Ten cuidado con el poder de tus palabras. Somos los únicos conductores de nuestro destino y lo que decimos tiene la habilidad de llevar nuestros destinos en muchas direcciones." :AUTHOR "Yehuda Berg" :ITEM "" :DATE "")
 (:CATEGORY "LANGUAGE" :TEXT "Los límites de mi lenguaje son los límites de mi mente." :AUTHOR "Ludwig Wittgenstein" :ITEM "" :DATE "")
 (:CATEGORY "MAN" :TEXT "Cualquier hombre hoy es el resultado del pensamiento de ayer." :AUTHOR "Napoleon Hill" :ITEM "" :DATE "")
 (:CATEGORY "UNHAPPY" :TEXT "You have been spending your entire life learning to be unhappy." :AUTHOR "Srikumar Rao" :ITEM "" :DATE "")
 (:CATEGORY "PATH" :TEXT "Si no sabes cual es tu camino, la próxima misión de tu vida será descubrirlo." :AUTHOR "Albert Liebermann" :ITEM "" :DATE "")
 (:CATEGORY "LIMITS" :TEXT "Deja de pensar en clave de limitaciones y empieza a pensar en clave de posibilidades." :AUTHOR "Terry Josephson" :ITEM "" :DATE "")
 (:CATEGORY "DEAD" :TEXT "No son los muertos los que en dulce calma la paz disfrutan de su tumba fría muertos son los que tienen muerta el alma y viven todavía." :AUTHOR "Gustavo Adolfo Bécquer" :ITEM "" :DATE "")
 (:CATEGORY "WELL" :TEXT "Si te arrojas a un pozo la providencia no está obligada a ir a buscarte." :AUTHOR "Proverbio Persa" :ITEM "" :DATE "")
 (:CATEGORY "OTHER" :TEXT "Sabrás quién es el que te quiere cuando te veas reflejado en él." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "JUDGE" :TEXT "Sé tu propio juez; pero un juez justo." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "HUNGER" :TEXT "Sólo pasa hambre el que no sabe que tiene dos manos." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "BODY" :TEXT "Si alimentas tu cuerpo para que te sirva, debes también alimentar tu alma, para que también te sirva." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "LOOK" :TEXT "Habla mirando a los ojos, transmite tu fuerza en tu mirada." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "SOUL" :TEXT "Un alma poco alimentada es un alma débil, sin fuerza. Un alma bien alimentada es un alma que genera energía, que contagia, que anima." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "FEAR" :TEXT "Nunca hables con miedo, porque las palabras se volverán contra ti. Si tienes miedo no hables, porque el miedo es también contagioso." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "MARRIAGE" :TEXT "No sabrás con quién te has casado hasta que no te separes." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "HEALTH" :TEXT "Si quieres recuperar la salud deja la crítica, el resentimiento y la culpa, responsables de nuestras enfermedades." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "SCHOOLS" :TEXT "Pasé por todas las escuelas pero no me quedé en ninguna, todas me dejaron algo pero no me detuvieron, porque voy en busca de mí mismo, donde conoceré la verdad." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Si es apego no es amor porque el amor no produce pesar y el apego sí." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Cuando nos distraemos de la vida, ya estamos transitando la muerte." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "PEST" :TEXT "No hay peste más dañina que la ignorancia ni esclavitud más grande que la mentira." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "HAPPY" :TEXT "Si somos felices por una causa, por otra causa dejaremos de serlo, es decir que si somos felices por algo, no somos felices." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "DREAMS" :TEXT "Si no soñaras, si no te agotaras en los pleitos de los sueños, descansarías más y te levantarías mejor. Para eso debes vivir plenamente cada vigilia para que el inconsciente no tenga que recordarte nada a la hora del sueño." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "AGE" :TEXT "Nos envejece más la cobardía que el tiempo, los años solo arrugan la piel pero el miedo arruga el alma." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "THIEF" :TEXT "Si para combatir al ladrón robas, si para combatir al mentiroso mientes, si para combatir al asesino matas, ¿En qué te diferencias de ellos? Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "No se puede conocer el amor sin humildad, sin amplitud, sin generosidad, sin una libertad total, sin coraje." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "PATH" :TEXT "Hay que disfrutar el camino, estar atentos al paso que estamos dando, no a la meta, que nunca se alcanza porque transitamos un infinito en una eternidad." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "KNOWLEDGE" :TEXT "Llegarás al verdadero conocimiento cuando tengas verdadera necesidad de él." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "THINK" :TEXT "Pensamiento, palabra y acción deben ser un solo acto para poder alcanzar a la verdad en cualquier circunstancia." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "EGO" :TEXT "El ego no permite el desapego, por eso fácilmente te lleva al sufrimiento." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "BODY" :TEXT "Cuida el cuerpo que te fue dado, pero recuerda que no eres el cuerpo." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "DEPRESSION" :TEXT "No estas deprimido, estas distraído, distraído de la vida que te puebla. Tienes corazón, cerebro, alma y espíritu, entonces cómo puedes sentirte pobre y desdichado." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "GRANDFATHER" :TEXT "Mi abuelo era un hombre muy valiente, solo le tenía miedo a los idiotas. Le pregunté por qué y me respondió: Porque son muchos y al ser mayoría eligen hasta al presidente." :AUTHOR "Facundo Cabral" :ITEM "" :DATE "")
 (:CATEGORY "SUMMIT" :TEXT "Cerca de la cima siempre hay mil excusas para bajarse y una sola para subir." :AUTHOR "Ramón Portilla" :ITEM "" :DATE "")
 (:CATEGORY "HAPPY" :TEXT "Persigo la felicidad y la montaña responde a mi búsqueda." :AUTHOR "Chantal Maudit" :ITEM "" :DATE "")
 (:CATEGORY "FRIENDSHIP" :TEXT "Antes que el esfuerzo, las rocas y el viento, la amistad es el componente esencial en la montaña." :AUTHOR "Geyson Millar" :ITEM "" :DATE "")
 (:CATEGORY "MOUNTAIN" :TEXT "Quien ha escuchado alguna vez la voz de las montañas, nunca la podrá olvidar." :AUTHOR "Proverbio Tibetano" :ITEM "" :DATE "")
 (:CATEGORY "MOUNTAIN" :TEXT "Regresad vivos, regresad como amigos, llegad a la cumbre. Por este orden." :AUTHOR "Roger Baxter-Jones" :ITEM "" :DATE "")
 (:CATEGORY "MOUNTAIN" :TEXT "No conquistamos las montañas, sino a nosotros mismos." :AUTHOR "Edmund Hillary" :ITEM "" :DATE "")
 (:CATEGORY "MOUNTAIN" :TEXT "El alpinista es quién conduce su cuerpo allá dónde un día sus ojos lo soñaron." :AUTHOR "Gaston Rébuffat" :ITEM "" :DATE "")
 (:CATEGORY "MOUNTAIN" :TEXT "Mucho más que una disciplina para el cuerpo, el alpinismo es un lujo para el espíritu y un recurso para el alma." :AUTHOR "Georges Sonnier" :ITEM "" :DATE "")
 (:CATEGORY "FRIENDSHIP" :TEXT "Amistad que acaba no había comenzado." :AUTHOR "Publio Sirio" :ITEM "" :DATE "")
 (:CATEGORY "JUDGE" :TEXT "Justo es aquello que conviene al que dicta sentencia." :AUTHOR "Johann Wolfgang von Goethe" :ITEM "" :DATE "")
 (:CATEGORY "DIFFICULT" :TEXT "They must find it difficult. Those who have taken authority as the truth. Rather than the truth as the authority." :AUTHOR "G. Massey" :ITEM "" :DATE "")
 (:CATEGORY "HEALTH" :TEXT "It is no measure of health to be well adjusted to a profoundly sick society." :AUTHOR "Jiddu Krishnamurti" :ITEM "" :DATE "")
 (:CATEGORY "PROBLEM" :TEXT "Freedom from the desire for an answer is essential to the understanding of a problem." :AUTHOR "Jiddu Krishnamurti" :ITEM "" :DATE "")
 (:CATEGORY "FREEDOM" :TEXT "None are more hopelessly enslaved than those who falsely believe they are free." :AUTHOR "Johann Wolfgang von Goethe" :ITEM "" :DATE "")
 (:CATEGORY "MONEY" :TEXT "Gastamos dinero que no tenemos, en cosas que no necesitamos, para impresionar a gente a la que no le importamos." :AUTHOR "Will Smith" :ITEM "" :DATE "")
 (:CATEGORY "WIND" :TEXT "No hay viento bueno para el que no sabe donde vá." :AUTHOR "Séneca" :ITEM "" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "Who controls the past controls the future. Who controls de present controls the past." :AUTHOR "George Orwell" :ITEM "" :DATE "")
 (:CATEGORY "POSSESSION" :TEXT "Puedes tener lo que quieras si estás dispuesto a renunciar a la creencia de que no puedes tenerlo." :AUTHOR "Dr. Robert Anthony" :ITEM "" :DATE "")
 (:CATEGORY "BEING" :TEXT "Ser tú mismo en un mundo que está tratando constantemente de hacerte otra persona es el mayor logro." :AUTHOR "Ralph Waldo Emerson" :ITEM "" :DATE "")
 (:CATEGORY "PATH" :TEXT "Dos caminos se bifurcaban en un bosque, yo tomé el menos transitado, y eso marcó toda la diferencia." :AUTHOR "Robert Frost" :ITEM "" :DATE "")
 (:CATEGORY "SHOOT" :TEXT "Todo tiro no hecho es tiro errado." :AUTHOR "Wayne Gretzky" :ITEM "" :DATE "")
 (:CATEGORY "TREE" :TEXT "El mejor momento para plantar un árbol fue hace 20 años. El segundo mejor momento es ahora." :AUTHOR "Proverbio Chino" :ITEM "" :DATE "")
 (:CATEGORY "TIME" :TEXT "No espere. El tiempo nunca será justo." :AUTHOR "Napoleón Hill" :ITEM "" :DATE "")
 (:CATEGORY "TRUTH" :TEXT "La verdad puede ser desconcertante. Puede tomar algo de trabajo para digerirse. Puede ser contra intuitiva. Puede contradecir prejuicios profundamente arraigados. Puede que no sea acorde con lo que desesperadamente queremos que sea verdad. Pero nuestras preferencias no determinan la verdad." :AUTHOR "Carl Sagan" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Vivir es más importante que buscar el sentido de la vida. Los hermanos Karamazov." :AUTHOR "Dostoyeski" :ITEM "" :DATE "")
 (:CATEGORY "TIME" :TEXT "Your time is limited, so don't waste it living someone else's life… Everything else is secondary." :AUTHOR "Charles Darwin" :ITEM "" :DATE "")
 (:CATEGORY "TIME" :TEXT "Un hombre que se atreve a malgastar una hora de su tiempo no ha descubierto el valor de la vida." :AUTHOR "Plutarco" :ITEM "" :DATE "")
 (:CATEGORY "CURIOUS" :TEXT "Be curious. Read widely. Try new things. What people call intelligence just boils down to curiosity." :AUTHOR "Aaron Swartz" :ITEM "" :DATE "")
 (:CATEGORY "WISDOM" :TEXT "Lo más difícil de la vida es conocerse a uno mismo y lo más fácil es criticar a los demás." :AUTHOR "Tales de Mileto" :ITEM "" :DATE "")
 (:CATEGORY "PARENTS" :TEXT "El signo más evidente de que te has emancipado emocionalmente de tus padres es la sensación de profunda gratitud hacia ellos." :AUTHOR "Clay Newman" :ITEM "" :DATE "")
 (:CATEGORY "BUSINESS" :TEXT "No hay mayor negocio que vender a gente desesperada un producto que asegura eliminar la desesperación." :AUTHOR "Aldous Huxley" :ITEM "" :DATE "")
 (:CATEGORY "THOUGHTS" :TEXT "Cuida tus pensamientos, porque se convertirán en tus palabras. Cuida tus palabras, porque se convertirán en tus actos. Cuida tus actos, porque se convertirán en tus hábitos. Cuida tus hábitos, porque se convertirán en tu carácter. Y tú carácter marcará tu destino." :AUTHOR "Mahatma Gandhi" :ITEM "" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "No olvides que bueno o malo esto también pasará." :AUTHOR "Jorge Bucay" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "Crecerás el día en que verdaderamente te rías por primera vez de ti mismo." :AUTHOR "Ethel Barrymore" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "En nada se revela mejor el carácter de las personas, como en los motivos de su risa." :AUTHOR "Johann Wolfgang von Goethe" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "El día peor empleado es aquel en que no se ha reído." :AUTHOR "Chamfort" :ITEM "" :DATE "")
 (:CATEGORY "FRIEND" :TEXT "Un amigo es uno que lo sabe todo de ti y a pesar de ello te quiere." :AUTHOR "Elbert Hubbart" :ITEM "" :DATE "")
 (:CATEGORY "EXPERIENCE" :TEXT "Un fracasado es un hombre que ha cometido un error y no es capaz de convertirlo en experiencia." :AUTHOR "Elbert Hubbart" :ITEM "" :DATE "")
 (:CATEGORY "CRITIC" :TEXT "Para evitar la crítica, no hagas nada, no digas nada, no seas nada." :AUTHOR "Elbert Hubbart" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Desconocemos el amor de los padres, hasta que tenemos a nuestros propios hijos." :AUTHOR "Henry Ward Beecher" :ITEM "" :DATE "")
 (:CATEGORY "BIRTH" :TEXT "Nacemos al mundo de la Naturaleza; cuando nacemos por segunda vez, es al mundo del espíritu." :AUTHOR "Bhagavad Gita" :ITEM "" :DATE "")
 (:CATEGORY "ISLAND" :TEXT "Ningún ser humano es una isla, cerrada en sí misma, sino que todo hombre es una parte del continente, un pedazo de la tierra firme; si un terrón de tierra es arrastrado por el mar, Europa se ve disminuida, igual que si se tratara de un promontorio, igual que si se tratara de la mansión de tus amigos o de tu propia mansión; la muerte de todo hombre me disminuye, porque yo tengo que ver con la humanidad; no preguntes, pues, nunca por quién doblan las campanas: doblan por ti." :AUTHOR "John Donne" :ITEM "" :DATE "")
 (:CATEGORY "MISFORTUNE" :TEXT "Todas las desdichas del hombre derivan del hecho de que no es capaz de estar sentado tranquilamente, solo, en una habitación." :AUTHOR "Blaise Pascal" :ITEM "" :DATE "")
 (:CATEGORY "WRATH" :TEXT "No serás castigado por tu ira, pero la ira te castigará." :AUTHOR "Buda" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Vive de tal modo que no te avergüence ofrecer el loro de la familia al chismorreo de la calle." :AUTHOR "Will Rogers" :ITEM "" :DATE "")
 (:CATEGORY "TIME" :TEXT "Todos los blancos tienen reloj pero nunca tienen tiempo." :AUTHOR "Dicho africano" :ITEM "" :DATE "")
 (:CATEGORY "ANXIETY" :TEXT "Nada disminuye más rápido la ansiedad que el tomar acción." :AUTHOR "Walter Anderson" :ITEM "" :DATE "")
 (:CATEGORY "OPPORTUNITY" :TEXT "Si la oportunidad no llama construye una puerta." :AUTHOR "Milton Berle" :ITEM "" :DATE "")
 (:CATEGORY "INTELIGNECE" :TEXT "Antes de convencer al intelecto, es imprescindible tocar y predisponer el corazón." :AUTHOR "Blaise Pascal" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Solo es grande en la vida quien sabe ser pequeño. Empieza haciendo lo necesario, después lo posible, y de repente te encontrarás haciendo lo imposible." :AUTHOR "San Francisco de Asís" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Cuando la vida te ofrezca un limón, exprímelo y haz limonada." :AUTHOR "W. Clement Stone" :ITEM "" :DATE "")
 (:CATEGORY "SUN" :TEXT "Ponte frente al sol y las sombras quedarán detrás de ti." :AUTHOR "Proverbio Mahorí" :ITEM "" :DATE "")
 (:CATEGORY "THINK" :TEXT "Lo que pienses lo serás, lo que sientas lo atraerás, lo que imaginas lo crearás." :AUTHOR "Buda" :ITEM "" :DATE "")
 (:CATEGORY "BUSINESS" :TEXT "Our main business is not to see what lies dimly at a distance, but to do what lies clearly at hand." :AUTHOR "Thomas Carlyle" :ITEM "" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "Por tanto, no os preocupéis por el día de mañana; porque el día de mañana se cuidará de sí mismo. Bástele a cada día sus propios problemas." :AUTHOR "Mateo" :ITEM "6:34" :DATE "")
 (:CATEGORY "BEING" :TEXT "La tensión es quien crees que deberías ser. La relajación es quien eres." :AUTHOR "Proverbio Chino" :ITEM "" :DATE "")
 (:CATEGORY "NEED" :TEXT "Siempre se necesita aquello que no se sabe y aquello que se sabe no tiene ningún uso." :AUTHOR "Johann Wolfgang von Goethe" :ITEM "Fausto" :DATE "")
 (:CATEGORY "POSSIBLE" :TEXT "Para que pueda surgir lo posible es necesario probar una y otra vez lo imposible." :AUTHOR "Hermann Hesse" :ITEM "" :DATE "")
 (:CATEGORY "PHANTOMS" :TEXT "Los fantasmas asustan más de lejos que de cerca." :AUTHOR "Nicolás Maquiavelo" :ITEM "" :DATE "")
 (:CATEGORY "BREVITY" :TEXT "Brevity is the soul of wit." :AUTHOR "foo at acm.org>" :ITEM "" :DATE "")
 (:CATEGORY "EDUCATION" :TEXT "Educar a un niño no es hacerle aprender algo que no sabía, sino hacer de él alguien que no existía." :AUTHOR "John Ruskin" :ITEM "" :DATE "")
 (:CATEGORY "TREE" :TEXT "El árbol torcido vive su vida, mientras el árbol derecho termina convertido en tabla." :AUTHOR "Proverbio Chine" :ITEM "" :DATE "")
 (:CATEGORY "PRESENT" :TEXT "La verdadera generosidad para con el futuro consiste en entregarlo todo al presente." :AUTHOR "Albert Camus" :ITEM "" :DATE "")
 (:CATEGORY "DREAMS" :TEXT "He tenido sueños y he tenido pesadillas. Superé mis pesadillas gracias a mis sueños." :AUTHOR "Jonas Salk" :ITEM "" :DATE "")
 (:CATEGORY "PROJECT" :TEXT "Cuando te inspira un objetivo importante, un proyecto extraordinario, todos tus pensamientos rompen sus ataduras; tu mente supera los límites, tu conciencia se expande en todas direcciones y tú te ves en un mundo nuevo y maravilloso. Las fuerzas, facultades y talentos ocultos cobran vida y descubres que eres una persona mejor de lo que habías soñado ser." :AUTHOR "Patanjali" :ITEM "" :DATE "")
 (:CATEGORY "REALITY" :TEXT "La realidad existente no puede realmente ser rechazada ni aceptada. Huir de ella es como tratar de huir de tus propios pies. Aceptarla es como tratar de besar tus propios labios. Todo lo que hay que hacer es mirar, comprender y estar en paz." :AUTHOR "Anthony de Mello" :ITEM "La oración de la rana" :DATE "")
 (:CATEGORY "KNOWLEDGE" :TEXT "Trata de probar a qué sabe la ausencia de saber." :AUTHOR "Saraha" :ITEM "" :DATE "")
 (:CATEGORY "THINK" :TEXT "Thus, the task is, not so much to see what no one has yet seen; but to think what nobody has yet thought, about that which everybody sees." :AUTHOR "Erwin Schrödinger" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "For what it's worth… it's never too late, or in my case too early, to be whoever you want to be. There's no time limit. Start whenever you want.
You can change or stay the same. There are no rules to this thing. We can make the best or the worst of it.
I hope you make the best of it. I hope you see things that startle you. I hope you feel things you've never felt before. I hope you meet people who have a different point of view.
I hope you live a life you're proud of, and if you're not. I hope you have the courage to start over again." :AUTHOR "F. Scott Fitzgerald" :ITEM "" :DATE "")
 (:CATEGORY "CLEANING" :TEXT "7 secretos para limpiar tu casa y tu mente:
1. Asume la tarea con la actitud adecuada
2. Céntrate solo en limpiar
3. Comienza en silencio
4. Deja que circule el aire
5. Limpia los objetos con cuidado y cariño
6. Experimenta gratitud hacia las cosas que te sirvieron
7. Divide la limpieza con quienes compartas la casa." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CRISIS" :TEXT "En tiempos de crisis los inteligentes buscan soluciones y los inútiles culpables." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Silly is a state of Mind, Stupid is a way of Life." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "El filósofo William James decía que en la vida siempre habrá verdaderas pérdidas y verdaderos perdedores. La tragedia no se puede evitar en la vida, aunque solo sea porque responder a preguntas morales importantes del tipo " ¿CÓMO DEBERÍA VIVIR? " siempre hace que un bien entre en conflicto con otro. Solo una mente estrecha y limitada no vería los muchos " YOES " que uno podría haber sido, o los múltiples caminos que habría podido seguir en la vida pero que se han perdido o sacrificado para que uno sea lo que es ahora o que para que lleve la vida tiene. Nunca hay un yo único que poder ser, un yo más auténtico que descubrir o un yo más desarrollado al que aspirar; como tampoco hay un único e inequívoco bien o meta a la que aspirar. Y eso se aplica también a la felicidad. Con toda decisión moral, sea más o menos libre, sea personal o colectiva, siempre se sacrifica algo bueno —algún yo que podríamos ser, algún valor por el que merece la pena luchar o algún proyecto social que desarrollar—. Ahí reside una de las tragedias irreductibles de la vida y que acompañan a toda elección, tanto grande como pequeña, tanto social como personal o política. Ni la mejor de las ciencias de la felicidad prometidas podrán ahorrarnos esas grandes y pequeñas pérdidas que inextricablemente acompañan a los grandes y pequeños sacrificios que hacemos en la vida." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LATER" :TEXT "El día acaba de empezar… y ya son las seis de la tarde,
Acaba de llegar el lunes y estamos ya en viernes,
… y el mes se ha terminado,
… y el año está casi acabado,
… y ya han pasado 40, 50, o 60 años de nuestras vidas,
… y nos damos cuenta que hemos perdido a nuestros padres, a nuestros amigos.
… y nos damos cuenta que es demasiado tarde para volver atrás… Entonces,
… Intentemos, a pesar de todo, disfrutar a fondo del tiempo que nos queda…
No paremos en buscar actividades que nos gusten… Pongamos colores en nuestra grisalla… Sonriamos ante las pequeñas cosas de la vida que animan nuestros corazones.
Y a pesar de todo tenemos que continuar disfrutando con serenidad del tiempo que nos queda.
Intentemos eliminar los «Después»…
Lo hago «Después»…
Diré «Después»…
Lo pensaré «Después»…
Dejamos todo para más tarde como si el «Después» fuese nuestro.
Porque lo que no comprendemos es que:
– «Después», el café se enfría…
– «Después», las prioridades cambian…
– «Después», el encanto se rompe…
– «Después», la salud pasa…
– «Después», las niñas y los niños crecen…
– «Después», las madres y los padres envejecen…
– «Después», las promesas se olvidan…
– «Después», el día se convierte en noche…
– «Después», la vida se termina…
Y «Después» es a menudo demasiado tarde…
Entonces… no dejemos nada para más tarde…
Puesto que dejando siempre todo para más tarde podemos perder los mejores momentos,
las mejores experiencias,
los mejores amigos,
la mejor familia…
El día es hoy… el instante es ahora…
No estamos en una edad en la que podamos permitirnos dejar para mañana lo que debe ser hecho inmediatamente. En el trabajo, por las amigas, por los amigos, las hermanas, los hermanos, por la familia,
Entonces veamos si tienes tiempo para leer este mensaje y compartirlo,
O entonces lo dejarás quizás para… «Después»…
Y no lo compartirás «Nunca»." :AUTHOR "Boucar Diouf" :ITEM "Después" :DATE "")
 (:CATEGORY "LOOSE" :TEXT "No importa la cantidad de leche que derrames. Lo importante es no perder la vaca." :AUTHOR "Dicho tejano" :ITEM "" :DATE "")
 (:CATEGORY "REJECT" :TEXT "Estaba allí desde el primer momento,
en la adrenalina
que circulaba por las venas de tus padres
cuando hacían el amor para concebirte,
y después en el fluido
que tu madre bombeaba a tu pequeño corazón
cuando todavía eras sólo un parásito.
Llegué a ti antes de que pudieras hablar,
antes aun de que pudieras entender algo
de lo que los otros te hablaban.
Estaba ya, cuando torpemente
intentabas tus primeros pasos
ante la mirada burlona y divertida de todos.
Cuando estabas desprotegido y expuesto,
cuando eras vulnerable y necesitado.
Aparecí en tu vida
de la mano del pensamiento mágico,
me acompañaban…
las supersticiones y los conjuros,
los fetiches y los amuletos…
las buenas formas, las costumbres y la tradición…
tus maestros, tus hermanos y tus amigos…
Antes de que supieras que yo existía,
yo dividí tu alma en un mundo de luz y uno de oscuridad.
Un mundo de lo que está bien y otro de lo que no lo está.
Yo te traje tus sentimientos de vergüenza,
te mostré todo lo que hay en ti de defectuoso,
de feo,
de estúpido,
de desagradable.
Yo te colgué la etiqueta de «DIFERENTE»
cuando te dije por primera vez al oído
que algo no andaba del todo bien contigo.
Existo desde antes de la conciencia,
desde antes de la culpa,
desde antes de la moralidad,
desde los principios del tiempo,
desde que Adán se avergonzó de su cuerpo
al notar que estaba desnudo…
y lo cubrió.
Soy el invitado no querido,
el visitante no deseado,
y sin embargo
soy el primero en llegar y el último en irme.
Me he vuelto poderoso con el tiempo,
escuchando los consejos de tus padres sobre cómo
triunfar en la vida.
Observando los preceptos de tu religión,
que te dicen qué hacer y qué no hacer
para poder ser aceptado por Dios en su seno.
Sufriendo las bromas crueles
de tus compañeros de colegio,
cuando se reían de tus dificultades.
Soportando las humillaciones de tus superiores.
Contemplando tu desgarbada imagen en el espejo
y comparándola después con las de los «EXITOSOS»
que se muestran por televisión.
Y ahora, por fin.
poderoso como soy
y por el simple hecho
de ser mujer,
de ser negro,
de ser judío,
de ser homosexual,
de ser oriental,
de ser discapacitado,
de ser alto, petiso, o gordo…
puedo transformarte…
en un tacho de basura,
en escoria,
en un chivo expiatorio,
en el responsable universal,
en un maldito
bastardo desechable.
Generaciones y generaciones de hombres y mujeres
me apoyan.
No puedes librarte de mí.
La pena que causo es tan insostenible
que para soportarme,
deberás pasarme a tus hijos,
para que ellos me pasen a los suyos,
por los siglos de los siglos.
Para ayudarte a ti y a tu descendencia,
me disfrazaré de perfeccionismo,
de altos ideales,
de autocrítica,
de patriotismo,
de moralidad,
de buenas costumbres,
de autocontrol.
La pena que te causo es tan intensa
que querrás negarme
y para eso
intentarás esconderme detrás de tus personajes,
detrás de las drogas,
detrás de tu lucha por el dinero,
detrás de tus neurosis
detrás de tu sexualidad indiscriminada.
Pero no importa lo que hagas,
no importa adónde vayas,
yo estaré allí
siempre allí.
Porque viajo contigo
día y noche
sin descanso,
sin límites.
Yo soy la causa principal de la dependencia,
de la posesividad,
del esfuerzo,
de la inmoralidad,
del miedo,
de la violencia,
del crimen,
de la locura.
Yo te enseñé el miedo a ser rechazado,
y condicioné tu existencia a ese miedo.
De mí dependes para seguir siendo
esa persona buscada, deseada,
aplaudida, gentil y agradable
que hoy muestras a los otros.
De mí dependes
porque yo soy el baúl en el que escondiste
aquellas coas más desagradables,
más ridículas,
menos deseables de ti mismo.
Gracias a mí,
has aprendido a conformarte
con lo que la vida te da,
porque después de todo,
cualquier cosa que vivas será siempre más
de lo que crees que mereces.
¿Has adivinado, verdad?
Soy el sentimiento de rechazo que sientes por ti mismo.
SOY… EL SENTIMIENTO DE RECHAZO
QUE SIENTES POR TI MISMO.
Recuerda nuestra historia…
Todo empezó aquel día gris
en que dejaste de decir orgulloso:
¡YO SOY!
y entre avergonzado y temeroso,
bajaste la cabeza
y cambiaste tus dichos y actitudes
por un pensamiento:
YO DEBERÍA SER…" :AUTHOR "Leo Booth" :ITEM "Autorechazo" :DATE "")
 (:CATEGORY "WORLD" :TEXT "The problem with the world is that the intelligent people are full of doubts, while the stupid ones are full of confidence." :AUTHOR "Charles Bukowski " :ITEM "" :DATE "")
 (:CATEGORY "BAGGAGE" :TEXT "No cambiéis, el deseo de cambiar es enemigo del amor.
No os cambiéis a vosotros mismos: amaos a vosotros mismos tal y como sois.
No hagáis cambiar a los demás: amad a todos tal como son.
No intentéis cambiar el mundo: el mundo está en manos de Dios y él lo sabe. Y si lo hacéis así… todo cambiará maravilosamente a su tiempo y a su manera.
Hizo una pausa y añadió sus últimas palabras:
Dejaos llevar por la corriente de la vida, ligeros de equipaje…
Así se fue él." :AUTHOR "Anthony de Mello" :ITEM "Ligero de equipaje (Carlos G. Vallés)" :DATE "")
 (:CATEGORY "TRUTH" :TEXT "- Si aspiran a la verdad (explicaba Nasrudín a un grupo de buscadores que habían venido a escuchar sus enseñanzas) tendrán que pagar por ella.
- Pero, ¿Por qué se debe de pagar por algo como la verdad?
- ¿No han notado ustedes que la escasez de una cosa determina su valor?." :AUTHOR "Idries Shah" :ITEM "Las ocurencias del increíble mulá Nasrudín" :DATE "")
 (:CATEGORY "SMART" :TEXT "Un intelectual es el que dice una cosa simple de un modo complicado. Un artista es el que dice una cosa complicada de un modo simple." :AUTHOR "" :ITEM "Escritos de un viejo indecente." :DATE "1969")
 (:CATEGORY "LAW" :TEXT "Siempre me ha fascinado la ley del esfuerzo invertido que a veces llamo «la  ley de la retrocesión». Cuando intentas permanecer en la superficie del agua te hundes pero cuando tratas de sumergirte flotas. Cuando retienes el aliento lo pierdes lo cual hace pensar enseguida en un dicho muy antiguo y al que se hace muy poco caso: «Quién salve su alma la perderá»." :AUTHOR "Alan Watts" :ITEM "" :DATE "")
 (:CATEGORY "AMBITION" :TEXT "Mi ambición está limitada por mi pereza." :AUTHOR "Factotum " :ITEM "" :DATE "1975")
 (:CATEGORY "DIFFERENCE" :TEXT "La diferencia entre un valiente y un cobarde, es que un cobarde se lo piensa dos veces antes de saltar a la jaula con un león. El valiente simplemente no sabe lo que es un león. Sólo cree que lo sabe." :AUTHOR "" :ITEM "Escritos de un viejo indecente." :DATE "1969")
 (:CATEGORY "MORAL" :TEXT "Las personas sin moral a menudo se consideran a sí mismos más libres, pero la mayoría eran incapaces de sentir odio o amor." :AUTHOR "" :ITEM "Mujeres" :DATE "1978")
 (:CATEGORY "PEOPLE" :TEXT "La gente no necesita amor, lo que necesita es triunfar en una cosa u otra. Puede ser en el amor, pero no es imprescindible." :AUTHOR "" :ITEM "Factotum" :DATE "1975")
 (:CATEGORY "MEN" :TEXT "El hombre, a pesar de su bravuconería es el fiel, el que generalmente siente el amor. La mujer es experta en la traición y la tortura y la perdición. Nunca envidies la mujer de un hombre. Detrás de todo esto está el infierno en vida." :AUTHOR "" :ITEM "Aparecido en una carta a Steven Richmond." :DATE "")
 (:CATEGORY "RELIGION" :TEXT "If a «religion» is defined to be a system of ideas that contains unprovable statements, then Gödel taught us that mathematics is not only a religion, it is the only religion that can prove itself to be one." :AUTHOR "John D. Barrow" :ITEM "The Artful Universe" :DATE "1995")
 (:CATEGORY "HEART" :TEXT "El mediador entre el cerebro y las manos ha de ser el corazón." :AUTHOR "Fritz Lang" :ITEM "Metropolis" :DATE "1927")
 (:CATEGORY "LIFE" :TEXT "Todos padecemos una enfermedad incurable: La vida" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "¿Porqué me debería preocupar por la posteridad?. ¿Qué ha hecho la posteridad por mi?." :AUTHOR "Groucho Marx" :ITEM "" :DATE "")
 (:CATEGORY "SOMEBODY" :TEXT "El problema es que buscamos a alguien con quien envejecer… Y realmente hay que encontrar a alguien con quien seguir siendo niños." :AUTHOR "Charles Bukowski" :ITEM "" :DATE "")
 (:CATEGORY "WORRIES" :TEXT "Deja de preocuparte por cosas que no han ocurrido." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "Tema recurrente y actual… es que estamos rodeados…
No debemos confundir al estúpido con el idiota o con el imbécil…
Es mucho más peligroso un IMBÉCIL que un IDIOTA y, por último, que un ESTÚPIDO.
Vamos a intentar explicarlo…
ESTÚPIDO. Del Latín Stupidus, significa sorprendido o asombrado, de ahí estupefacto. Los romanos empezaron a aplicarla de modo despectivo a aquellos que eran un tanto timoratos o que se asombraban por todo. Resumiendo, un estúpido es un alelado o un pasmado. Es decir, se llama estúpido a toda persona que realiza una estupidez fruto de ese pasmo. Por estupidez nos referimos a un acto que no necesariamente provoca un mal a alguien pero que ocurre por una carencia de sentido común en el individuo que lo realiza.
IDIOTA. Idiota proviene del griego Idiotes, palabra con la que se designaba a las personas inexpertas o profanas en algún tema o profesión. A lo largo de los siglos el significado fue variando hasta que en el siglo XII entró en nuestro idioma proveniente del francés Idiot que significa persona ignorante. Es decir, un idiota es un ignorante.
El sujeto es consciente del acto que está llevando a cabo. Sin embargo, lo realiza de todas formas, porque su sentido común no le da para hacer un acto más inteligente. No da para más.
IMBÉCIL.  Proviene del Latín Imbecillis y significa persona débil o enjuta. Aunque en un principio hacía referencia a una dolencia física, con el devenir del tiempo cambió para definir un mal mental y así imbécil era “débil mental”. Imbécil, parece ser, también podría derivar de in báculo (sin bastón), esto es, sin principios ni valores.   El imbécil también es plenamente consciente del acto que está realizando. Sin embargo, y ahí estriba la peligrosidad del imbécil, lo ha planeado y ha desarrollado estrategias para ascender socialmente a base de imbecilidades. El acto de imbecilidad, a diferencia del de idiotez o estupidez, está orientado a la aceptación de los que considera sus pares. Es decir, va dirigido hacia un grupo específico .
Según Savater hay varios tipos de imbéciles:
- El imbécil que no cree en nada y que todo le da igual.
- Aquél que cree que lo quiere todo y luego paradójicamente no hace nada.
- El que no sabe ni quiere saber, simplemente sigue a la masa como si fuera una oveja más del rebaño.
- El que sabe lo que quiere pero no se molesta en conseguirlo ya que no le apetece esforzarse.
- El imbécil que tiene mucho ímpetu pero no es capaz de diferenciar lo bueno de lo malo." :AUTHOR "David López Lluch" :ITEM "" :DATE "20151217" :URL "https://davidblopezlluch.edu.umh.es/2015/12/17/717/")
 (:CATEGORY "LIFE" :TEXT "- ¿Y usted cómo ha llegado a su edad y se ha mantenido tan bien?.
- Porque nunca he discutido con nadie.
- No hombre, no será por eso…
- Pues no será no…" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SHEEPS" :TEXT "De lo que se trata no es de cambiar de pastor, sino dejar de ser ovejas." :AUTHOR "Estanislao Zulueta" :ITEM "" :DATE "")
 (:CATEGORY "GOVERMENT" :TEXT "Cuando los gobiernos temen a la gente hay libertad, Cuando la gente teme al gobierno hay tiranía." :AUTHOR "Thomas Jefferson" :ITEM "" :DATE "")
 (:CATEGORY "DEATH" :TEXT "Un condenado a muerte sube al patíbulo, resbala en un peldaño y dice: No es mi día." :AUTHOR "Michel Poiccard" :ITEM "Al final de la escapada" :DATE "1960")
 (:CATEGORY "PAST" :TEXT "Puede que hayamos terminado con nuestro pasado pero nuestro pasado no ha terminado con nosotros." :ITEM "Magnolia" :DATE "1999")
 (:CATEGORY "FAILURE" :TEXT "Que el fracaso te sorprenda intentándolo." :ITEM "" :DATE "")
 (:CATEGORY "WOMEN" :TEXT "La mujer ideal es una princesa en la vida social, una avara en los gastos y una puta en la cama, no como la mía que es una princesa en los gastos, una puta en la vida social y una avara en la cama." :AUTHOR "Schopenhauer" :ITEM "")
 (:CATEGORY "REVENGE" :TEXT "Crom, jamás te había rezado antes, no sirvo para ello, nadie ni siquiera tú recordarás si fuimos hombres buenos o malos porqué luchamos o porque morimos, no. Lo único que importa es que dos se enfrentan a muchos; eso es lo que importa. El valor te agrada Crom. Concédeme pues una petición. Concédeme la venganza, y si no me escuchas, ¡Vete al infierno!." :AUTHOR "Conan" :ITEM "Conan el bárbaro" :DATE "1982")
 (:CATEGORY "ENVY" :TEXT "La envidia no es más que una forma de admiración mal gestionada." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "POLITICS" :TEXT "La política es el camino para que los hombres sin principios puedan dirigir a los hombres sin memoria." :AUTHOR "Voltaire" :ITEM "" :DATE "")
 (:CATEGORY "CHAOS" :TEXT "Todo lo que tengo en común con los incontrolables y los locos, los viciosos y lo malvados, todo el caos que he causado y mi total indiferencia hacia él, ahora lo he superado. Mi dolor es constante y agudo y no espero un mundo mejor para nadie. De hecho, quiero que mi dolor se inflingia a otos. No quiero que nadie escape. Pero incluso después de admitir esto, no hay catarsis; mi castigo continúa eludiéndome y no obtengo un conocimiento más profundo de mi mismo. No se puede extraer ningún conocimiento nuevo de mi relato. Esta confesión no ha significado nada." :AUTHOR "Patrick Bateman" :ITEM "American Psycho" :DATE "2000")
 (:CATEGORY "HISTORY" :TEXT "Si hemos aprendido algo de la historia de los descubrimientos e innovaciones es que, a largo plazo y a menudo (y también a corto), las profecías más audaces resultan ridículamente conservadoras." :AUTHOR "Arthur C. Clarke" :ITEM "" :DATE "")
 (:CATEGORY "DREAMS" :TEXT "Perceiving is dreaming true. Dreaming is perceiving free." :AUTHOR "Stephen LaBerge" :ITEM "" :DATE "")
 (:CATEGORY "FIGHT" :TEXT "Aprendí hace tiempo a no luchar con un cerdo. Tú te ensucias y al cerdo le gusta." :AUTHOR "George Bernard Shaw" :ITEM "" :DATE "")
 (:CATEGORY "WORLD" :TEXT "Hay otros mundos pero están en éste" :AUTHOR "Paul Éluard" :ITEM "" :DATE "")
 (:CATEGORY "SUCCESS" :TEXT "El éxito es como los pedos, la gente se molesta cuando no es suyo" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "El trabajo se expande para llenar el tiempo disponible para su realizaión." :AUTHOR "" :ITEM "Ley de Parkinson" :DATE "")
 (:CATEGORY "WINE" :TEXT "No bebo por el propio placer del vino ni para acarnecer la fe, lo hago para olvidarme de mi mismo por un momento, tan solo eso deseo de la embriagez, solo eso." :AUTHOR "Omar Khayyam" :ITEM "" :DATE "")
 (:CATEGORY "HUMAN" :TEXT "El resultado fue la aparición del ser humano, como le conocemos: un animal hiperansioso que inventa constantemente razones para su ansiedad, incluso cuando no existe ninguna." :AUTHOR "Denial of death" :ITEn "Ernest Becker" :DATE "")
 (:CATEGORY "GROW" :TEXT "Crecer es ocultar la masa de tejido interno cicatrizado que palpita en nuestros sueños" :AUTHOR "Ernest Becker" :ITEM "" :DATE "")
 (:CATEGORY "MADNESS" :TEXT "No estar loco equivaldría a otra forma de locura" :AUTHOR "Blaise Pascal" :ITEM "" :DATE "")
 (:CATEGORY "LONELY" :TEXT "Y cuando nadie te despierta por la mañana, y cuando nadie te espera en la noche, y cuando puedes hacer lo que quieras. ¿Cómo lo llamas?. ¿Libertad o Soledad?." :AUTHOR "Charles Bukowski" :ITEM "" :DATE "")
 (:CATEGORY "PRICE" :TEXT "Hoy en día la gente sabe el precio de todo y el valor de nada." :AUTHOR "Oscar Wilde" :ITEM "" :DATE "")
 (:CATEGORY "FORTRAN" :TEXT "Fortran programmers know the values of a few things and de cost of a few very small things." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HISTORY" :TEXT "Quizá la única lección que nos enseña la historia es que los seres humanos no aprendemos nada de las lecciones de la historia." :AUTHOR "Aldoux Huxley" :ITEM "" :DATE "")
 (:CATEGORY "SUICIDE" :TEXT "Junto al cadaver de un suicida se encontro una carta dirigida al Juez, que decia:
No culpe a nadie de mi muerte, me quito la vida porque dos días mas que viviese no sabria quien soy en este mar de lagrimas…
Vera usted Sr. Juez, Tuve la desgracia de casarme con una viuda, ésta tenia una hija; de haberlo sabido, nunca lo hubiera hecho.
Mi padre, para mayor desgracia era viudo, se enamoró y se caso con la hija de mi mujer, de manera que mi mujer era suegra de su suegro, mi hijastra se convirtio en mi madre y mi padre al mismo tiempo era mi yerno.
Al poco tiempo mi madrastra trajo al mundo un varón, que era mi hermano, pero era nieto de mi mujer de manera que yo era abuelo de mi hermano.
Con el correr del tiempo mi mujer trajo al mundo un varón que como era hermano de mi madre, era cuñado de mi padre y tio de sus hijos.
Mi mujer era suegra de su hija,yo soy, en cambio padre de mi madre, y mi padre y su mujer son mis hijos; Ademàs, yo soy mi propio abuelo.
Sr. Juez: me despido del mundo porque no se quien soy." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PERSISTENCE" :TEXT "Nada en el mundo puede reemplazar a la persistencia. Ni el talento, porque nada es mas común que un hombre talentoso sin éxito. Ni la genialidad, los genios sin recompensa es casi un proverbio. Ni la educación, el mundo está lleno de abandonos escolares. La persistencia y la determinación solas son omnipotentes. El eslogan Sigue Hacia Delante ha resuelto y resolverá los problemas de la humanidad." :AUTHOR "Calvin Cooldige" :ITEM "" :DATE "")
 (:CATEGORY "SUPERIOR" :TEXT "Si un hombre es superior a mí en algún sentido, en ese sentido aprendo de él." :AUTHOR "" :ITEM "Dale Carnegie" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "Las personas rara vez triunfan, a no ser que se diviertan en lo que hacen. Dale Carnegie" :AUTHOR "Dale Carnegie" :ITEM "" :DATE "")
 (:CATEGORY "PRAY" :TEXT "Gracias por el techo, la cama y el agua potable. Gracias por saber leer, escribir y contar. Gracias por haber tenido la suerte de nacer entre el cinco por ciento más rico del mundo sin hacer nada para merecerlo. Gracias por no saber qué es huir y dejarlo todo atrás por culpa del hambre o la guerra.
Gracias al hospital, la farola, y el camión de la basura por ser rutina. Gracias a la cocina, el radiador y la caldera por darme calor sin esfuerzo. Gracias a los vehículos y a la tecnología por acercarme a los que están lejos. Gracias a internet por abrirme las puertas de todo. Gracias a los grifos. Gracias al interruptor por obrar el milagro diario de la luz artificial.
No ser creyente no me impide creer. Creo en mantener los ojos abiertos. En no dejarme domesticar por la comodidad. En contribuir, en cooperar, en concienciar, aunque sea por ganarme unos gramos de dignidad.
Pido disculpas por la autocomplacencia. Por creerme Occidente cuando vivimos en un planeta redondo. Por este estilo de vida depredador que tira la comida caducada y guarda en el armario cosas sin estrenar. Pido disculpas por esas veces que olvido que tengo más de lo que necesito para ser feliz. Pido disculpas porque sé que mi bienestar está cimentado sobre el malestar de casi todos los demás.
Ésta es una oración sin sujeto al que predicar. Una oración laica sin dios, virgen, ni imagen que idolatrar. Y no es plegaria ni ruego ni súplica porque no tengo ningún derecho a pedir." :AUTHOR "" :ITEM "Oración laica" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "Existe un tipo de persona que siente un gran desdén por la “inmediatez”, que intenta cultivar su interioridad, que basa su orgullo en algo más profundo e interno, que crea una distancia entre ella y una persona corriente. Kierkegaard denomina a este tipo de persona «introvertida». Le preocupa algo más lo que significa ser una persona, con individualidad y carácter único. Le gusta la soledad y se retira de vez en cuando para reflexionar, o quizás para alimentar ideas sobre su yo secreto, como quiera que este sea. Al fin y al cabo, esto es dicho y hecho, es el único problema real de la vida, la única preocupación de la persona que realmente vale la pena: ¿Cuál es su verdadero talento, su regalo secreto, su auténtica vocación? ¿De qué modo uno es verdaderamente único y cómo puede expresar este carácter exclusivo, darle forma, dedicarlo a algo que sea superior a ella? ¿Cómo puede la persona recurrir a su ser interno privado, el gran misterio que siente en lo más hondo de su corazón, de sus emociones, de sus anhelos, y utilizarlo para vivir de un modo más distintivo, para que con la peculiar calidad de su talento la enriquezca a ella y a la humanidad?." :AUTHOR "Ernest Becker" :ITEM "La negación de la muerte" :DATE "")
 (:CATEGORY "HAPPINESS" :TEXT "Los hombres olvidan siempre que la felicidad humana es una disposición de la mente y no una condición de las circunstancias." :AUTHOR "John Locke" :ITEM "" :DATE "")
 (:CATEGORY "FUTURE" :TEXT "Las personas llegarán a amar su opresión, a adorar las tecnologías que deshacen su capacidad de pensar." :AUTHOR "Aldoux Huxley" :ITEM "" :DATE "")
 (:CATEGORY "BOOKS" :TEXT "No hace falta quemar libros si el mundo empieza a llenarse de gente que no lee, que no aprende, que no sabe." :AUTHOR "Ray Bradbury" :ITEM "" :DATE "")
 (:CATEGORY "POLITICS" :TEXT "La política es el arte de impedir que la gente se entrometa en lo que le atañe." :AUTHOR "Paul Valery" :ITEM "" :DATE "")
 (:CATEGORY "HOMELAND" :TEXT "La verdadera patria del hombre es la infancia." :AUTHOR "Rainer María Rilke" :ITEM "" :DATE "")
 (:CATEGORY "MEANINGS" :TEXT "Me pone incómodo hablar de significados y cosas. Es mejor no saber mucho acerca de lo que significan las cosas, debido a que el significado es algo muy personal" :AUTHOR "David Lynch" :ITEM "" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "This book is dedicated to Dennis Ritchie and to Steve Jobs. To Dennis for giving us the tools to program. To Steve for giving us a reason to program." :AUTHOR "JT Kalnay" :ITEM "88 C programs" :DATE "2012")
 (:CATEGORY "DEATH" :TEXT "¿Para qué extrañar a los muertos?. No es necesario. Ellos están en un mejor lugar. ¿Por qué mejor no extrañas a ti?. No eres ni la sombra de lo que deseabas ser." :AUTHOR "Charles Bukowski" :ITEM "" :DATE "")
 (:CATEGORY "POLITICS" :TEXT "Uno de los castigos por rehusarte en política es que terminarás siendo gobernado por hombres inferiores a ti." :AUTHOR "Platón" :ITEM "" :DATE "")
 (:CATEGORY "SCIENCE" :TEXT "Si he hecho descubrimientos invaluables, ha sido más por tener paciencia que por cualquier otro talento." :AUTHOR "Iscaac Newton" :ITEM "" :DATE "")
 (:CATEGORY "FIRE" :TEXT "Estaba acostado en la cama una noche y pensé que iba a dejarlo todo, otra pequeña voz dentro de mi dijo: No te rindas. Salva esa pequeña brasa de chispa. Nunca des esa chispa porque mientras la tengas puedes comenzar el fuego de nuevo." :AUTHOR "Charles Bukowski" :ITEM "" :DATE "")
 (:CATEGORY "MEDIOCRICY" :TEXT "La mediocridad, posiblemente consiste en estar delante de la grandeza y no darse cuenta." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SOUL" :TEXT "Siempre me ha fascinado la ley del esfuerzo invertido que a veces llamo 'la ley de la retrocesión'. Cuando intentas permanecer en la superficie del agua te hundes pero cuando tratas de sumergirte flotas. Cuando retienes el aliento lo pierdes lo cual hace pensar enseguida en un dicho muy antiguo y al que se hace muy poco caso: 'Quién salve su alma la perderá'.
" :AUTHOR "Alan Watts" :ITEM "" :DATE "")
 (:CATEGORY "Mi ambición está limitada por mi pereza" :TEXT "" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "Un estudiante le preguntó a la antropóloga Margaret Mead que cuál consideraba que fue la primera señal de civilización humana. El estudiante esperaba una respuesta tipo 'lanzas para cazar' o 'redes para pescar' o 'vasijas para cocinar'. Pero no, la Dra. Mead dijo que la primera señal de civilización fue el hallazgo de un fémur que había estado fracturado y después había sanado. Mead explicó que en el reino animal, si te rompes un hueso, estás muerto. No puedes correr del peligro, no puedes caminar al río para beber agua, ni puedes cazar para comer. Te conviertes en presa para un predador. Ningún animal es capaz de tener un hueso roto y sobrevivir lo suficiente para sanarlo.  Un fémur que estuvo roto y fue sanado, significa que alguien se quedó un tiempo para atender a esa persona lastimada, limpió su herida, la llevó a un lugar seguro, le dio de comer y beber y estuvo a su lado hasta su recuperación. 'Ayudar a alguien en momentos difíciles, o cuando lo necesita es cuando comienza la civilización', dijo la Dra. Mead. Somos civilizados y mostramos lo mejor de nuestra humanidad cuando servimos al prójimo." :TEXT "" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "La biblioteca es la más democrática de las instituciones, porque nadie en absoluto puede decirnos qué leer, cuándo y cómo." :TEXT "" :AUTHOR "Doris Lessing" :ITEM "" :DATE "")
 (:CATEGORY "Mi sobrino de pequeño se puso un bañador rosa con un pez dibujado para ir a la piscina. Unos niños señalando su bañador, le gritaron: '¡Mariquita!, ¡Mariquita!', y él les dijo con cara de asombro: 'No, es un pez'. " :TEXT "" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "En un país bien gobernado, la pobreza es algo que avergüenza. En un país mal gobernado, la riqueza es algo que avergüenza." :TEXT "" :AUTHOR "Confucio" :ITEM "" :DATE "")
 (:CATEGORY "Sigue a tu corazón pero lleva contigo a tu cerebro" :TEXT "" :AUTHOR "Alfred Adler" :ITEM "" :DATE "")
 (:CATEGORY "Para hacerse amo, e incluso para hacerse libre, el siervo le arrebata al amo el látigo para autoflagelarse." :TEXT "" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Gracias a la vida que me ha dado tanto. Me ha dado el sonido y el abecedario. Con él las palabras que pienso y declaro. Madre, amigo y luz alumbrando. La ruta del alma del que estoy amando." :AUTHOR "Violeta Parra" :ITEM "" :DATE "")
 (:CATEGORY "DECISSION" :TEXT "Las buenas decisiones vienen de la experiencia, pero la experiencia viene de las malas decisiones." :AUTHOR "Mark Twain" :ITEM "" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "Cuando otras personas esperan de nosotros que seamos como ellos quieren, nos obligan a destruir a la persona que realmente somos." :AUTHOR "Jim Morrison" :ITEM "" :DATE "")
 (:CATEGORY "ERRORS" :TEXT "Solo aquéllos que cometen errores están preparados para acometer grandes tareas." :AUTHOR "BUSHIDO" :ITEM "" :DATE "")
 (:CATEGORY "PROBLEMS" :TEXT "Los nuevos problemas no son las consecuencias de fracasos accidentales sino de los éxitos de la tecnología." :AUTHOR "Barry Commoner" :ITEM "" :DATE "")
 (:CATEGORY "AMAZE" :TEXT "Lo más elevado que el ser humano puede alcanzar es su capacidad de asombro; y si lo fenómenos esenciales le hacen asombrase, dejadle que sea feliz; no puede recibir nada más elevado, y nada debe buscar más allá de esto; aquí está el límite." :AUTHOR "Johann Wolfgang von Goethe" :ITEM "" :DATE "")
 (:CATEGORY "AUTOCONICUSNESS" :TEXT "La autoconciencia supone una obstrucción, porque es como interrumpir una canción después de cada nota para escuchar el eco y luego sentirse irritado porque se ha perdido el ritmo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SITUATIONS" :TEXT "Las situaciones extremas delhombre son las oportunidades de Dios." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Consideramos la vida como una rueda que gira colocada verticalmente,con la persona andando sobre su neumático. A medida que avanza, la rueda gira bajo sus pies hacia él, y si no quiere irse hacia atrás y caer al suelo, debe andar a la misma velocidad que gira la rueda. Si la sobrepasa, se caerá hacia delante y se dará de bruces en la rueda. Pues vivimos en todo momento como si estuviéramos sobre una rueda; en el instante en que intentamos aferramos a ese momento, a ese punto concreto de la rueda, ya no se halla en la parte superior y hemos perdido el equilibrio." :AUTHOR "Alan Watts" :ITEM "Conviértete en lo que eres" :DATE "")
 (:CATEGORY "MAN" :TEXT "En muchos aspectos, el hombre es la más implacablemente feroz de las bestias. Como ocurre con todos los animales gre garios, «en su pecho habitan dos almas», como dice Fausto, una hecha de sociabilidad y amabilidad, la otra de envidia y antagonismo hacia sus pares. Aunque de una forma general no puede vivir sin ellos, en lo que respecta a determinados individuos, a menudo ocurre que tampoco puede vivir con ellos." :AUTHOR "William James" :ITEM "" :DATE "")
 (:CATEGORY "STRENGTH" :TEXT "Todos tenemos fuerza suficiente como para soportar las desgracias de los demás." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ZAR" :TEXT "Un zar estaba siempre sumido en la tristeza y se dijo: “Daré la mitad de mi reino a quien me cure”. Entonces todos los sabios se reunieron y celebraron una junta para sanar al zar, pero no encontraron ningún remedio. Uno de ellos, sin embargo, declaró que sí era posible curarlo. Y dijo: “Si sobre la tierra se encuentra un hombre feliz, quitadle la camisa y que se la ponga el zar. Con esto estará curado”. El zar hizo buscar en su reino a un hombre feliz. Los enviados del soberano exploraron todo el país, pero no encontraron a un hombre feliz. No encontraron a nadie contento con su suerte. Uno era rico, pero estaba enfermo; otro gozaba de salud, pero era pobre; el que era rico y sano se quejaba de su mujer; otro, de sus hijos… A todos les faltaba algo… Una noche el hijo del zar al pasar por una pobre choza oyó que alguien exclamaba: “¡Gracias a Dios he trabajado y comido bien! ¿Qué me falta?”. El hijo del zar se sintió lleno de alegría. Inmediatamente mandó que le llevaran la camisa de aquel hombre a quien a cambio se le entregaría cuanto dinero pidiera. Los enviados se presentaron a toda prisa en la casa de aquel hombre para quitarle la camisa, pero el hombre feliz era tan pobre que no tenía ni camisa." :AUTHOR "Leon Tolstoi" :ITEM "Cuentos de primavera" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "Let us change our traditional attitude to the construction of programs: Instead of imagining that our main task is to instruct a computer what to do, let us concentrate rather on explaining to human beings what we want a computer to do. The practitioner of literate programming can be regarded as an essayist, whose main concern is with exposition and excellence of style. Such an author, with thesaurus in hand, chooses the names of variables carefully and explains what each variable means. He or she strives for a program that is comprehensible because its concepts have been introduced in an order that is best for human understanding, using a mixture of formal and informal methods that reinforce each other." :AUTHOR "Donald Knuth" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Pero, ¿Qué vamos a hacer?. Parece que hay dos alternativas. La primera consiste en descubrir, de un modo u otro, un nuevo mito, o resucitar uno antiguo de un modo convincente. Si la ciencia no puede demostrar que Dios no existe, podemos tratar de vivir y actuar como si, después de todo, existiera de verdad. No parece que haya nada que perder en este juego, pues si la muerte es el final, nunca sabremos que hemos perdido. Pero, evidentemente, esto jamás equivaldrá a una fe vital, pues es como si uno dijera: «Puesto que, de todos modos, la vida es fútil, finjamos que no lo es». La segunda consiste en tratar de enfrentarse sombríamente al hecho de que La vida «es un cuento contado por un idiota», y obtener de ella lo que podamos, dejando que la ciencia y la tecnología nos sirvan lo mejor que puedan en nuestra travesía de una nada a otra." :AUTHOR "Alan Watts" :ITEM "La sabiduría de la incertidumbre" :DATE "1951")
 (:CATEGORY "EMACS" :TEXT "I’m using Linux. A library that emacs uses to communicate with Intel hardware." :AUTHOR "Erwin" :ITEM "#emacs@Freenode" :DATE "")
 (:CATEGORY "MIND" :TEXT "Hay que ser abierto de mente, pero no tanto como para que se te caiga el cerebro." :AUTHOR "Richard Feynman" :ITEM "" :DATE "")
 (:CATEGORY "WAY" :TEXT "Nuestro camino no es por fáciles prados de hierba, sino que es un sendero de montaña escarpado y lleno de dificultades. pero siempre hacia adelante, hacia arriba, hacia el Sol." :AUTHOR "Ruth Westheimer" :ITEM "" :DATE "")
 (:CATEGORY "TEACH" :TEXT "Teach your children early what you learned late." :AUTHOR "Richard Feynman" :ITEM "" :DATE "")
 (:CATEGORY "PATIENCE" :TEXT "Si he hecho descubrimientos invaluables ha sido por tener paciencia más que cualquier otro talento." :AUTHOR "Isaac Newton" :ITEM "" :DATE "")
 (:CATEGORY "RESPECT" :TEXT "Busca respeto, no atención, créeme, dura más." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WORLD" :TEXT "Ríe y el mundo reirá contigo, llora y llorarás solo." :AUTHOR "" :ITEM "Oldboy" :DATE "2003")
 (:CATEGORY "DEATH" :TEXT "La muerte le pregunta a la vida: ¿Porqué a mi todos me odian y a ti todos te aman?. Porque yo soy una bella mentira y tú una triste realidad." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "BIKES" :TEXT "Un ciclista es un desastre para la economía del país:
No compra coches ni presta dinero para comprar.
No paga pólizas de seguro.
No compra combustible, no paga para llevar el coche a revisión, ni necesita reparaciones.
No usa aparcamiento pagado.
No causa accidentes graves.
No necesita grandes infraestructuras, ni autopistas con muchos carriles.
No padece de sobrepeso.
Las personas sanas no son necesarias ni útiles para la economía.
No compra medicinas.
No va a hospitales ni visita a médicos.
No aporta nada al PIB del país.
Por el contrario, cada nuevo restaurante de comida rápida crea al menos 30 empleos, en realidad 10 cardiólogos, 10 dentistas, 10
expertos en dieta y nutricionistas, obviamente, así como las personas que trabajan en la propia tienda.
Elige bien: ¿Una bicicleta o un restaurante de comida rápida? Vale la pena pensar en ello.
PD: caminar es aún peor ya que los peatones ni siquiera compran una bicicleta." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HEART" :TEXT "Si quieres saber dónde está tu corazón, mira dónde va tu mente cuando sueñas despierto." :AUTHOR "Walt Whitman" :ITEM "" :DATE "")
 (:CATEGORY "PLANS" :TEXT "En la batalla te das cuenta de que los planes son inservibles, pero hacer planes es indispensable." :AUTHOR "Dwight E. Eisenhower" :ITEM "" :DATE "")
 (:CATEGORY "HAPPINESS" :TEXT "Existen dos maneras de ser feliz en esta vida, una es hacerse el idiota y la otra serlo." :AUTHOR "Sigmund Freud" :ITEM "" :DATE "")
 (:CATEGORY "CONQUER" :TEXT "Always to seek to conquer myself rather than fortune, to change my desires rather than the established order, and generally to believe that nothing except our thoughts is wholly under our control, so that after we have done our best in external matters, what remains to be done is absolutely impossible, at least as far as we are concerned." :AUTHOR "René Descartes" :ITEM "" :DATE "")
 (:CATEGORY "HAPPINESS" :TEXT "Nunca serás feliz si continúas buscando en qué consiste la felicidad. Nunca vivirás si estás buscando el significado de la vida." :AUTHOR "Albert Camus" :ITEM "" :DATE "")
 (:CATEGORY "EXPERIENCE" :TEXT "El deseo de una experiencia más positiva es, en sí misma, una experiencia negativa. Y, paradójicamente, la aceptación de la experiencia negativa es, en sí misma, una experiencia positiva." :AUTHOR "Mark Manson" :ITEM "El sutil arte de que todo te importe una mierda" :DATE "")
 (:CATEGORY "FEELINGS" :TEXT "Si no te importa una mierda sentirte mal, el maldito círculo vicioso del sobreanálisis entra en cortocircuito; te dices a ti mismo: «Me siento fatal, pero me importa una mierda». Y entonces llega el hada madrina de las mierdas con su polvo mágico y dejas de odiarte por sentirte mal." :AUTHOR "Mark Manson" :ITEM "El sutil arte de que todo te importe una mierda" :DATE "")
 (:CATEGORY "CHOICES" :TEXT "Tengo dos opciones: Quedarme en la oficina postal y volverme loco… o quedarme fuera, jugar a ser escritor y morir de hambre. He decidido morirme de hambre." :AUTHOR "Charles Buckowski" :ITEM "" :DATE "")
 (:CATEGORY "DREAMS" :TEXT "What if you slept? And what if, in your sleep, you dreamed? And what if, in your dream, you went to heaven and there plucked an strange and beautiful flower? And what if, when you awoke, you had the flower in your hand? Ah, what then?." :AUTHOR "Samuel Taylor Coleridge" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Cuando a Idries Shah, el preeminente instructor sufí, se le pidió que nombrase «Un error fundamental del ser humano», contestó: «Pensar que está vivo, cuando meramente se ha quedado dormido en la sala de espera de la vida»." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MYTHS" :TEXT "Los mitos son sueños publicos. Los sueños son mitos privados." :AUTHOR "Joseph Campbell" :ITEM "El poder del mito" :DATE "")
 (:CATEGORY "WORRIES" :TEXT "Chew your food not your worries." :AUTHOR "" :ITEM "Buddisht philosophies" :DATE "")
 (:CATEGORY "HUMAN" :TEXT "One does not become fully human painlessly." :AUTHOR "Rollo May" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "We often live as if we’re in a dream. We’re dragged into the past or pulled into the future. We’re bound by our sorrow, agitation, and fear. We hold on to our anger, which blocks communication. «Liberation» means transforming and transcending these conditions in order to be fully awake, at ease, peaceful, joyful and fresh. We practice stopping and observing deeply in order to arrive at liberation. When we live in this way, our life is worth living, and we become a source of joy to our family and to everyone around us.”" :AUTHOR "Tarthang Tulku" :ITEM "" :DATE "")
 (:CATEGORY "MAN" :TEXT "A man is as wretched as he has convinced himself that he is." :AUTHOR "Seneca" :ITEM "" :DATE "")
 (:CATEGORY "PROGRESS" :TEXT "¿Qué progreso, me preguntas, he conseguido? He empezado a ser amigo de mí mismo." :AUTHOR "Seneca" :ITEM "" :DATE "")
 (:CATEGORY "MEN" :TEXT "Siempre ha sido así y siempre será igual, que el tiempo y el mundo, el dinero y el poder, pertenecen a los mediocres y superficiales, y a los otros, a los verdaderos hombres, no les pertenece nada. Nada más que la muerte." :AUTHOR "Herman Hesse" :ITEM "" :DATE "")
 (:CATEGORY "TREASURE" :TEXT "The cave you fear to enter holds the treasure you seek." :AUTHOR "Joseph Campbell" :ITEM "" :DATE "")
 (:CATEGORY "SINS" :TEXT "Los siete pecados sociales:
Política sin principios
Negocios sin moral
Bienestar sin trabajo
Educación sin carácter
Ciencia sin humanidad
Goce sin responsabilidad
Religión sin sacrificio." :AUTHOR "Mahatma Gandhi" :ITEM "" :DATE "")
 (:CATEGORY "MEDIA" :TEXT "La manipulación mediática hace más daño que la bomba atómica porque destruye los cerebros." :AUTHOR "Noam Chomsky" :ITEM "" :DATE "")
 (:CATEGORY "SHOUT" :TEXT "Un dia el maestro preguntó: ¿Por qué grita la gente cuando está enojada?. Uno de los monjes contestó: Porque perdemos la calma, por eso gritamos. Pero, ¿Por qué gritar cuando la otra persona está a tu lado?, volvió a preguntar el maestro. Nadie contestó esta vez… Y y el maestro dijo: Cuando dos personas están enojadas, sus corazones se alejan mucho. Para cubrir esa distancia deben gritar, para poder escucharse. Cuanto más enojados estén, más alejados estarán y más fuerte tendrán que gritar para escucharse el uno al otro. Sin embargo, cuando dos personas se enamoran, se hablan suavemente porque sus corazones están muy cerca. La distancia entre ellos es muy pequeña. Cuando discutan no dejen que sus corazones se alejen, no digan palabras que los distancien más, pues llegará un día en que la distancia sea tanta que no podrán encontrar el camino de regreso." :AUTHOR "Alejandro Jodorowsky" :ITEM "" :DATE "")
 (:CATEGORY "CIVILIZATION" :TEXT "Un estudiante le preguntó a la antropóloga Margaret Mead que cuál consideraba que fue la primera señal de civilización humana. El estudiante esperaba una respuesta tipo «lanzas para cazar» o «redes para pescar» o «vasijas para cocinar». Pero no, la Dra. Mead dijo que la primera señal de civilización fue el hallazgo de un fémur que había estado fracturado y después había sanado. Mead explicó que en el reino animal, si te rompes un hueso, estás muerto. No puedes correr del peligro, no puedes caminar al río para beber agua, ni puedes cazar para comer. Te conviertes en presa para un predador. Ningún animal es capaz de tener un hueso roto y sobrevivir lo suficiente para sanarlo. Un fémur que estuvo roto y fue sanado, significa que alguien se quedó un tiempo para atender a esa persona lastimada, limpió su herida, la llevó a un lugar seguro, le dio de comer y beber y estuvo a su lado hasta su recuperación. «Ayudar a alguien en momentos difíciles, o cuando lo necesita es cuando comienza la civilización», dijo la Dra. Mead. Somos civilizados y mostramos lo mejor de nuestra humanidad cuando servimos al prójimo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "STORM" :TEXT "Y una vez que la tormeta haya pasado, no recordarás cómo lograste salir de ella, cómo lograste sobrevivir, ni siquiera estáras seguro de si la tormeta ha pasado o no. Pero una cosa es cierta, una vez que hayas salido de la tormenta, no serás la misma persona que entró en ella. De eso trata la tormænta." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "IRON" :TEXT "No one can destory iron, but its own rust can. Likewisel, no one can destroy a person, but his own mindset can." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "THINK" :TEXT "Cuando todos piensan igual, es que ninguno está pensando." :AUTHOR "Walter Lippman" :ITEM "" :DATE "")
 (:CATEGORY "CONSUMISM" :TEXT "En la era del consumo, reparar es un acto de rebeldía." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MEDIOCRITY" :TEXT "La mediocridad, posiblemente, consiste en estar cerca de la grandeza y no darse cuenta de ello." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TREE" :TEXT "Se dice que ningún árbol puede crecer hasta el cielo a menos que sus raíces lleguen al infierno." :AUTHOR "Carl Gustav Jung" :ITEM "" :DATE "")
 (:CATEGORY "ELECTIONS" :TEXT "Los malos gobernantes son elegidos por los buenos ciudadanos que no votan." :AUTHOR "George Jean Nathan" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Me avergoncé de mi mismo cuando me di cuenta de que la vida es una fiesta de disfraces; y yo asistía con mi rostro real." :AUTHOR "Frank Kafka" :ITEM "" :DATE "")
 (:CATEGORY "SOCIETY" :TEXT "Una sociedad no se define por la riqueza que tiene sino por la pobreza que no tiene." :AUTHOR "Jorge Majfud" :ITEM "" :DATE "")
 (:CATEGORY "BELIEFS" :TEXT "What if I told you your beliefs don't make you a free thinker. The ability to change your beliefs based on nes information does." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SALVATION" :TEXT "Nadie puede salvarte sino tú mismo, y mereces salvarte. No es una guerra fácil de ganar, pero si algo merece la pena ganar, es esto. Piénsalo, piensa en salvarte a ti mismo. Tu parte espiritual, la parte de tus entrañas, tu parte mágica y ebria. Sálvala, no te unas a los muertos de espíritu." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WELLNESS" :TEXT "Many of us have been trained to believe that disease is contagious, yet what if we learn to believe that wellness is even more contagious?." :AUTHOR "Judy Tsuei" :ITEM "The little book of tibetan rites and rituals" :DATE "")
 (:CATEGORY "IDENTITY" :TEXT "I am not who you think I am; I am not who I think I am; I am who I think you think I am." :AUTHOR "Charles Horton Cooley" :ITEM "" :DATE "")
 (:CATEGORY "UNIVERSE" :TEXT "Han sido tiempos gloriosos para estar vivo y hacer investigaciones en torno a la fisica teórica. Nuestra imagen del universo ha cambiado mucho en los últimos 50 años, y me alegra saber que he podido hacer una pequeña contribución. El hecho de que los humanos que no somos más que simples conjuntos de partículas fundamentales de la naturaleza nos hayamos acercado tanto a la comprensión de las leyes que nos rigen a nosotros y a nuestro universo es un triunfo. Quiero compartirte mi emoción y entusiasmo sobre esta búsqueda. Así que recuerda voltear hacia arriba para ver las estrellas, en vez de hacia abajo para mirarte los pies. Trata de encontrarle el sentido a lo que ves, y de preguntarte qué hace que exista el universo. Ten curiosidad. Y por más dificil que parezca la vida, siempre hay algo que puedes hacer y en lo que puedes triunfar. Lo importante es que no te rindas." :AUTHOR "Stephen Hawking" :ITEM "Últimas palabras" :DATE "2018")
 (:CATEGORY "PERCEPTION" :TEXT "Una niña tenía dos manzanas en su mano. Su mamá se le acercó y le preguntó a su hija si le daría una manzana. La niña rápidamente mordió una y luego la otra. La mamá sintió como que se le congeló la sonrisa y trató de no mostrar su decepción. Pero la niña le pasa una de las manzanas y le dice: «Toma mamita, ésta es la más dulce». No importa cuánta experiencia o conocimiento crees que tienes, nunca hagas juicios. Ofrécele al otro la oportunidad de dar una explicación. Lo que percibes puede no ser la realidad." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "FEAR" :TEXT "I must not fear. Fear is the mind-killer. Fear is the little-death that brings total obliteration. I will face my fear. I will permit it to pass over me and through me. And when it has gone past I will turn the inner eye to see its path. Where the fear has gone there will be nothing. Only I will remain." :AUTHOR "" :ITEM "The ultimate quote from the Kwisatz Haderach" :DATE "")
 (:CATEGORY "ENFORCE" :TEXT "Este esfuerzo por conseguir que cada cual apruebe aquello que uno ama u odia es, en realidad, ambición; y así vemos que cada cual apetece, por naturaleza, que los demás vivan según la índole propia de él. Pero como todos lo apetecen a la vez, a la vez se estorban unos a otros, y como todos quieren ser alabados y amados por todos, se tienen odio unos a otros." :AUTHOR "Baruch Spinoza" :ITEM "Ética (III, Pr. XXXI, Esc.)" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Lo mejor de la vida es derrotar a tus enemigos y perseguirles, despojarles de sus riquezas, ver llorar a sus seres queridos, cabalgar sus monturas y pegar tu vientre a sus hijas y sus esposas." :AUTHOR "Genghis Khan" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Gracias a la vida que me ha dado tanto. Me ha dado el sonido y el abecedario. Con él las palabras que pienso y declaro. Madre, amigo, hermano, y luz alumbrando. La ruta del alma del que estoy amando." :AUTHOR "Violeta Parra" :ITEM "Gracias a al vida" :DATE "")
 (:CATEGORY "COMPUTER SCIENCE" :TEXT "Like mathematics, computer science will be somewhat different from the other sciences, in that it deals with man-made laws which can be proved, instead of natural laws which are never known with certainty. Thus, the two subjects will be like each other in many ways. The difference is in the subject matter and approach- mathematics dealing more or less with theorems, infinite processes, static relationships, and computer science dealing more or less with algorithms, finitary constructions, dynamic relationships." :AUTHOR "Donald Knuth" :ITEM "" :DATE "")
 (:CATEGORY "CONSCIENCE" :TEXT "La conciencia solo puede existir de una manera, y es teniendo conciencia de que existe." :AUTHOR "Jean Paul Sartre" :ITEM "" :DATE "")
 (:CATEGORY "QUESTION" :TEXT "Cuando las cosas se vuelven demasiado complicadas, a veces tiene sentido parar y preguntarse: ¿He pl
anteado la pregunta correcta?." :AUTHOR "Enrico Bombieri" :ITEM "Prime Territory, en The Sciences" :DATE "")
 (:CATEGORY "BUDDISTS" :TEXT "Cuando un ágil Burman me puso la zancadilla en el campo de fútbol, y el árbitro (otro Burman) miró para otro lado, el público lanzó una espantosa carcajada [...] Los peores eran los jóvenes monjes budistas." :AUTHOR "George Orwell" :ITEM "" :DATE "")
 (:CATEGORY "WAR" :TEXT "El objeto de la guerra no es morir por tu país sino conseguir que el tipo del bando contrario muera por el suyo." :AUTHOR "George S. Patton" :ITEM "" :DATE "")
 (:CATEGORY "FAILURE" :TEXT "No basta con que tengamos éxito. Además, los gatos tienen que fracasar." :AUTHOR "" :ITEM "Un perro, en una viñeta de la revista The New Yorker" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Cuando estaba enamorado, había mariposas por todas partes, la voluptuosidad de la pa sión me carcomía la cabeza. Durante todo ese tiempo no escribí, no trabajé, no me encontré con los amigos. Vivía pendiente de los movimientos o de la quietud de mi amada: consumía montañas de cigarrillos y toneladas de vitaminas, me afeitaba dos y hasta tres veces por día: hacia dietas, caminatas. Me perseguía hasta la certeza la paranoia del engaño, pensaba todo el tiempo en besarla, en mirarla, en acariciarla. Durante semanas gaste demasiado dinero, demasiada esperanza, demasiada crema para el sol, demasiada esperma y demasiado perfume. Escuchaba demasiada música clásica, utilizaba demasiado tiempo, consumí toda mi tole rancia y agote hasta la última de mis lágrimas. Por eso siempre digo recordando esos momentos: Nunca he sufrido tanto como cuando era feliz." :AUTHOR "Juan Carlos Benitez" :ITEM "" :DATE "")
 (:CATEGORY "DYSTOPIA" :TEXT "You may have heard of the great reset or the world government. Is it a controlled demolition of the global markets, economies and the world as we know it?. A shift into a new dystopian future where the elites are the masters of the slaves without the cosmetics of democracy?." :AUTHOR "KimDotCom" :ITEM "https://mobile.twitter.com/KimDotcom/status/1525294670846504960" :DATE "")
 (:CATEGORY "PROGRAMMING" :TEXT "// Querido programador:
// Cuando escribí este código, sólo Dios y yo
// sabíamos cómo funcionaba.
// Ahora, ¡sólo Dios lo sabe!
// Así que si está tratando de 'optimizar'
// esta rutina y fracasa (seguramente),
// por favor, incremente el siguiente contador
// como una advertencia
// para el siguiente colega:
// total_horas_perdidas_aquí = 189" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "TEACHER" :TEXT "Estimado profesor: Él tiene que aprender que no todos. los hombres son justos, no todos son verdaderos, pero por favor digale que para cada villano hay un héroe, que para cada egoísta, también hay un líder dedicado.
Enséñele que para cada enemigo, ahí también habrá un amigo. Enséñele que es mejor obtener una moneda ganada con el sudor con el sudor de su frente de una moneda robada. Enséñele a perder, pero también aprender a disfrutar de la victoria, háblele de la envidia y sáquelo de ella, déle a conocer la profunda alegría de la sonrisa silenciosa, y a maravillarse con los libros, pero deje que él también aprenda con el cielo, las flores del campo, las montañas y valles.
En la broma con amigos, expliquele que más vale una derrota honrosa que una victoria vergonzosa. Enséñele a creer en sí mismo, incluso si está sólo frente a todo el mundo. Enséñele a ser suave con los gentiles y ser duro con los duros, enseñele a no entrar en un tren, sólo porque otros entraron. Enséñele a escuchar a todos, pero a la hora de la verdad, decidir sólo, enseñele a reír cuando esté triste y explíquele que a veces los hombres también lloran.
Trátelo bien pero no lo mime, ya que sólo en la prueba de fuego se sabe que el acero es real. Déjelo tener el coraje de ser impaciente y a tener coraje con paciencia.
Transmitale una fe sublime en el creador y fe también en sí mismo, porque sólo entonces podrá tener fe en los hombres. Sé que pido mucho, pero vea lo que puede hacer, querido profesor." :AUTHOR "Abraham Lincoln" :ITEM "Carta de Abraham Lincoln al profesor de su hijo" :DATE "1830")
 (:CATEGORY "RELIGION" :TEXT "When religion and politics travel in the same cart, the riders believe nothing can stand in their way. Their movements become headlong - faster and faster and faster. They put aside all thoughts of obstacles and forget the precipice does not show itself to the man in a blind rush until it's too late." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SCIENCE" :TEXT "La ciencia es más que un cuerpo de conocimiento; es una forma de pensar. Tengo el presentimiento de una América en la época de mis hijos o nietos, cuando Estados Unidos sea una economía de servicios e información; cuando casi todas las industrias manufactureras clave se han ido a otros países; cuando los asombrosos poderes tecnológicos están en manos de unos pocos, y nadie que represente el interés público puede siquiera comprender los problemas; cuando las personas han perdido la capacidad de establecer sus propias agendas o cuestionar con conocimiento a quienes tienen autoridad; cuando, agarrados a nuestros cristales y consultando nerviosamente nuestros horóscopos, nuestras facultades críticas en declive, incapaces de distinguir entre lo que se siente bien y lo que es verdad, nos deslizamos, casi sin darnos cuenta, de regreso a la superstición y la oscuridad." :AUTHOR "Carl Sagan" :ITEM "The Demon-Haunted World: Science as a Candle in the Dark" :DATE "1995")
 (:CATEGORY "TEACHING" :TEXT "Antes de morir, hija mía, quisiera estar seguro de haberte enseñado... a disfrutar del amor, a enfrentar tus miedos y confiar en tu fuerza, a entusiasmarte con la vida, a pedir ayuda cuando la necesites, a decir o callar, según tu conveniencia, a ser amiga de ti misma, a no tenerle miedo al ridículo, a darte cuenta de lo mucho que mereces ser querida, a tomar tus propias decisiones, a quedarte con el crédito por tus logros, a superar la adicción a la aprobación de los demás, a no hacerte cargo de las responsabilidades de todos, a ser consciente de tus sentimientos y actuar en consecuencia, a dar porque quieres, y nunca porque estés obligada a hacerlo. Antes de morir, hija mía, quisiera estar seguro de haberte enseñado... a exigir que se te pague adecuadamente por tu trabajo, a aceptar tus limitaciones y vulnerabilidades sin enojo, a no imponer tu criterio, ni permitir que te impongan el de otros, a decir que sí solo cuando quieras y decir que no sin culpa, a tomar más riesgos, a aceptar el cambio y revisar tus creencias, a tratar, y exigir ser tratada, con respeto, a llenar primero tu copa y, después, la de los demás, a planear para el futuro sin intentar vivir en función de él. Antes de morir, hija mía, quisiera estar seguro de haberte enseñado... a valorar tu intuición, a celebrar las diferencias entre los sexos, a hacer de la comprensión y el perdón tus prioridades, a aceptarte así como eres, a crecer aprendiendo de los desencuentros y de los fracasos, a no avergonzarte de andar riendo a carcajadas por la calle sin ninguna razón, a darte todos los permisos sin otra restricción que la de no dañar a otros ni a ti misma. Pero sobre todo, hija mía, porque te amo más que a nadie, quisiera estar seguro de haberte enseñado... a no idolatrar a nadie... y a mí, que soy tu padre, menos que a nadie." :AUTHOR "Jorge Bucay" :ITEM "" :DATE "")
 (:CATEGORY "DEPRESSION" :TEXT "Un hombre entra en la consulta de su psiquiatra. Le dice: «Doctor, estoy deprimido. La vida es cruel y me siento solo en un mundo amenazador». El médico le responde que no se preocupe, porque su solución es sencilla: «Vaya a ver al gran payaso Pagliacci, que está estos días en la ciudad. Ya verá como él le anima». Sin embargo, el paciente rompe a llorar: «Pero doctor... yo soy Pagliacci»." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "La magia del amor es que quien te ama sabe en qué lugar debe pegar para herirte de muerte y nunca lo hará." :AUTHOR "Joseph Zinker" :ITEM "" :DATE "")
 (:CATEGORY "GOD" :TEXT "Confía mucho en Dios… pero ata tú mismo tu camello." :AUTHOR "Proverbio sufí" :ITEM "" :DATE "")
 (:CATEGORY "WANT" :TEXT "No te lo vas a creer, una mujer explosiva me ha dicho que si la acompañaba a casa me haría lo que yo quisiera. He tenido que declinar la oferta. ¿Y si no sé lo que quiero?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "ADDICTIONS" :TEXT "Persons with severe addictions are among those contemporary prophets that we ignore to our own demise, for they show us who we truly are." :AUTHOR "Kent Dunnington" :ITEM "" :DATE "")
 (:CATEGORY "INTUITION" :TEXT "Hay cosas que solo la inteligencia es capaz de buscar, pero que, por sí misma, no encontratará nunca. Solo el instinto las encontraría, pero jamás las buscará. De ahí se deduce que la verdadera facultad cognoscitiva no reside ni en el instinto ni en la inteligencia. ¿Dónde reside, pues? En la fusión de ambos, esto es, en la intuición. Mediante la intuición es posible trascender el terreno conceptual en el que se fundamentan las leyes de las relaciones entre las cosas y captar la verdadera naturaleza de la realidad." :AUTHOR "Henri Bergson" :ITEM "" :DATE "")
 (:CATEGORY "WARRIOR" :TEXT "Más vale ser un guerrero en un jardín que un jardinero en la guerra." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MEDIOCRICITY" :TEXT "Cada acierto nos trae un enemigo. Para ser popular hay que ser mediocre." :AUTHOR "Oscar Wilde" :ITEM "El retrato de Dorian Gray" :DATE "")
 (:CATEGORY "HATED" :TEXT "I'd rather be hated for who I am, than be loved for who I am not." :AUTHOR "Kurt Cobain" :ITEM "" :DATE "")
 (:CATEGORY "STUPIDITY" :TEXT "There is more stupidity than hydrogen in the Universe." :AUTHOR "Frank Zappa" :ITEM "" :DATE "")
 (:CATEGORY "FREEDOM" :TEXT "No one is free, even the birds are chained to the sky" :AUTHOR "Bod Dylan" :ITEM "" :DATE "")
 (:CATEGORY "MUSIC" :TEXT "When I die I want people to play my music, go wild and freak out and do anything they want to do." :AUTHOR "Jimi Hendrix" :ITEM "" :DATE "")
 (:CATEGORY "FEAR" :TEXT "Expose yourself to your deepest fear; after that, fear has no power. You are free." :AUTHOR "Jim Morrison" :ITEM "" :DATE "")
 (:CATEGORY "BELIEVE" :TEXT "I may not beleive in myself, but I beleive in what I'm doing." :AUTHOR "Jimmy Page" :ITEM "" :DATE "")
 (:CATEGORY "TIME" :TEXT "Time you enjoyed wasting was not wasted." :AUTHOR "John Lennon" :ITEM "" :DATE "")
 (:CATEGORY "WORK" :TEXT "Joy's law: «No matter who you are, most of the smartest people work for someone else [other than you]»." :AUTHOR "Chris Anderson" :ITEM "" :DATE "2013")
 (:CATEGORY "HUMAN" :TEXT "Respice post te! Hominem te esse memento" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CAPITALISM" :TEXT "La gran astucia del capitalismo consiste en canalizar las fuerzas destructivas y la pulsión de muerte y reconducirlas hacia el crecimiento." :AUTHOR "Bernard Maris" :ITEM "" :DATE "")
 (:CATEGORY "WORLD" :TEXT "El mundo se convierte en una suma de artefactos sin vida; del alimento sintético a los órganos sintéticos, el hombre entero se convierte en parte del mecanismo total que él controla y que simultáneamente lo controla a él. … Aspira a fabricar robots, que serán una de las mayores hazañas de su mente técnica, y algunos especialistas nos afirman que el robot apenas se distinguirá de los hombres vivientes. Esto no será una hazaña tan asombrosa, ahora que el hombre es difícil de distinguir de un robot. El mundo de la vida se ha convertido en mundo de «no vida»; las personas son ya «no personas», un mundo de muerte. La muerte ya no se expresa simbólicamente por heces ni cadáveres malolientes. Sus símbolos son ahora máquinas limpias y brillantes." :AUTHOR "Erich Fromm" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "La risa se está convirtiendo en un nuevo lujo, y aún peor: puesto que la función hace al órgano, Se nos está atrofiando esa parte del cerebro que se activa mediante la estimulación que ofrece el sentido del humor." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "HORIZON" :TEXT "Por fin el horizonte se nos muestra libre otra vez, aunque desde luego no esté claro; por fin nuestros barcos puedan haber aventurarse a salir otra vez, aventurarse a afrontar cualquier peligro. Todo el atrevimiento del amante del conocimiento se permite otra vez. El mar, nuestro mar se extiende abierto otra vez. Quizás nunca haya habido un «mar abierto» como éste." :AUTHOR "Friedrich Nietzsche" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "Ríe y el mundo reirá contigo; llora y llorarás solo." :AUTHOR "Ella Wheeler Wilcox" :ITEM "Soledad" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Nadie te puede construir el puente por el que tú, y solo tú, debes cruzar el río de la vida." :AUTHOR "Friedich Nietzsche" :ITEM "" :DATE "")
 (:CATEGORY "BE" :TEXT "Quién no ha, no es." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PAST" :TEXT "Lo pasado, pisado." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MONEY" :TEXT "El dinero no es la riqueza, la riqueza es saber usar el dinero." :AUTHOR "Séneca" :ITEM "" :DATE "")
 (:CATEGORY "FISHES" :TEXT "Había una vez dos peces jóvenes que iban nadando y se encontraron por casualidad con un pez más viejo que nadaba en dirección contraria; el pez más viejo los saludó con la cabeza y les dijo: «Buenos días, chicos. ¿Cómo está el agua?». Los dos peces jóvenes siguieron nadando un trecho; por fin uno de ellos miró al otro y le dijo: «¿Qué demonios es el agua?»." :AUTHOR "David Foster wallace" :ITEM "Chara a sus estudiantes" :DATE "20050521")
 (:CATEGORY "ITACA" :TEXT "Cuando emprendas tu viaje a Ítaca
pide que el camino sea largo,
lleno de aventuras, lleno de experiencias.
No temas a los lestrigones ni a los cíclopes
ni al colérico Poseidón,
seres tales jamás hallarás en tu camino,
si tu pensar es elevado, si selecta
es la emoción que toca tu espíritu y tu cuerpo.
Ni a los lestrigones ni a los cíclopes
ni al salvaje Poseidón encontrarás,
si no los llevas dentro de tu alma,
si no los yergue tu alma ante ti.
Pide que el camino sea largo.
Que muchas sean las mañanas de verano
en que llegues -¡con qué placer y alegría!-
a puertos nunca vistos antes.
Detente en los emporios de Fenicia
y hazte con hermosas mercancías,
nácar y coral, ámbar y ébano
y toda suerte de perfumes sensuales,
cuantos más abundantes perfumes sensuales puedas.
Ve a muchas ciudades egipcias
a aprender, a aprender de sus sabios.
Ten siempre a Ítaca en tu mente.
Llegar allí es tu destino.
Mas no apresures nunca el viaje.
Mejor que dure muchos años
y atracar, viejo ya, en la isla,
enriquecido de cuanto ganaste en el camino
sin aguardar a que Ítaca te enriquezca.
Ítaca te brindó tan hermoso viaje.
Sin ella no habrías emprendido el camino.
Pero no tiene ya nada que darte.
Aunque la halles pobre, Ítaca no te ha engañado.
Así, sabio como te has vuelto, con tanta experiencia,
entenderás ya qué significan las Ítacas." :AUTHOR "Konstantino Kavafis" :ITEM "Ítaca" :DATE "")
 (:CATEGORY "METAVERSE" :TEXT "La otra chica era una brandy y su acompañante un Clint. Brandy y Clint son dos modelos prediseñandos muy vendidos. Cuando las adolescentas blancas sin demasiados recursos económicos tienen una cita en el metaverso, van invariablemente a la sección de juegos de ordenador de Wal-Mart más cercano y se compran el modelo Brandy. Se puede escoger entre tres tamaños de pecho: improbable, imposible y ridículo. El repertorio de expresiones faciales de Brandy es bastante limitado: mona con pucheros, mona y voluptuosa, vivaracha e interesada, sonriente y receptiva, o mona y colgada. Las pestañas le miden más de un centímetro, y el software es tan basto que parecen astillas de ébano macizo. Cuando una Brandy agita las pestañas, casi se puede sentir el aire que desplazan." :AUTHOR "Neil Stephanson" :ITEM "Snow flake" :DATE "")
 (:CATEGORY "El ser humano no tiene paciencia para enriquecerse lentamente, por eso decide arruinarse rápidamente." :TEXT "" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DEVIL" :TEXT "La mejor jugada del diablo fue convencer al mundo de que no existía." :AUTHOR "Verbal Kint" :ITEM "Sospechosos habituales" :DATE "")
 (:CATEGORY "UNIVERSITY" :TEXT "Han vuelto a pedirle una millonada al decano de la facultad de Físicas para hacer un experimento.
- ¡Otra vez ! Pero bueno, ¿Por qué no podéis ser como los matemáticos, que se apañan solo con papel, lápiz y una papelera?. ¿O como los filósofos, que sólo necesitan papel y lápiz?." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "PEOPLE" :TEXT "En mi relación con las personas he aprendido que, en definitiva, no me resulta beneficioso comportarme como si yo fuera distinto de lo que soy: mostrarme tranquilo y satisfecho cuando en realidad estoy enojado y descontento; aparentar que conozco las respuestas cuando en verdad las ignoro; ser cariñoso mientras me siento hostil; manifestarme aplomado cuando en realidad siento temor e inseguridad.
Soy más eficaz cuando puedo escucharme con tolerancia y ser yo mismo.
He descubierto el enorme valor de permitirme comprender a otra persona.
He descubierto que abrir canales por medio de los cuales los demás puedan comunicar sus sentimientos, su mundo perceptual privado, me enriquece.
Me ha gratificado en gran medida el hecho de poder aceptar a otra persona.
Puedo confiar en mi experiencia." :AUTHOR "Carl Rogers" :ITEM "" :DATE "")
 (:CATEGORY "HABITS" :TEXT "El hábito, si no se resiste, al poco tiempo se vuelve una necesidad." :AUTHOR "San Agustín de Hipona" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Esperar que la vida te trate bien porque eres buena gente es como prentender que un tigre no te coma porque eres vegatariano." :AUTHOR "Bruce Lee" :ITEM "" :DATE "")
 (:CATEGORY "WORLD" :TEXT "El mundo te preguntará quién eres,y, si no lo sabes, te lo dirá." :AUTHOR "Carl Gustav Jung" :ITEM "" :DATE "")
 (:CATEGORY "FRACTAL" :TEXT "Dark corners are basically fractal—no matter how much you illuminate, there’s always a smaller but darker one." :AUTHOR "Brian Kernighan" :ITEM "" :DATE "")
 (:CATEGORY "FOOD" :TEXT "Comer lo que tiene una pata (setas y vegetales) es mejor que lo que tiene dos (aves) que es mejor que comer lo que tiene cuatro (ternera, cerdo y otros mamíferos)." :AUTHOR "Proverbio chino" :ITEM "" :DATE "")
 (:CATEGORY "MEN" :TEXT "Los hombres solo quieren que les coman el rabo. Tantas horas al día como sea posible. Tantas chicas bonitas como sea posible. Fuera de eso, les interesan las cuestiones técnicas. ¿Ha quedado lo bastante claro?." :AUTHOR "Michael Houellebecq" :ITEM "" :DATE "")
 (:CATEGORY "HEART" :TEXT "Nada pesa tanto como un corazón lastimado que debe seguir caminando." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SUCCESS" :TEXT "El éxito de un minuto es el resultado del fracaso de años." :AUTHOR "Robert Browning" :ITEM "" :DATE "")
 (:CATEGORY "SUCCESS" :TEXT "El éxito se consigue fracasando en el fracaso." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "WISDOM" :TEXT "Libros, caminos y días dan al hombre sabiduría." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Paso corto, mirada larga, diente de lobo y cara de bobo" :AUTHOR "Un maqui superviviente de la Guerra Civil Española" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "No te detengas
No dejes que termine el día sin haber crecido un poco, sin haber sido feliz, sin haber aumentado tus sueños. No te dejes vencer por el desaliento. No permitas que nadie te quite el derecho a expresarte, que es casi un deber. No abandones las ansias de hacer de tu vida algo extraordinario. No dejes de creer que las palabras y las poesías sí pueden cambiar el mundo. Pase lo que pase nuestra esencia está intacta. Somos seres llenos de pasión. La vida es desierto y oasis. Nos derriba, nos lastima, nos enseña, nos convierte en protagonistas de nuestra propia historia. Aunque el viento sople en contra, la poderosa obra continúa: Tú puedes aportar una estrofa. No dejes nunca de soñar, porque en sueños es libre el hombre." :AUTHOR "Walt Whitman" :ITEM "" :DATE "")
 (:CATEGORY "THOUGHTS" :TEXT "Si se diera cuenta de lo poderosos que son sus pensamientos, nunca tendría un pensamiento negativo." :AUTHOR "Peace Pilgrim" :ITEM "" :DATE "")
 (:CATEGORY "ANGRYNESS" :TEXT "No me enojo por las cosas que puedo controlar, porque si puedo controlarlas entonces no tiene sentido enojarse. Y no me enojo por las cosas que no puedo controlar, porque si no puedo controlarlas, entonces no tiene sentido enojarse" :AUTHOR "Mickey Rivers" :ITEM "" :DATE "")
 (:CATEGORY "CONSUMER" :TEXT "Cómo sabe Target lo que quieres antes que tú. Trabajar en Target le ofreció a Pole la oportunidad para estudiar a la más compleja de las criaturas: el comprador estadounidense en su hábitat natural." :AUTHOR "Charles Duhigg" :ITEM "El poder de los hábitos" :DATE "")
 (:CATEGORY "COMPANIES" :TEXT "Constituidos por feudos donde los ejecutivos compiten por el poder y los méritos, normalmente en escaramuzas ocultas que hacen que sus propias actuaciones parezcan superiores y reduzcan las de sus rivales. Las divisiones compiten por los recursos y se sabotean entre ellas para robarse la gloria. Los jefes crean divisiones entre sus subordinados para que no puedan rebelarse contra ellos." :AUTHOR "Charles Duhigg" :ITEM "El poder de los hábitos" :DATE "")
 (:CATEGORY "MARKETING" :TEXT "Si mañana los medios de comunicación masiva iniciaran una campaña para promover el consumo de mierda (excrementos, heces, etc.), la sociedad empezaría a comer mierda. Primero, se hablaría sobre un gran descubrimiento científico (la ciencia usada como la nueva religión del XXI) y lo errados que hemos estados durante miles de años por desechar nuestra mierda. Después, mostrarían el consejo de expertos sobre los valores nutricionales de la mierda. Y, finalmente, el proceso de normalización terminaría al plasmar con naturalidad el consumo de mierda por parte de gente famosa y gente corriente en la calle, en películas y otros encuentros sociales." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "CAPITALISM" :TEXT "El público se sorprendió. Estas reuniones suelen seguir siempre el mismo guión: el nuevo gerente empieza con una introducción, bromea sobre su persona en un acto de falsa modestia — me pasé el tiempo durmiendo cuando estaba en la Facultad de Ciencias Empresariales de Harvard, por ejemplo, luego promete incrementar los beneficios y abaratar los costes. Después toca vilipendiar los impuestos, las normativas empresariales, y a veces, con una exaltación propia de una experiencia directaen un tribunal de divorcios, a los abogados. Por último, la charla termina con un aluvión de palabras de moda «sinergia», «dimensionamiento idóneo» y «competencia cooperativa» — tras lo cual todo el mundo puede regresar a su trabajo con la garantía de que el capitalismo está a salvo un día más" :AUTHOR "Charles Duhigg" :ITEM "El Poder de los hábitos" :DATE "")
 (:CATEGORY "CONTROL" :TEXT "Someone pointed out that our inability to control the outcome of events is a blessing in disguise, because if we were able to determine the exact outcome, then the options would be limited to only those scenarios that we could think up. That way, things might turn out to be exceedingly predictable." :AUTHOR "Edward Viljoen" :ITEM "The power of meditation" :DATE "")
 (:CATEGORY "LEAVE" :TEXT "What would be your reaction and mine if we knew the only thing we could take with us when we leave this world would be that which we really are? Would not our reaction be more kind and gentle? Would not our very possessions seem of less value?." :AUTHOR "Ernest Holmes" :ITEM "" :DATE "")
 (:CATEGORY "THINK" :TEXT "Have you ever stopped to think, and then forgot to start again?” I have. I have stepped out of being present." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Vive tu vida como si subieras una montaña. De vez en cuando mira la cumbre, pero más importante es admirar las cosas bellas del camino. Sube despacio, firme, y disfruta cada momento. Las vistas desde la cima serán el regalo perfecto tras el viaje." :AUTHOR "Harold V. Melchert" :ITEM "" :DATE "")
 (:CATEGORY "MARRIAGE" :TEXT "Es una tontería mirar debajo de la cama. Si tu mujer tiene una visita, lo más probable es que la esconda en el armario. Conozco a un hombre que se encontró con tanta gente en el armario que tuvo que divorciarse únicamente para conseguir dónde colgar la ropa." :AUTHOR "Groucho Marx" :ITEM "" :DATE "")
 (:CATEGORY "DEATH" :TEXT "Death is not the ultimate tragedy of life. The ultimate tragedy is depersonalization, dying in an alien and sterile area, separated from the spiritual nourishment that comes from being able to reach out to a loving hand, separated from a desire to experience the things that make life worth living, separated from hope." :AUTHOR "Norman Cousins" :ITEM "Anatomy of an illness" :DATE "")
 (:CATEGORY "DEATH" :TEXT "No one gets out of this world alive. It isn’t dying we should fear, but rather, what dies inside us while we live." :AUTHOR "Norman Cousins" :ITEM "Anatomy of an illness" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Si un hombre no ha descubierto algunas cosas por las que morir, no está preparado para vivir." :AUTHOR "Martin Luther King" :ITEM "" :DATE "")
 (:CATEGORY "SHIT" :TEXT "Triscaba en el prado una vaca que al tiempo que vio como se acercaba un zorro vio caer del nido a un pajarito que no sabia volar. Maternal, la vaca defecó encima del pajarito para ocultarlo a la vista del zorro pero el pajarito empezó a decir pío, pío, pío y el zorro, alertado, escarbó entre la caca de la vaca, cogió al pajarito entre sus dientes, lo llevo al río, la lavó y cuando estuvo limpio se lo zampó en un plís plás y siguió su camino. La fábula tiene moraleja: no todos los que te cubren de caca son tus enemigos, no todos los que te la limpian son tus amigos y mientras estés cubierto de mierda nunca digas ni pío." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SIMPLE" :TEXT "Todo debe hacerse lo más simple posible, pero no más simple." :AUTHOR "Albert Einstein" :ITEM "" :DATE "")
 (:CATEGORY "FOOD" :TEXT "Hacer que la misma agencia que es responsable de la agroindustria también les diga a los estadounidenses lo que deben comer es como hacer que el zorro vigile el gallinero." :AUTHOR "David Raubenheimer y Stephen J. Simpson" :ITEM "Come como los animales" :DATE "")
 (:CATEGORY "INTUITION" :TEXT "Hay cosas que solo la inteligencia es capaz de buscar, pero que, por sí misma, no encontratará nunca. Solo el instinto las encontraría, pero jamás las buscará. La verdadera facultad cognoscitiva no reside ni en el instinto ni en la inteligencia. ¿Dónde reside, pues?. En la fusión de ambos, esto es, en la intuición. Mediante la intuición es posible trascender el terreno conceptual en el que se fundamentan las leyes de las relaciones entre las cosas y captar la verdadera naturaleza de la realidad." :AUTHOR "Henry Bergson" :ITEM "" :DATE "")
 (:CATEGORY "SERVICE" :TEXT "No sirvas a quién sirvió ni pidas a quién pidió." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "MONEY" :TEXT "El dinero es el único vehículo que encontró el ser humano para llegar a sus ruinas." :AUTHOR "Magdalena S. Blesa" :ITEM " Instrucciones a mis hijos" :DATE "")
 (:CATEGORY "CLIMBING" :TEXT "Hay dos tipos de escaladores, los escaladores inteligentes y los escaladores muertos." :AUTHOR "Don Whillans" :ITEM "" :DATE "")
 (:CATEGORY "SKYDIVE" :TEXT "Hay dos tipos de paracaidístas, los que han tenido una emergencia y los que la van a tener" :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SKYDIVE" :TEXT "En el paracaidismo, si ves a la gente como hormigas todo va bien, si ves a las hormigas como gente algo va mal." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "SKYDIVE" :TEXT "En el paracaidismo, más vales estar abajo queriendo estar arriba que estar arriba queriendo estar abajo." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Poco plato, mucho trato y mucho zapato." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DESERT" :TEXT "Siempre es duro ver el propósito de vagar por el desierto hasta que se ha terminado." :AUTHOR "John Bunyan" :ITEM "" :DATE "")
 (:CATEGORY "SUCCESS" :TEXT "El maestro nunca aspira a lo grande. De este modo alcanza la grandeza." :AUTHOR "Lao Tse" :ITEM "Tao Te Ching" :DATE "")
 (:CATEGORY "NATURE" :TEXT "No olvides que la tierra se deleita en sentir tus pies descalzos y los vientos anhelan jugar con tu pelo." :AUTHOR "Khalil Griban" :ITEM "" :DATE "")
 (:CATEGORY "THOUGHTS" :TEXT "Hablas cuando dejas de estar en paz con tus pensamientos." :AUTHOR "Khalil Griban" :ITEM "" :DATE "")
 (:CATEGORY "EVIL" :TEXT "First we overlook evil, Then we permit evil. Then we legalize evil. Then we promote evil. Then we celebrate evil. Then we persecute those who still call it evil." :AUTHOR "Fr. Dwight Longenecker" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "Ser gracioso no es únicamente un talento innato. Los muchos años que he dedicado al humor me han enseñado que éste puede ser continuamente desarrollado y definido explorando nuestro interior, con el fin de encontrar la confianza en uno mismo. Creo que el auténtico talento cómico descansa en la esencia de uno mismo." :AUTHOR "Leo Bassi" :ITEM "" :DATE "")
 (:CATEGORY "UNKNOWN" :TEXT "La gente teme lo desconocido. Se enfrascan en lo conocido y, cual botella de cristal, flotan sobre la superficie al ritmo de una corriente. No les importa descubrir que hay fuera de la botella. Les basta con saber que la botella que les protege tiene la marca de Coca Cola." :AUTHOR "John Rivera" :ITEM "Aviva el fuego hombre de cera" :DATE "")
 (:CATEGORY "HAPPINESS" :TEXT "La mayor felicidad de la vida es la convicción de que somos amados. Amados por nosotros mismos, o más bien, amados a pesar de nosotros mismos." :AUTHOR "Víctor Hugo" :ITEM "Los miserables" :DATE "")
 (:CATEGORY "LOVE" :TEXT "El amor es la poesía de los sentidos, la inteligencia es la nitidez de la razón." :AUTHOR "Enrique Rojas" :ITEM "Todo lo que necesitas saber sobre la vida" :DATE "")
 (:CATEGORY "PROBLEMS" :TEXT "Lo que nos mete en problemas no es lo que no sabemos, sino lo que creemos que sabemos, pero no sabemos." :AUTHOR "Mark Twain" :ITEM "" :DATE "")
 (:CATEGORY "OLDIE" :TEXT "Cuando un anciano muere, es una biblioteca la que se quema, dijo el escritor maliense." :AUTHOR "Amadou Hampâté" :ITEM "" :DATE "1960")
 (:CATEGORY "AGEING" :TEXT "Envejecer significa retirarse gradualmente de la apariencia." :AUTHOR "Johann Wolfgang von Goethe" :ITEM "" :DATE "")
 (:CATEGORY "NAME" :TEXT "Más aún: descubrí una relación entre tener nombre y existir, pues cada vez que volvía al hotel me daba cuenta de que en la ciudad había visto tan solo aquello que sabía nombrar, por ejemplo recordaba una acacia pero no el árbol que crecía junto a ella, porque desconocía su nombre. En una palabra, comprendí que cuanto más vocabulario atesorase, más pronto - y más rico en su inabarcable diversidad — se abriría ante mí el mundo." :AUTHOR "Ryszard Kapuściński" :ITEM "Viajes con Heródoto" :DATE "")
 (:CATEGORY "DECISSIONS" :TEXT "Decisiones difíciles = Vida fácil. Decisiones fáciles = Vida difícil" :AUTHOR "Antonio Valenzuela" :ITEM "Activa tus mitocondrias" :DATE "")
 (:CATEGORY "WATER" :TEXT "Dos peces nadan uno junto al otro cuando se topan con un pez más viejo avanzando en sentido contrario. Éste los saluda y dice: «Buen día muchachos, ¿Cómo está el agua?», antes de seguir su camino. Los dos peces siguen nadando hasta que, después de un tiempo, uno se gira y le pregunta al otro: «¿Qué demonios es el agua?»." :AUTHOR "David Foster Wallace" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "La calidad de nuestra vida depende de la calidad de nuestros pensamientos." :AUTHOR "Marco Aurelio" :ITEM "" :DATE "")
 (:CATEGORY "WISDOM" :TEXT "Señor, dame serenidad para aceptar las cosas que no puedo cambiar, valor para cambiar las cosas que puedo cambiar y sabiduría para reconocer la diferencia." :AUTHOR "Reinhold Niebuhr" :ITEM "" :DATE "")
 (:CATEGORY "LAUGH" :TEXT "Ríe y el mundo reirá contigo; llora y el mundo, dándote la espalda, te dejará llorar." :AUTHOR "Charlie Chaplin" :ITEM "" :DATE "")
 (:CATEGORY "LOOK" :TEXT "Si vas a retroceder, hazlo sin recelo. Si avanzas, hazlo con tu ser completo, sin mirar atrás." :AUTHOR "Arno Ilgner" :ITEM "" :DATE "")
 (:CATEGORY "FEAR" :TEXT "El miedo es como un huésped indeseable que no sale de casa, por lo que hay que fingir que es un amigo." :AUTHOR "Sigmund Freud" :ITEM "" :DATE "")
 (:CATEGORY "HEART" :TEXT "Lo que hoy siente tu corazón, mañana lo entenderá tu cabeza." :AUTHOR "Anónimo" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "Cuenta una vieja leyenda de los indios sioux que, una vez, hasta la tienda del viejo brujo
de la tribu llegaron, tomados de la mano, Toro Bravo, el más valiente y honorable de los jóvenes guerreros, y Nube Alta, la hija del cacique y una de las más hermosas mujeres de la tribu.
—Nos amamos —empezó el joven.
—Y nos vamos a casar —dijo ella.
—Y nos queremos tanto que tenemos miedo.
—Queremos un hechizo, un conjuro, un talismán.
—Algo que nos garantice que podremos estar siempre juntos.
—Que nos asegure que estaremos uno al la do del otro hasta encontrar a Manitú el día de la muerte.
—Por favor —repitieron—, ¿hay algo que podamos hacer?
El viejo los miró y se emocionó de verlos tan jóvenes, tan enamorados, tan anhelantes esperando su palabra.
—Hay algo... —dijo el viejo después de una larga pausa—. Pero no sé... es una tarea muy difícil y sacrificada.
—No importa —dijeron los dos.
—Lo que sea —ratificó Toro Bravo.
—Bien —dijo el brujo—, Nube Alta,¿ ves el monte al norte de nuestra aldea? Deberás escalarlo sola y sin más armas que una red y tus manos, y deberás cazar el halcón más hermoso y vigoroso del monte. Si lo atrapas, deberás traerlo aquí con vida el tercer día después de
la luna llena. ¿ Comprendiste?
La joven asintió en silencio.
—Y tú, Toro Bravo —siguió el brujo—, deberás escalar la montaña del trueno y cuando llegues a la cima, encontrar la más bravía de todas las águilas y solamente con tus manos y una red deberás atraparla sin heridas y traerla ante mí, viva, el mismo día en que vendrá Nube Alta... Salgan ahora.
Los jóvenes se miraron con ternura y después de una fugaz sonrisa salieron a cumplir la misión encomendada, ella hacia el norte, él hacia el sur...
El día establecido, frente a la tienda del brujo, los dos jóvenes esperaban con sendas bolsas de tela que contenían las aves solicitadas.
El viejo les pidió que con mu cho cuidado las sacaran de las bolsas. Los jóvenes lo hicieron y expusieron ante la aprobación del viejo los pájaros cazados. Eran verdaderamente hermosos ejemplares, sin duda lo mejor de su estirpe.
—¿Volaban alto? —preguntó el brujo.
—Sí, sin dudas. Como lo pediste... ¿Y ahora? —preguntó el joven—. ¿Los mataremos y beberemos el honor de su sangre?
—No —dijo el viejo.
—Los cocinaremos y comeremos el valor en su carne —propuso la joven.
—No —repitió el viejo—. Hagan lo que les digo. Tomen las aves y átenlas entre sí por las patas con estas tiras de cuero... Cuando las hayan anudado, suéltenlas y que vuelen libres.
El guerrero y la joven hicieron lo que se les pedía y soltaron los pájaros.
El águila y el halcón in tentaron levantar vuelo pero sólo consiguieron revolcarse en el piso. Unos minutos después, irritadas por la incapacidad, las aves arremetieron a picotazos entre sí hasta lastimarse.
—Este es el conjuro. Jamás olviden lo que han visto. Son ustedes como un águila y un halcón; si se atan el uno al otro, aunque lo hagan por amor, no solo vivirán arrastrándose, sino que además, tarde o temprano, empezarán a lastimarse uno al otro. Si quieren que el amor entre ustedes perdure, vuelen juntos pero jamás atados." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "Se debe tener más miedo a una vida mala que a la muerte." :AUTHOR "Bertolt Brecht" :ITEM "" :DATE "")
 (:CATEGORY "LIFE" :TEXT "En el pasado, la gente vivía la vida de sus antepasados, de generación en generación. Ahora los progenitores quieren vivir la vida de sus descendientes." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "DEATH" :TEXT "Una vez el guerrero está preparado para el hecho de morir, vive su vida sin la preocupación de morir, y escoge sus acciones basado en un principio, no en el miedo.
Si preparando correctamente el corazón cada mañana y noche, uno es capaz de vivir como si su cuerpo ya estuviera muerto, gana libertad en El Camino. Su vida entera estará sin culpa, y tendrá éxito en su llamado." :AUTHOR "" :ITEM "Código del Bushido" :DATE "")
 (:CATEGORY "DEATH" :TEXT "Érase una vez, en la ciudad de Bagdad, un criado que servía a un rico mercader. Un día, muy de mañana, el criado se dirigió al mercado para hacer la compra. Pero esa mañana no era como todas; porque esa mañana vio a la Muerte en el mercado y porque la Muerte le hizo un gesto.
Aterrado el criado volvió a la casa del mercader.
‐ Amo ‐le dijo‐, déjame el caballo más veloz de la casa. Esta noche quiero estar muy lejos de Bagdad. Esta noche quiero estar en la remota ciudad de Ispahán.
‐ Pero ¿Por qué quieres huir?
‐ Porque he visto a la Muerte en el mercado y me ha hecho un gesto de amenaza.
El mercader se compadeció de él y le dejó el caballo; y el criado partió con la esperanza de estar por la noche en Ispahán. Por la tarde, el propio mercader fue al mercado, y, como le había sucedido antes al criado, también él vio a la Muerte.
‐ Muerte ‐le dijo acercándose a ella‐, ¿Por qué has hecho un gesto de amenaza a mi criado?
‐¿Un gesto de amenaza? ‐contestó la Muerte‐ No, no ha sido un gesto de amenaza, sino de asombro. Me ha sorprendido verlo aquí,
tan lejos de Ispahán, porque hoy en la noche debo llevarme en Ispahán a tu criado." :AUTHOR "" :ITEM "Cuento persa" :DATE "")
 (:CATEGORY "LIFE" :TEXT "No one gets out of this world alive. It isn’t dying we should fear, but rather, what dies inside us while we live." :AUTHOR "Norman Cousins" :ITEM "" :DATE "")
 (:CATEGORY "WORLD" :TEXT "Miramos el mundo una sola vez, en la infancia. El resto es memoria. Y así, la vida se desliza después de esa mirada primigenia" :AUTHOR "Louise Glück" :ITEM "" :DATE "")
 (:CATEGORY "LOVE" :TEXT "El Amor no puede existir en un tiempo que sólo existe en nuestra imaginación, ni demostrar que es merecedor de nuestra confianza si nos aferramos a nuestras abyectas fantasías. Tenemos que estar dispuestos a descansar en sus tiernos brazos sin cuestionar la razón de su presencia ni el por qué querrían seguir sosteniéndonos. Terminaré mi exposición con esta bella historia que me contó un amigo. Un hombre que pasó a mejor vida llegó ante Dios. Dios pasó revista a su vida, mostrándole las numerosas lecciones que había aprendido. Una vez que hubo terminado, inquirió:
— Hijo mío, ¿Tienes alguna pregunta?
— Sí —respondió el hombre—, mientras pasábamos revista a mi vida observé que cuando las cosas iban bien había dos pares de huellas en el camino y supe que caminabas a mi lado. Pero cuando las cosas se ponían difíciles tan sólo había un par de huellas. ¿Por qué, Padre, me abandonaste en los momentos difíciles?
Y Dios le respondió:
— Mal interpretas, hijo mío. Es cierto que cuando las cosas iban bien yo caminaba a tu lado indicándote el camino a seguir. Pero cuando las cosas se ponían difíciles te llevaba en mis brazos." :AUTHOR "Hugh Prather" :ITEM "" :PLACE "Santa Fe, Nuevo México\" :DATE \"23/6/1979")
 (:CATEGORY "KIDS" :TEXT "Y saber que solo somos niños heridos con máscaras de adultos sanos y felices. Hasta que despertemos." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "KIDS" :TEXT "Enseña a los niños y no será necesario castigar a los hombres." :AUTHOR "Pitágoras" :ITEM "" :DATE "")
 (:CATEGORY "EDUCATION" :TEXT "La educación no es la preparación para la vida. La educación es la vida en sí misma." :AUTHOR "John Dewey" :ITEM "" :DATE "")
 (:CATEGORY "AUTOIMAGE" :TEXT "Estar preocupado por la autoimagen es como ser ciego y sordo; como estar en medio de un gran campo de flores silvestres con una capucha cubriéndonos la cabeza como estar ante un árbol lleno de pájaros cantores con tapones en los oídos." :AUTHOR "" :ITEM "" :DATE "")
 (:CATEGORY "KIDS" :TEXT "El niño que no juega no es niño, pero el hombre que no juega perdió para siempre al niño que vivía en él y que le hará mucha falta." :AUTHOR "Pablo Neruda" :ITEM "" :DATE "")
 (:CATEGORY "" :TEXT "Camina plácido entre el ruido y la prisa,
y recuerda que la paz se puede encontrar en el silencio. En cuanto te sea posible y sin rendirte, mantén buenas relaciones con todas las personas. Enuncia tu verdad de una manera serena y clara, y escucha a los demás, incluso al torpe e ignorante, también ellos tienen su propia historia. Evita a las personas ruidosas y agresivas, ya que son un fastidio para el espíritu. Si te comparas con los demás, te volverás vano y amargado pues siempre habrá personas más grandes y más pequeñas que tú. Disfruta de tus éxitos, lo mismo que de tus planes. Mantén el interés en tu propia carrera, por humilde que sea, ella es un verdadero tesoro en el fortuito cambiar de los tiempos. Sé cauto en tus negocios, pues el mundo está lleno de engaños. Pero no dejes que esto te vuelva ciego para la virtud que existe, hay muchas personas que se esfuerzan por alcanzar nobles ideales, la vida está llena de heroísmo. Sé tú mismo, y en especial no finjas el afecto, y no seas cínico en el amor, pues en medio de todas las arideces y desengaños, es perenne como la hierba. Acata dócilmente el consejo de los años, abandonando con donaire las cosas de la juventud.
Cultiva la firmeza del espíritu para que te proteja de las adversidades repentinas, mas no te agotes con pensamientos oscuros, muchos temores nacen de la fatiga y la soledad. Sobre una sana disciplina, sé benigno contigo mismo. Tú eres una criatura del universo, no menos que los árboles y las estrellas, tienes derecho a existir, y sea que te resulte claro o no, indudablemente el universo marcha como debiera. Por eso debes estar en paz con Dios, cualquiera que sea tu idea de Él, y sean cualesquiera tus trabajos y aspiraciones, conserva la paz con tu alma en la bulliciosa confusión de la vida. Aún con todas sus farsas, penalidades y sueños fallidos, el mundo es todavía hermoso. Sé alegre. Esfuérzate por ser feliz. " :AUTHOR "Max Ehrmann" :ITEM "Desiderata" :DATE "1948")
 (:CATEGORY "HISTORY" :TEXT "The so-called lessons of history are for the most part the razionalizations of the victors. History is written by the survivors." :AUTHOR "Max Lerner" :ITEM "" :DATE ""))

