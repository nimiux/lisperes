#!/bin/sh

# To make distribution from current code use: version="HEAD"
version="$1"

[ -z "${version}" ] && echo "Usage: $((basename $0)) <version>" && exit 1

base="lisperes-${version}"
rm -vf "${base}".tar{,.bz2}
git archive "--prefix=${base}/" --format=tar -v "--output=${base}.tar" ${version}
bzip2 -v9 "${base}.tar"

