#!/bin/bash

SRCDIR="${HOME}/lisperes/"
DSTDIR="${HOME}/common-lisp/lisperes/"

HTDOCSDIR="${SRCDIR}/htdocs/"
PUBLICDIR="${HOME}/public_html/"

syncdir () {
    dir=$1
    targetdir="${PUBLICDIR}/$dir/"
    rsync -avzt --delete "${HTDOCSDIR}/$dir/" "${targetdir}"
    find $targetdir -type f -exec chmod 644 {} \;
    find $targetdir -type d -exec chmod 755 {} \;
}

deploy () {
    syncdir css
    syncdir fonts
    syncdir img
    echo "copying humans.txt"
    cp "${HTDOCSDIR}/humans.txt" "${PUBLICDIR}"
    echo "rsync -avzt --delete --exclude .git $SRCDIR $DSTDIR"
    rsync -avzt --delete --exclude .git $SRCDIR $DSTDIR
}

while : ; do
    echo "Waiting for changes in $SRCDIR..."
    inotifywait -r -q ${SRCDIR}
    echo "$(date): Changes detected in $SRCDIR. Deploying..."
    sleep 5
    deploy
done
