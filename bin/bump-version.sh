#!/bin/sh

VERSIONFILE=version.sexp
CHANGELOGFILE="CHANGELOG.org"
PROJECTURL="http://codeberg.org/nimiux/lisperes"
CURRENTVERSION=$(git tag | tail -1)
NEWVERSION=${1}

[ -z "${NEWVERSION}" ] && echo "Usage: $(basename $0) <version>" && exit 1

add_to_top () {
    logfile=${1}
    temp=$(mktemp)
    cat - ${1} > $temp && mv $temp ${1}
}

# Version file
echo ";; -*- lisp -*-" > ${VERSIONFILE}
echo "\"${NEWVERSION}\"" >> ${VERSIONFILE}

# Changelog
touch ${CHANGELOGFILE}
echo | add_to_top ${CHANGELOGFILE}
git log ${CURRENTVERSION}...HEAD --pretty=format:"** [[${PROJECTURL}/commit/%H][%s]]" | add_to_top ${CHANGELOGFILE}
echo "* Version: ${NEWVERSION}  $(date +%Y-%m-%d)  $(git config --get user.name) <$(git config --get user.email)>" | \
     add_to_top ${CHANGELOGFILE}

git commit -am "Bump version to ${NEWVERSION}"
