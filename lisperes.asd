;;; -*- mode: Lisp; show-trailing-whitespace: t; indent-tabs-mode: t; -*-

;;; Copyright (c) 2012-2025. José María Alonso Josa. All rights reserved

#-(or sbcl)
(warn "This system hasn't been tested in your lisp implementation")

(in-package :cl-user)

(defpackage :lisperes-asd
  (:use :cl :asdf))

(in-package :lisperes-asd)

(defsystem :lisperes
    :serial t
    :version (:read-file-form "version.sexp")
    :description "Web app written in Common Lisp"
    :author "Chema Alonso Josa <nimiux@freeshell.de>"
    :maintainer "Chema Alonso Josa <nimiux@freeshell.de>"
    :licence "UNLICENSE"
    :depends-on (:cl-who
		 :hunchentoot
		 :lass
		 :parenscript
		 :slynk
		 :swank
		 :linedit)
    :components ((:static-file "version.sexp")
                 (:static-file "README.org")
		 (:static-file "CHANGELOG")
		 (:static-file "INSTALL.org")
		 (:static-file "UNLICENSE")
		 (:static-file "db/fortunes")
		 (:file "package")
		 (:file "specials")
		 (:file "fortune")
		 (:file "pages")
		 (:file "lisperes")
		 (:file "install"))
    :in-order-to ((test-op (test-op lisperes/test))))

(defsystem :lisperes/test
    :description "Self test functionality for this web application."
    :depends-on (:lisperes
		 :prove)
    :defsystem-depends-on (:prove-asdf)
    :components ((:file "test/package")
                 (:test-file "test/tests"))
    :perform (test-op :after (op c)
                        (funcall (intern #.(string :run) :prove) c)))
