#!/usr/bin/perl

# Proxy to publish the app

#use LWP::UserAgent;
use warnings;

use DateTime qw( );
use File::Spec;
my ($vol, $dir, $file) = File::Spec->splitpath($ENV{SCRIPT_FILENAME});
my $logfile = "$dir/../log/lisperes/cgi.log";

sub printenvvars {
    foreach $key (sort keys(%ENV)) {
        print FH "$key = $ENV{$key}\n";
    }
}

sub logit {
    # Use Apache log format. For example
    # "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\""
    # 127.0.0.1 - frank [10/Oct/2000:13:55:36 -0700] "GET /apache_pb.gif HTTP/1.0" 200 2326 "http://www.example.com/start.html" "Mozilla/4.08 [en] (Win98; I ;Nav)"
    # 31.4.135.133 - frank [10/Feb/2023:21:23:59 -0700] "GET /apache. HTTP/1.0" 200 200 "http://freeshell.de/nimiux/" "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/110.0"
    open(FH, '>>', $logfile) or die "Could not open file '$logfile' $!";
    print FH $ENV{REMOTE_ADDR};
    print FH " - - ";
    my $dt = DateTime->now(time_zone => 'local');
    $dt->subtract( days => 7 );
    my $strtime = $dt->strftime("%d/%b/%Y:%H:%M:%S %z");
    print FH qq([$strtime] );
    print FH qq("$ENV{REQUEST_METHOD} $ENV{SERVER_PROTOCOL}" );
    print FH "200 200 ";
    print FH qq("http://$ENV{SERVER_NAME}/$ENV{REQUEST_URI}" );
    print FH qq("$ENV{HTTP_USER_AGENT}" );
    print FH "\n";
    close FH;
}

#my $uri = $ENV{REQUEST_URI};
#$uri = s/~nimiux//;

# URL
#my $url = URI->new( 'http://127.0.0.1:4242' );

# user agent to use a proxy
#my $user_agent = LWP::UserAgent->new;
#$user_agent->proxy( 'http', 'http://our_proxy:port/' );

# request
#my $req = HTTP::Request->new( GET => "$url/$uri" );

# response
#my $res = $user_agent->request( $req );

# Log it
logit;

# Render content
print "Content-Type: text/html\n\n";
system "curl http://127.0.0.1:4242";
#print $res->content;
